import re
import string
import itertools
import pandas as pd
import nltk
from nltk.stem import WordNetLemmatizer
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer

nltk.download('punkt')
nltk.download('stopwords')
nltk.download('wordnet')
nltk.download('averaged_perceptron_tagger')


try:
	import spacy
	nlp = spacy.load('en_core_web_sm')
except OSError:
    print('Downloading language model for the spaCy POS tagger\n'
        "(don't worry, this will only happen once)")
    from spacy.cli import download
    download('en_core_web_sm')

import en_core_web_sm
nlp = en_core_web_sm.load()

@pd.api.extensions.register_dataframe_accessor("textPreProcessing")
@pd.api.extensions.register_series_accessor("textPreProcessing")
class TextPreProcessing(object):
    """
    This class contains methods required to clean and preprocess text data in order to perform different NLP techniques
    """
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
    def removeWhiteSpace(self, colNames = None):
        """
        This function removes whitespaces from the corpus which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which whitespaces has to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removeWhiteSpace(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = [k.strip() for k in df]
                df = [re.sub(r' +',' ', k) for k in df.values.tolist()]
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [k.strip() for k in df[column]]
                df[column] = [re.sub(r' +',' ', k) for k in df[column].values.tolist()]
                       
        return df
    
     
    def convertLowerCase(self, colNames = None):
        """
        This function converts all the text in the corpus to lower case.
    
        Parameters:
        -----------
        colNames    : List of column names that are needed to convert to lower case for a standard format
    
        Examples:
        ---------
        df = df.textPreProcessing.convertLowerCase(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = df.str.lower()
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = df[column].str.lower()
         
        return df

    def removeEmail(self, colNames = None):
        """
        This function removes email from the corpus which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which email has to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removeEmail(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = [re.sub('\S+@\S+',"", k) for k in df.values.tolist()]
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [re.sub('\S+@\S+',"", k) for k in df[column].values.tolist()]
    
        return df
    
    
    def removeHyperlink(self, colNames = None):
        """
        This function removes hyperlink from the corpus which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which hyperlinks has to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removeHyperlink(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = [re.sub(r"http\S+","", k) for k in df.values.tolist()]
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [re.sub(r"http\S+","", k) for k in df[column].values.tolist()]
                 
        return df
    
    
    def removeDigits(self, colNames = None):
        """
        This function removes digits/numbers from the corpus which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which digits/numbers has to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removeDigits(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = [re.sub("\d+", "", k) for k in df.values.tolist()]
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [re.sub("\d+", "", k) for k in df[column].values.tolist()]
                         
        return df
    
    
    def removeOtherWords(self, colNames = None):
        """
        This function removes words from the corpus which is given as an input by the user which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which some words has to be removed as per the user which is not required for text analysis
    
        Examples:
        --------
        df = df.textPreProcessing.removeOtherWords(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                data = df.values.tolist()
                removeWords = input("enter the words to remove from corpus separated by comma: ")
                removeWordsList = removeWords.split(",") 
                removeWordsList = [str(a) for a in removeWordsList]
                for k in removeWordsList:
                    allSentences=[]
                    for i in data:
                        wordList = i.split()
                        kk = ' '.join([r for r in wordList if r not in k])
                        allSentences.append(kk)
                df=allSentences
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                data = df[column].values.tolist()
                removeWords = input("enter the words to remove from corpus separated by comma: ")
                removeWordsList = removeWords.split(",") 
                removeWordsList = [str(a) for a in removeWordsList]
                for k in removeWordsList:
                    allSentences=[]
                    for i in data:
                        wordList = i.split()
                        kk = ' '.join([r for r in wordList if r not in k])
                        allSentences.append(kk)
                        data = allSentences
                df[column]=allSentences

                              
        return df
    
    
    def removePunctuation(self, colNames = None):
        """
        This function removes punctuation from the corpus which is not required for text analysis.
    
        Parameters:
        -----------
        colNames    : List of column names from which punctuation has to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removePunctuation(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                df = [re.compile('[%s]' % re.escape(string.punctuation)).sub('', k) for k in df.values.tolist()]
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [re.compile('[%s]' % re.escape(string.punctuation)).sub('', k) for k in df[column].values.tolist()]
        
                                  
        return df
    
    def makeWordsStandardized(self, colNames = None):
        """
        This function is used to make words standardized. 
        Example:
        Input: I am sooooo happppppyyyyyyyy. 
        Output: I am soo happyy.
    
        Parameters:
        -----------
        colNames    : List of column names for which words need to be standardized.
    
        Examples:
        ---------
        df = df.textPreProcessing.makeWordsStandardized(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                allSentences = []
                for k in df:
                    sentence = ''.join(''.join(s)[:2] for _, s in itertools.groupby(k))
                    allSentences.append(sentence)
                df=allSentences
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                allSentences = []
                for k in df[column]:
                    sentence = ''.join(''.join(s)[:2] for _, s in itertools.groupby(k))
                    allSentences.append(sentence)
                df[column]=allSentences
        
        
        return df
    
    def removeSpecialCharacters(self, colNames = None,regexPattern=r'[^\s0-9a-zA-Z\)\!\"\#\$\%\&\'\(\\\*\+\,\-\.\}\[\/\:\;\<\>\=\?\@\^\|\~\_\`\{\]]'):
        """
        This function is used to remove special characters from the corpus which is not required for text analysis. 
        Example of Special Characters: Ã, ¢,†,Ã°,Ÿ
    
        Parameters:
        -----------
        colNames    : List of column names from which special characters need to be removed.
        regexPattern: The defined regular expression pattern removes all the special characters present in the corpus except 0-9,a-z,A-Z and all the punctuation marks.
                      User can change the regexPattern as per the requirement.
        Examples:
        ---------
        df = df.textPreProcessing.removeSpecialCharacters(['tweet'],regexPattern = r'[^\s0-9a-zA-Z\)\!\"\#\$\%\&\'\(\\\*\+\,\-\.\}\[\/\:\;\<\>\=\?\@\^\|\~\_\`\{\]]')
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        regPattern = regexPattern
        if colNames is None:
            try:
                df = [re.sub(regPattern,"", k) for k in df.values.tolist()]

            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                df[column] = [re.sub(regPattern,"", k) for k in df[column].values.tolist()]
        
        
        return df
    
    def removeWordsWithOneCharacter(self, colNames = None):
        """
        This function is used to remove words which have only one character. 
    
        Parameters:
        -----------
        colNames    : List of column names from which words which have one character need to be removed
    
        Examples:
        ---------
        df = df.textPreProcessing.removeWordsWithOneCharacter(['tweet'])
        >>> Output  : Returns a pandas dataframe
        """
        df = self._obj.copy()
        if colNames is None:
            try:
                allSentences=[]
                data = df.values.tolist()
                for i in data:
                    wordList = i.split(" ")
                    wordList = [str(a) for a in wordList]
                    for w in wordList:
                        if len(w)==1:
                            wordList.remove(w)
                    sentence = " ".join(r for r in wordList)
                    allSentences.append(sentence)
                df=allSentences
            except:
                print("Input data could not be parsed. Do provide the list of column names in the parameter list of the function.")
        else:
            for column in colNames:
                allSentences=[]
                data = df[column].values.tolist()
                for i in data:
                    wordList = i.split(" ")
                    wordList = [str(a) for a in wordList]
                    for w in wordList:
                        if len(w)==1:
                            wordList.remove(w)
                    sentence = " ".join(r for r in wordList)
                    allSentences.append(sentence)
                df[column]=allSentences

        
        return df

    
        
    def lemmatize(self, colNames = None):
        """
        The function will apply lemmatization on the input tokenized dataframe/series.
        
        Parameters:
        -----------
        colNames    : List of column names that are need to lemmatize.
        
        Examples:
        ---------
        df = df.textPreProcessing.lemmatize(colNames=['Tokenized_tweet'])
        >>> Output  : Returns a pandas dataframe        
        """
        df = self._obj.copy()
        wordNetLemmatizer = WordNetLemmatizer()
        if colNames is None:
            try:
                df = pd.Series([(lambda sentence: [wordNetLemmatizer.lemmatize(word) for word in sentence])(sentence) for sentence in list(df)])
            except Exception as e:
                print("Error occured while parsing data. Try to provide list of colNmaes")
                print(e)
        else:
            for column in colNames:
                df[column] = [(lambda sentence: [wordNetLemmatizer.lemmatize(word) for word in sentence])(sentence) for sentence in df[column].tolist()]
        return df
    
    
    
    def tokenization (self,colNames=None,by='Word'):
        """
        Segmenting text into words and sentences
        
        Parameters:
        -----------
        colNames    : List of text data column names that are need to tokenize
        by          :'Word' or 'Line' tokenization
        
        Examples:
        ---------
        df = df.textPreProcessing.tokenization(colNames=['tweet'],by='Word')
        >>> Output  : Returns a pandas dataframe with a tokenized column        
        """
        df = self._obj.copy()
        if colNames == None:
            try:
                df = df.apply(nltk.word_tokenize)
            except Exception as e:
                print("Please enter a series or the column name which has to be tokenized")
                print(e)
        else:
            for column in colNames:
                if by=='Word':
                    df["Tokenized_"+column] = df[column].apply(nltk.word_tokenize)
                if by=='Line':
                    df["Tokenized_"+column] = df[column].apply(nltk.sent_tokenize)        
        return df    
    
    def POS(self,colNames=None):
        """
        This function assigns parts of speech(POS) to each word of a given text 
        (such as nouns, verbs, adjectives, and others) based on its definition and its context.
        
        Parameters:
        -----------
        colNames    : List of word tokenized column names where parts of speech need to find
        
        Examples:
        ---------
        df = df.textPreProcessing.POS(colNames=['Tokenized_tweet'])
        >>> Output  : Returns a pandas dataframe with word tokenized column with parts of speech for each word       
        """
        df = self._obj.copy()
        if colNames == None:
            try:
                df = df.apply(nltk.pos_tag)
            except Exception as e:
                print("Please enter a series or the column name which has to be tokenized")
                print(e)
        else:
            for column in colNames:
                df["POS_tagging_"+column] = df[column].apply(nltk.pos_tag)
        return df
    
    
    def removeStopWord(self,colNames=None):
        """
        This function removes pre defined stop words
        
        Parameters:
        -----------
        colNames    : List of word tokenized column names
        
        Examples:
        ---------
        df = df.textPreProcessing.removeStopWord(colNames=['Tokenized_tweet'])
        >>> Output : Returns a pandas dataframe with a word tokenized column in which stop words is removed
        """
        df = self._obj.copy()
        stopWords= set(stopwords.words("english"))
        if colNames == None:
            try:
                df = df.apply(lambda x: [item for item in x if item not in stopWords])
            except Exception as e:
                print("Please enter a series or the column name which has to be tokenized")
                print(e)
        else:
            for column in colNames:
                df[column] = df[column].apply(lambda x: [item for item in x if item not in stopWords])
        return df

    def stemmer(self,colNames=None):
        """
        The function will apply stemming on the input tokenized dataframe/series.
        
        Parameters:
        -----------
        colNames    : List of column names that are need to do stemming.
        
        Examples:
        ---------
        df = df.textPreProcessing.stemmer(colNames=['Tokenized_tweet'])
        >>> Output : Returns a pandas dataframe with a work tokenized column in which stemming is done
        """
        df = self._obj.copy()
        porterStemmer  = PorterStemmer()
        if colNames is None:
            try:
                df = pd.Series([(lambda sentence: [porterStemmer.stem(word) for word in sentence])(sentence) for sentence in list(df)])
            except Exception as e:
                print("Error occured while parsing data. Try to provide list of colNmaes")
                print(e)
        else:
            for column in colNames:
                df[column] = [(lambda sentence: [porterStemmer.stem(word) for word in sentence])(sentence) for sentence in df[column].tolist()]
        return df
    
    def namedEntity(self,colNames):
        """
        The function will find out named entities  on the input tokenized dataframe/series.
        
        Parameters:
        -----------
        colNames    : List of word tokenized column names to recognize named entities.
        
        Examples:
        ---------
        df = df.textPreProcessing.namedEntity(colNames=['Tokenized_tweet'])
        >>> Output : Returns a pandas dataframe with word tokenized column with named entities for that tokenized column
        """
        df = self._obj.copy()
        named_entity = []
        for column in colNames:
            for sent in df[column]:
                sentence = " ".join(str(x) for x in sent)
                doc = nlp(sentence)
                ner = [(X.text, X.label_) for X in doc.ents]
                named_entity.append(ner)
        df['NER'] = named_entity
        return df
    
        