﻿from sklearn.ensemble import RandomForestClassifier
#import ModelEvaluationMetrices
import pandas as pd

class RandomForestClassifier1:
    """
    This class builds a random forest classifier and scores the dataset
    """
    def __init__(self, df,n_estimators='warn', criterion='gini', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto', max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None, random_state=None, verbose=0, warm_start=False, class_weight=None):
        """
        The constructor of the class creates an object for RandomForestClassifier1 class
        Parameters:
        -----------
        df                       : pandas dataframe (the dataset)
        n_estimators             : integer, optional (default=10) The number of trees in the forest.
        criterion                : string, optional (default="gini") The function to measure the quality of a split. Supported criteria are "gini" for the Gini impurity and "entropy" for the information gain. Note: this parameter is tree-specific.
        max_depth                : integer or None, optional (default=None) The maximum depth of the tree. If None, then nodes are expanded until all leaves are pure or until all leaves contain less than min_samples_split samples.
        min_samples_split        : int, float, optional (default=2) The minimum number of samples required to split an internal node:
                                   If int, then consider min_samples_split as the minimum number.
                                   If float, then min_samples_split is a fraction and ceil(min_samples_split * n_samples) are the minimum number of samples for each split.
        min_samples_leaf         : int, float, optional (default=1) The minimum number of samples required to be at a leaf node. A split point at any depth will only be considered if it leaves at least min_samples_leaf training samples in each of the left and right branches. This may have the effect of smoothing the model, especially in regression.
                                   If int, then consider min_samples_leaf as the minimum number.
                                   If float, then min_samples_leaf is a fraction and ceil(min_samples_leaf * n_samples) are the minimum number of samples for each node.
        min_weight_fraction_leaf : float, optional (default=0.) The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided.
        max_features             : int, float, string or None, optional (default="auto")
                                   The number of features to consider when looking for the best split:
                                   If int, then consider max_features features at each split.
                                   If float, then max_features is a fraction and int(max_features * n_features) features are considered at each split.
                                   If "auto", then max_features=sqrt(n_features).
                                   If "sqrt", then max_features=sqrt(n_features) (same as "auto").
                                   If "log2", then max_features=log2(n_features).
                                   If None, then max_features=n_features.
                                   Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than max_features features.
        max_leaf_nodes           : int or None, optional (default=None) Grow trees with max_leaf_nodes in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes.
        min_impurity_decrease    : float, optional (default=0.) A node will be split if this split induces a decrease of the impurity greater than or equal to this value.
        bootstrap                : boolean, optional (default=True) Whether bootstrap samples are used when building trees. If False, the whole datset is used to build each tree.
        oob_score                : bool (default=False) Whether to use out-of-bag samples to estimate the generalization accuracy.
        n_jobs                   : int or None, optional (default=None) The number of jobs to run in parallel for both fit and predict. None means 1 unless in a joblib.parallel_backend context. -1 means using all processors.
        random_state             : int, RandomState instance or None, optional (default=None) If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.
        verbose                  : int, optional (default=0) Controls the verbosity when fitting and predicting.
        warm_start               : bool, optional (default=False) When set to True, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just fit a whole new forest.
        class_weight             : dict, list of dicts, "balanced", "balanced_subsample" or None, optional (default=None)
                                   Weights associated with classes in the form {class_label: weight}. If not given, all classes are supposed to have weight one. For multi-output problems, a list of dicts can be provided in the same order as the columns of y.
                                   Note that for multioutput (including multilabel) weights should be defined for each class of every column in its own dict. For example, for four-class multilabel classification weights should be [{0: 1, 1: 1}, {0: 1, 1: 5}, {0: 1, 1: 1}, {0: 1, 1: 1}] instead of [{1:1}, {2:5}, {3:1}, {4:1}].
                                   The “balanced” mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as n_samples / (n_classes * np.bincount(y))
                                   The “balanced_subsample” mode is the same as “balanced” except that weights are computed based on the bootstrap sample for every tree grown.
                                   For multi-output, the weights of each column of y will be multiplied.
                                   Note that these weights will be multiplied with sample_weight (passed through the fit method) if sample_weight is specified.
        Examples:
        ---------
        rfObject = RandomForestClassifier1(df,n_estimators='warn', criterion='gini', max_depth=None, min_samples_split=2, min_samples_leaf=1, min_weight_fraction_leaf=0.0, max_features='auto', max_leaf_nodes=None, min_impurity_decrease=0.0, min_impurity_split=None, bootstrap=True, oob_score=False, n_jobs=None, random_state=None, verbose=0, warm_start=False, class_weight=None)
        
        """
        self.df = df
        self.clf = RandomForestClassifier(n_estimators=n_estimators, criterion=criterion, max_depth=max_depth, min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf, min_weight_fraction_leaf= min_weight_fraction_leaf, max_features=max_features, max_leaf_nodes=max_leaf_nodes, min_impurity_decrease= min_impurity_decrease, min_impurity_split= min_impurity_split, bootstrap=bootstrap, oob_score=oob_score, n_jobs=n_jobs, random_state=random_state, verbose=verbose, warm_start=warm_start, class_weight=class_weight)
        
        
        
        
    def fit(self,depVar,vector):
        """
        fit - This function fit the gradient boosting model to the dataset.
        Parameters:
        -----------   
        depVar : Dependent variable from the dataset
        vector : bow[0], sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques.
                 For using vectorizeTfidf, give tfidf[0]
                 For using vectorizeHashing, give hashing_matrix
                 For using embedDoc2Vec, give doc2vecMatrix
            
        Examples:
        ---------
        rfModel = rfObject.fit(depVar='label',vector=bow[0])
        >>> Output  : Random forest fitted to the dataset
        """
        
        self.vector = vector
        return self.clf.fit(self.vector, self.df[depVar])
            
            
    def scoreDataset(self,df):
        """
        scoreDataset - outputs the predicted probablities of the Random forest model built, the user can input test/ train dataset in order to score the dataset
        Parameters:
        -----------   
        df : pandas dataframe (the dataset)
            
        Examples:
        ---------
        scoreDfRf = rfObject.scoreDataset(df)
        >>> Output : Dataframe with the predicted probablities
        
        """
        modelFit = self.clf
        vectorizeData = self.vector
        scoringTest = modelFit.predict_proba(vectorizeData)
        dataframe_test=pd.DataFrame(scoringTest, columns=['0','1'])
        scoringTest=dataframe_test['1']
        df_d = pd.concat([df, dataframe_test], axis=1)
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
        #df_d.to_csv("randomforestout.csv",index=False)
        
        return(df_d)
