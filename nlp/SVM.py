from sklearn import model_selection, svm
from sklearn.metrics import accuracy_score
import pandas as pd
#import ModelEvaluationMetrices

class SVM:
    """
    This class builds a SVM classifier  and scores the dataset
    
    """
    def __init__(self, df,C=1.0, kernel='rbf', degree=3, gamma='auto_deprecated', coef0=0.0, shrinking=True, probability=True, tol=0.001, cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', random_state=None):
        """
        The constructor of the class SVM creates an object
        Parameters:
        -----------
        df                      : pandas dataframe (the dataset)
        C                       : float, optional (default=1.0) Penalty parameter C of the error term.
        kernel                  : string, optional (default='rbf') Specifies the kernel type to be used in the algorithm. It must be one of 'linear', 'poly', 'rbf', 'sigmoid', 'precomputed' or a callable. If none is given, 'rbf' will be used. If a callable is given it is used to pre-compute the kernel matrix from data matrices; that matrix should be an array of shape (n_samples, n_samples).
        degree                  : int, optional (default=3) Degree of the polynomial kernel function ('poly'). Ignored by all other kernels.
        gamma                   : float, optional (default='auto') Kernel coefficient for 'rbf', 'poly' and 'sigmoid'. Current default is 'auto' which uses 1 / n_features, if gamma='scale' is passed then it uses 1 / (n_features * X.var()) as value of gamma. 
        coef0                   : float, optional (default=0.0) Independent term in kernel function. It is only significant in 'poly' and 'sigmoid'.
        shrinking               : boolean, optional (default=True) Whether to use the shrinking heuristic.
        probability             : boolean, optional (default=False) Whether to enable probability estimates. This must be enabled prior to calling fit, and will slow down that method.
        tol                     : float, optional (default=1e-3) Tolerance for stopping criterion.
        cache_size              : float, optional Specify the size of the kernel cache (in MB).
        class_weight            : {dict, ‘balanced’}, optional Set the parameter C of class i to class_weight[i]*C for SVC. If not given, all classes are supposed to have weight one. The “balanced” mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as n_samples / (n_classes * np.bincount(y))
        verbose                 : bool, default: False Enable verbose output. Note that this setting takes advantage of a per-process runtime setting in libsvm that, if enabled, may not work properly in a multithreaded context.
        max_iter                : int, optional (default=-1) Hard limit on iterations within solver, or -1 for no limit.
        decision_function_shape : 'ovo', 'ovr', default='ovr' Whether to return a one-vs-rest (‘ovr’) decision function of shape (n_samples, n_classes) as all other classifiers, or the original one-vs-one (‘ovo’) decision function of libsvm which has shape (n_samples, n_classes * (n_classes - 1) / 2). However, one-vs-one (‘ovo’) is always used as multi-class strategy.
        random_state            : int, RandomState instance or None, optional (default=None) The seed of the pseudo random number generator used when shuffling the data for probability estimates. If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

        Examples:
        ---------
        svmObject = SVM(df,C=1.0, kernel='rbf', degree=3, gamma='auto_deprecated', coef0=0.0, shrinking=True, probability=True, tol=0.001, cache_size=200, class_weight=None, verbose=False, max_iter=-1, decision_function_shape='ovr', random_state=None)
        
        """
        self.df = df
        self.clf = svm.SVC(C=C, kernel=kernel, degree=degree, gamma=gamma, coef0=coef0, shrinking=shrinking, probability=probability, tol=tol, cache_size=cache_size, class_weight=class_weight, verbose=verbose, max_iter=max_iter, decision_function_shape=decision_function_shape, random_state=random_state)
        
        
        
        
    def fit(self,depVar,vector):
        """
        fit - This function fit the SVM model to the dataset.
        Parameters:
        -----------   
        depVar : Dependent variable from the dataset
        vector : bow[0], sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques.
                 For using vectorizeTfidf, give tfidf[0]
                 For using vectorizeHashing, give hashing_matrix
                 For using embedDoc2Vec, give doc2vecMatrix
            
        Examples:
        ---------
        svmModel = svmObject.fit(depVar='label',vector=bow[0])
        >>> Output  : SVM fitted to the dataset
        """
        self.vector = vector
        return self.clf.fit(self.vector, self.df[depVar])
        
            
            
            
    def scoreDataset(self,df):
        """
        scoreDataset - outputs the predicted probablities of the SVM model built, the user can input test/ train dataset in order to score the dataset
        Parameters:
        -----------   
        df : pandas dataframe (the dataset)
            
        Examples:
        ---------
        scoreDfSVM = svmObject.scoreDataset(df)
        >>> Output : Dataframe with the predicted probablities
        
        """
        modelFit = self.clf
        vectorizeData = self.vector
        scoringTest = modelFit.predict_proba(vectorizeData)
        dataframe_test=pd.DataFrame(scoringTest, columns=['0','1'])
        scoringTest=dataframe_test['1']
        df_d = pd.concat([df, dataframe_test], axis=1)
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
        #df_d.to_csv("svmout.csv",index=False)
        return df_d

    
