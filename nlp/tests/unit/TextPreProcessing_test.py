import unittest
from unittest.mock import patch
import pandas as pd
import sys
import os
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import TextPreProcessing



class TestTextPreProcessing(unittest.TestCase):

    def setUp(self):
        self.dfRaw = pd.read_csv("dataset.csv")
        self.dfClean = pd.read_csv("clean_dataset_v2.csv")
        self.dfTokenize = self.dfRaw.textPreProcessing.tokenization(colNames=['tweet'])

        
    def test_tokenization(self):
        tokenizeData=pd.read_csv("tokenize.csv")
        testTokenize = self.dfTokenize.copy()
        testTokenize['Tokenized_tweet'] = testTokenize['Tokenized_tweet'].astype(str)
        pd.util.testing.assert_frame_equal(testTokenize, tokenizeData)
        
# =============================================================================
#     def test_stemmer(self):
#         stemmedData=pd.read_csv("newstem.csv")
#         dfStemmer = self.dfTokenize.copy()
#         dfStemmer = dfStemmer.textPreProcessing.stemmer(colNames=['Tokenized_tweet'])
#         dfStemmer['Tokenized_tweet'] = dfStemmer['Tokenized_tweet'].astype(str)
#         numpy.testing.assert_string_equal(str(dfStemmer),str(stemmedData))
#         #pd.util.testing.assert_frame_equal(dfStemmer, stemmedData)
#         
# =============================================================================
    def test_NER(self):
        NERData=pd.read_csv("NER.csv")
        dfNER = self.dfTokenize.copy()
        dfNER = dfNER.textPreProcessing.namedEntity(colNames=['Tokenized_tweet'])
        dfNER['NER'] = dfNER['NER'].astype(str)
        dfNER['Tokenized_tweet'] = dfNER['Tokenized_tweet'].astype(str)
        numpy.testing.assert_string_equal(str(dfNER),str(NERData))
        

    def test_convertLowerCase(self):
        pd.util.testing.assert_frame_equal(self.dfClean.textPreProcessing.convertLowerCase(colNames=['tweet']), pd.read_csv("convertLowerCase.csv"))

    def test_removeEmail(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeEmail(colNames=['tweet']), pd.read_csv("removeEmail.csv"))


    def test_removeHyperlink(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeHyperlink(colNames=['tweet']), pd.read_csv("removeHyperlink.csv"))

    def test_removeWhiteSpace(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeWhiteSpace(colNames=['tweet']), pd.read_csv("removeWhiteSpace.csv"))

    def test_removeDigits(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeDigits(colNames=['tweet']), pd.read_csv("removeDigits.csv"))

    def test_removeOtherWords(self):
        with patch('builtins.input', side_effect="NAN, nan"):
            dfremoveOtherWords = self.dfRaw.textPreProcessing.removeOtherWords(colNames=['tweet'])
        pd.util.testing.assert_frame_equal(dfremoveOtherWords, pd.read_csv("removeOtherWords.csv"))

    def test_removePunctuation(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removePunctuation(colNames=['tweet']), pd.read_csv("removePunctuation.csv"))

    def test_makeWordsStandardized(self):
        pd.util.testing.assert_frame_equal(self.dfClean.textPreProcessing.makeWordsStandardized(colNames=['tweet']), pd.read_csv("makeWordsStandardized.csv"))

    def test_removeSpecialCharacters(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeSpecialCharacters(colNames=['tweet'],regexPattern=r'[^\s0-9a-zA-Z\)\!\"\#\$\%\&\'\(\\\*\+\,\-\.\}\[\/\:\;\<\>\=\?\@\^\|\~\_\`\{\]]'),pd.read_csv("removeSpecialCharacters.csv"))

    def test_removeWordsWithOneCharacter(self):
        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeWordsWithOneCharacter(colNames=['tweet']), pd.read_csv("removeWordsWithOneCharacter.csv"))

    def test_lemmatize(self):
        dfLemmatize = self.dfTokenize.copy()
        dfLemmatize = dfLemmatize.textPreProcessing.lemmatize(colNames=['Tokenized_tweet'])
        dfLemmatize['Tokenized_tweet'] = dfLemmatize['Tokenized_tweet'].astype(str)
        pd.util.testing.assert_frame_equal(dfLemmatize, pd.read_csv("lemmatize.csv"))

    def test_POS(self):
        dfPOS = self.dfTokenize.copy()
        dfPOS = dfPOS.textPreProcessing.POS(colNames=['Tokenized_tweet'])
        dfPOS['Tokenized_tweet']=dfPOS['Tokenized_tweet'].astype(str)
        dfPOS['POS_tagging_Tokenized_tweet'] = dfPOS['POS_tagging_Tokenized_tweet'].astype(str)
        pd.util.testing.assert_frame_equal(dfPOS, pd.read_csv("POS.csv"))

    def test_removeStopWord(self):
        dfStopWords = self.dfTokenize.copy()
        dfStopWords = dfStopWords.textPreProcessing.removeStopWord(colNames=['Tokenized_tweet'])
        dfStopWords['Tokenized_tweet'] = dfStopWords['Tokenized_tweet'].astype(str)
        pd.util.testing.assert_frame_equal(dfStopWords, pd.read_csv("removeStopWord.csv"))
