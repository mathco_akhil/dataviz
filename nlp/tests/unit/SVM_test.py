import unittest
from unittest.mock import patch
import pandas as pd
import sys
import os
import numpy as np 
import pickle

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
from SVM import SVM



class TestSVM(unittest.TestCase):

    def setUp(self):
        self.df1 = pd.read_csv(r"dataset.csv")
        self.df2 = pd.read_csv("svmout.csv")
  #      self.dfTokenize = self.dfRaw.textPreProcessing.tokenization(colNames=['tweet'])
  #     

  #  def test_findContinuousColumns(self):
  #     self.df_nos.dataclean.findContinuousColumns(threshold=20)
  #      self.df_mixed.dataclean.findContinuousColumns(threshold=20)
  #      pd.util.testing.assert_frame_equal(self.df_mixed, pd.DataFrame([[1,2,3,4], [3,4,5,6], [numpy.nan,6,7,8]]))
  #      pd.util.testing.assert_frame_equal(self.df_nos, pd.DataFrame([[1,2,3,4], [3,4,5,6], [5,6,7,8]]))
  #      self.assertEqual(self.df_df1.dataclean.findContinuousColumns(),['Adult Mortality','infant deaths','percentage expenditure','Hepatitis B','Measles ',' BMI ','Life expectancy ','under-five deaths '])

        
    def test_scoreDataset(self):
        df = self.df1
        #df = self.df1[['label','tweet']]
        df['tweet'] = df['tweet'].astype(str)
        df = df.textPreProcessing.tokenization(['tweet'])
        #df = df[['label','Tokenized_tweet']]
        #self.df1['Tokenized tweet'] = [i.replace("'","") for i in self.df1['Tokenized tweet']]
        a = df.featureEngineering.vectorizeTfidf('Tokenized_tweet')
        df['Tokenized_tweet'] = df['Tokenized_tweet'].astype(str)
        log = SVM(df,random_state=101)
        log.fit('label',a[0])
        out = log.scoreDataset(df)
       
        #pd.testing.equal_content(out,self.df2)
        pd.util.testing.assert_frame_equal(out,self.df2)

        
#    def test_stemmer(self):
#         stemmedData=pd.read_csv("stemmer.csv")
#         stemmedData['tweetstemmed'] = stemmedData['tweetstemmed']
#         stemmedData['tweet'] = [i.replace('"', '') for i in stemmedData['tweet']]
# #        dfBeforeStem = pd.read_csv(r"testDataframeStem.csv")
# #        dfBeforeStem['tweet'] = [list(i.split('], [')) for i in dfBeforeStem['tweet']]
#         dfAfterstem = self.dfTokenize.textPreProcessing.stemmer(colNames=['tweet'])
#         #dfAfterstem['tweetstemmed'] = dfAfterstem['tweetstemmed']
#         dfAfterstem['tweet'] = dfAfterstem['tweet']
#         #testStem = self.dfTokenize.textPreProcessing.stemmer(tokenizedColumn=['tweet'])
#         #print(testStem.head())
#         #testStem['tweetstemmed'] = testStem['tweetstemmed']
#         pd.util.testing.assert_frame_equal(dfAfterstem, stemmedData)
#
#    def test_convertLowerCase(self):
#        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.convertLowerCase(colNames=['tweet']), pd.read_csv("convertLowerCase.csv"))
#
#    def test_removeEmail(self):
#        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeEmail(colNames=['tweet']), pd.read_csv("removeEmail.csv"))
#
#
#    def test_removeHyperlink(self):
#        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeHyperlink(colNames=['tweet']), pd.read_csv("removeHyperlink.csv"))
#
#    def test_removeWhiteSpace(self):
#        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeWhiteSpace(colNames=['tweet']), pd.read_csv("removeWhiteSpace.csv"))
#
#    def test_removeDigits(self):
#        pd.util.testing.assert_frame_equal(self.dfRaw.textPreProcessing.removeDigits(colNames=['tweet']), pd.read_csv("removeDigits.csv"))
#
#    def test_removeOtherWords(self):
#        with patch('builtins.input', side_effect="NAN, nan"):
#            dfremoveOtherWords = self.dfRaw.textPreProcessing.removeOtherWords(colNames=['tweet'])
#        pd.util.testing.assert_frame_equal(dfremoveOtherWords, pd.read_csv("removeOtherWords.csv"))

