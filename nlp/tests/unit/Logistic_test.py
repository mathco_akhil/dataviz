import unittest
import pandas as pd
import sys
import os
import pickle
import numpy as np

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

from Logistic import Logistic
import FeatureEngineering

class TestLogisticMethods(unittest.TestCase):

    def setUp(self):
        self.df1= pd.read_csv(r"dataset.csv")
        self.df2 = pd.read_csv('logisticout.csv')
           
        
    def test_scoreDataset(self):
        
        #df = self.df1[['label','tweet']]
        df = self.df1
        df['tweet'] = df['tweet'].astype(str)
        df = df.textPreProcessing.tokenization(['tweet'])
        #df = df[['label','Tokenized_tweet']]
        #self.df1['Tokenized tweet'] = [i.replace("'","") for i in self.df1['Tokenized tweet']]
        a = df.featureEngineering.vectorizeTfidf('Tokenized_tweet')
        df['Tokenized_tweet'] = df['Tokenized_tweet'].astype(str)
        log = Logistic(df,random_state=101,multi_class='auto')
        log.fit('label',a[0])
        out = log.scoreDataset(df)
        #pd.testing.equal_content(out,self.df2)
        pd.util.testing.assert_frame_equal(out,self.df2)

        
        
        