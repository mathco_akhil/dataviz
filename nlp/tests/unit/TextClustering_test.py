import unittest
from unittest.mock import patch
import pandas as pd
import sys
import os
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import LDA
import LSA
import NMF
import FeatureEngineering
import TextPreProcessing
from TopicVisualization import TopicVisualization



class TestTextClustering(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv("clean_dataset_v2.csv")
        self.df = self.df.textPreProcessing.tokenization(["tweet"],by='Word')
        self.matrix,self.vectorizer = self.df.featureEngineering.vectorizeBOW("Tokenized_tweet")
        
# =============================================================================
#     def test_LDA(self):
#         ldaModel = LDA.LDA(self.df,self.matrix,self.vectorizer,random_state=10)
#         print(ldaModel.lda)
#         pd.util.testing.assert_frame_equal(TopicVisualization(model_output=ldaModel.lda,n_components=ldaModel.n_components,matrix=ldaModel.matrix,vectorizer=ldaModel.vectorizer).displayTopic(n_top_words=40),pd.read_csv("lda.csv"))
#         
#     def test_LSA(self):
#         lsaModel = LSA.LSA(self.df,self.matrix,self.vectorizer,random_state=10)
#         pd.util.testing.assert_frame_equal(TopicVisualization(model_output=lsaModel.lsa,n_components=lsaModel.n_components,matrix=lsaModel.matrix,vectorizer=lsaModel.vectorizer).displayTopic(n_top_words=40),pd.read_csv("lsa.csv"))
#         
#     def test_NMF(self):
#         nmfModel = NMF.NMF(self.df,self.matrix,self.vectorizer,random_state=10)
#         pd.util.testing.assert_frame_equal(TopicVisualization(model_output=nmfModel.nmf,n_components=nmfModel.n_components,matrix=nmfModel.matrix,vectorizer=nmfModel.vectorizer).displayTopic(n_top_words=40),pd.read_csv("nmf.csv"))
# 
# =============================================================================

    def test_LDA(self):
        ldaObject = LDA.LDA(self.df,self.vectorizer,random_state=10)
        ldaObject.fit(self.matrix)
        print(ldaObject.lda)
        pd.util.testing.assert_frame_equal(TopicVisualization(modelOutput=ldaObject.lda,n_components=ldaObject.n_components,matrix=ldaObject.matrix,vectorizer=ldaObject.vectorizer).displayTopic(numberOfTopWords=40),pd.read_csv("lda.csv"))
        

# =============================================================================
#     def test_LSA(self):
#          lsaObject = LSA.LSA(self.df,self.vectorizer,random_state=10)
#          lsaObject.fit(self.matrix)
#          print(lsaObject.lsa)
#          pd.util.testing.assert_frame_equal(TopicVisualization(modelOutput=lsaObject.lsa,n_components=lsaObject.n_components,matrix=lsaObject.matrix,vectorizer=lsaObject.vectorizer).displayTopic(numberOfTopWords=40),pd.read_csv("lsa.csv"))
#          
# =============================================================================

    def test_NMF(self):
        nmfObject = NMF.NMF(self.df,self.vectorizer,random_state=10)
        nmfObject.fit(self.matrix)
        print(nmfObject.nmf)
        pd.util.testing.assert_frame_equal(TopicVisualization(modelOutput=nmfObject.nmf,n_components=nmfObject.n_components,matrix=nmfObject.matrix,vectorizer=nmfObject.vectorizer).displayTopic(numberOfTopWords=40),pd.read_csv("nmf.csv"))


