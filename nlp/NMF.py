﻿from sklearn.decomposition import NMF as NonNegativeMatrixVisualization
#from TopicVisualization import TopicVisualization
import pandas as pd



class NMF:
    """    
    This class contains functions for building the Topic Modelling using NMF (Non Negative Matrix Factorization) technique
    """

    def __init__(self, df,vectorizer,n_components=10, init=None, solver='cd', beta_loss='frobenius', tol=0.0001, max_iter=200, random_state=None, alpha=0.0, l1_ratio=0.0, verbose=0, shuffle=False):
        
        """
        The constructor of the class creates an object for class NMF
        Parameters:
        -----------
        df           : pandas_object (dataframe) 
        vectorizer   : tfidf[1] , a TfidfVectorizer object which is generated from vectorizeTfidf function earlier. vectorizeTfidf generates a tuple, where the second element of the tuple is vectorizer object, which is given as input over here. Mostly for NMF modelling technique, Tfidf is preferred as it is based on linear algebra method.
        n_components : int or None Number of topics 
        init         : None | 'random' | 'nndsvd' | 'nndsvda' | 'nndsvdar' | 'custom'  Method used to initialize the procedure. Default: None.
        solver       : 'cd' | 'mu'  Numerical solver to use: ‘cd’ is a Coordinate Descent solver. ‘mu’ is a Multiplicative Update solver.
        beta_loss    : float or string, default 'frobenius' String must be in {'frobenius', 'kullback-leibler', 'itakura-saito'}. Beta divergence to be minimized, measuring the distance between X and the dot product WH. Note that values different from 'frobenius' (or 2) and 'kullback-leibler' (or 1) lead to significantly slower fits. Note that for beta_loss <= 0 (or 'itakura-saito'), the input matrix X cannot contain zeros. Used only in 'mu' solver.
        tol          : float, default: 1e-4 Tolerance of the stopping condition 
        max_iter     : integer, default: 200 Maximum number of iterations 
        random_state : int, RandomState instance or None, optional, default: None . If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. 
        alpha        : double, default: 0 Constant that multiplies the regularization terms. Set it to zero to have no regularization.
        l1_ratio     : double, default: 0. The regularization mixing parameter, with 0 <= l1_ratio <= 1. For l1_ratio = 0 the penalty is an elementwise L2 penalty (aka Frobenius Norm). For l1_ratio = 1 it is an elementwise L1 penalty. For 0 < l1_ratio < 1, the penalty is a combination of L1 and L2.
        verbose      : bool, default=False  
        shuffle      : boolean, default: False  If true, randomize the order of coordinates in the CD solver. 
    
        Examples:
        ---------
        nmfObject = NMF(df,tfidf[1],n_components=10, init=None, solver='cd', beta_loss='frobenius', tol=0.0001, max_iter=200, random_state=None, alpha=0.0, l1_ratio=0.0, verbose=0, shuffle=False)
        """
        self.df = df
        self.n_components=n_components
        self.vectorizer = vectorizer
        self.nmf = NonNegativeMatrixVisualization(n_components=n_components, init=init, solver=solver, beta_loss=beta_loss, tol=tol, max_iter=max_iter, random_state=random_state, alpha=alpha, l1_ratio=l1_ratio, verbose=verbose, shuffle=shuffle)           
    
    

    def fit(self,matrix):
        """
        fit - This function fit the NMF topic modelling to the dataset.
        Parameters:
        -----------
        matrix : tfidf[0] , sparsed matrix which is generated from vectorizeTfidf function earlier. vectorizeTfidf generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques. But, mostly for NMF modelling technique, Tfidf is preferred as it is based on linear algebra method. 
    
        Examples:
        ---------
        nmf_model = nmfObject.fit(tfidf[0])
        >>> Output  : NMF model 
        """
        self.matrix=matrix
        return self.nmf.fit(matrix)
    
    
    def calculateWeightedMatrix(self):
        """
        weightedMatrix - This function gives matrix W, whose each row of the matrix contains weight of each component.
        X ~ WH, where X is the input matrix which contains weighted components
        Parameters:
        -----------
        No parameters required
        
        Examples:
        ---------
        matrixW = nmfObject.weightedMatrix()
        >>> Output  : matrix W which contains weight of the components 
        """
        return self.nmf.transform(self.matrix)
    
    def calculateComponentsMatrix(self):
        """
        componentsMatrix - This function gives matrix H, whose each row of the matrix contains components. 
        X ~ WH, where X is the input matrix which contains weighted components
        Parameters:
        -----------
        No parameters required
        
        Examples:
        ---------
        matrixH = nmfObject.componentsMatrix()
        >>> Output  : matrix H which contains components
        """
        return self.nmf.components_
        
        
