import pandas as pd
from sklearn.linear_model import LogisticRegression
import pickle
#@pd.api.extensions.register_dataframe_accessor("Logistic")
class Logistic(object):
    """
    
    This class builds a logistic classifier  and scores the dataset
    
    """


    def __init__(self, pandas_obj,
                 penalty='l2', 
                 dual=False, 
                 tol=0.0001, 
                 C=1.0, 
                 fit_intercept=True, 
                 intercept_scaling=1, 
                 class_weight=None, 
                 random_state=None, 
                 solver='warn', 
                 max_iter=100, 
                 multi_class='warn', 
                 verbose=0, 
                 warm_start=False, 
                 n_jobs=None):
        """
        The constructor of the class creates an object for Logistic class
        Parameters:
        -----------
        df                : pandas dataframe (the dataset)
        penalty           : str, 'l1', 'l2', 'elasticnet' or 'none', optional (default='l2') Used to specify the norm used in the penalization. The 'newton-cg', 'sag' and 'lbfgs' solvers support only l2 penalties. 'elasticnet' is only supported by the 'saga' solver. If 'none' (not supported by the liblinear solver), no regularization is applied.
        dual              : bool, optional (default=False) Dual or primal formulation. Dual formulation is only implemented for l2 penalty with liblinear solver. Prefer dual=False when n_samples > n_features.
        tol               : float, optional (default=1e-4) tolerance for stopping criteria
        C                 : float, optional (default=1.0) Inverse of regularization strength; must be a positive float. Like in support vector machines, smaller values specify stronger regularization.
        fit_intercept     : bool, optional (default=True) Specifies if a constant (a.k.a. bias or intercept) should be added to the decision function.
        intercept_scaling : float, optional (default=1) Useful only when the solver ‘liblinear’ is used and self.fit_intercept is set to True. In this case, x becomes [x, self.intercept_scaling], i.e. a "synthetic" feature with constant value equal to intercept_scaling is appended to the instance vector. The intercept becomes intercept_scaling * synthetic_feature_weight.
        class_weight      : dict or 'balanced', optional (default=None) Weights associated with classes in the form {class_label: weight}. If not given, all classes are supposed to have weight one. The "balanced" mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as n_samples / (n_classes * np.bincount(y)).
        random_state      : int, RandomState instance or None, optional (default=None) The seed of the pseudo random number generator to use when shuffling the data. If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. Used when solver == 'sag' or 'liblinear'.
        solver            : str, {'newton-cg', 'lbfgs', 'liblinear', 'sag', 'saga'}, optional (default='liblinear').
                            Algorithm to use in the optimization problem.
                            -For small datasets, 'liblinear' is a good choice, whereas 'sag' and 'saga' are faster for large ones.
                            -For multiclass problems, only 'newton-cg', 'sag', 'saga' and 'lbfgs' handle multinomial loss; ‘liblinear’ is limited to one-versus-rest schemes.
                            -'newton-cg', 'lbfgs', 'sag' and 'saga' handle L2 or no penalty
                            -'liblinear' and 'saga' also handle L1 penalty
                            -'saga' also supports ‘elasticnet’ penalty
                            -'liblinear' does not handle no penalty
        max_iter          : int, optional (default=100)Maximum number of iterations taken for the solvers to converge.
        multi_class       : str, {'ovr', 'multinomial', 'auto'}, optional (default='ovr') If the option chosen is 'ovr', then a binary problem is fit for each label. For 'multinomial' the loss minimised is the multinomial loss fit across the entire probability distribution, even when the data is binary. ‘multinomial’ is unavailable when solver='liblinear'. 'auto' selects 'ovr' if the data is binary, or if solver='liblinear' and otherwise selects 'multinomial'.
        verbose           : int, optional (default=0) For the liblinear and lbfgs solvers set verbose to any positive number for verbosity.
        warm_start        : bool, optional (default=False) When set to True, reuse the solution of the previous call to fit as initialization, otherwise, just erase the previous solution. Useless for liblinear solver.
        n_jobs            : int or None, optional (default=None) Number of CPU cores used when parallelizing over classes if multi_class='ovr'. This parameter is ignored when the solver is set to ‘liblinear’ regardless of whether 'multi_class' is specified or not.
        
        Examples:
        ---------
        logisticObject = Logistic(df,penalty='l2',dual=False,tol=0.0001,C=1.0,fit_intercept=True,intercept_scaling=1,class_weight=None,random_state=None,solver='warn',max_iter=100,multi_class='warn',verbose=0,warm_start=False,n_jobs=None)
        """
        self.df = pandas_obj
        self.clf = LogisticRegression(penalty=penalty, 
                                      dual=dual, 
                                      tol=tol, 
                                      C=C, 
                                      fit_intercept=fit_intercept, 
                                      intercept_scaling=intercept_scaling, 
                                      class_weight=class_weight, 
                                      random_state=random_state, 
                                      solver=solver, 
                                      max_iter=max_iter, 
                                      multi_class=multi_class, 
                                      verbose=verbose, 
                                      warm_start=warm_start, 
                                      n_jobs=n_jobs)
     
    def fit(self,depVar,vector):
        """
        fit - This function fit the Logistic Regression model to the dataset.
        Parameters:
        -----------   
        depVar : Dependent variable from the dataset
        vector : bow[0], sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques.
                 For using vectorizeTfidf, give tfidf[0]
                 For using vectorizeHashing, give hashing_matrix
                 For using embedDoc2Vec, give doc2vecMatrix
            
        Examples:
        ---------
        logisticModel = logisticObject.fit(depVar='label',vector=bow[0])
        >>> Output  : Logistic Regression fitted to the dataset
        """
        self.vector = vector
        return self.clf.fit(self.vector,self.df[depVar])
        
    def scoreDataset(self,df):
        """
        scoreDataset - outputs the predicted probablities of the logistic regression model built, the user can input test/ train dataset in order to score the dataset
        Parameters:
        -----------   
        df : pandas dataframe (the dataset)
            
        Examples:
        ---------
        scoreDfLogistic = logisticObject.scoreDataset(df)
        >>> Output : Dataframe with the predicted probablities
        
        """
        model_fit=self.clf
        vectorizeData = self.vector
        scoringTest= model_fit.predict_proba(vectorizeData)
        dataframe_test=pd.DataFrame(scoringTest, columns=['0','1'])
        scoringTest=dataframe_test['1']
        df_d = pd.concat([df, dataframe_test], axis=1)
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
        #df_d.to_csv("logisticout.csv",index=False)
        return(df_d)
         
        
    