from sklearn.decomposition import TruncatedSVD
#from TopicVisualization import TopicVisualization
import pandas as pd


class LSA:
    """
    This class builds the Topic Modelling using LSA (Latent Semantic Analysis) technique and creates a LSA object
    """
    def __init__(self, df,vectorizer,n_components=10, algorithm='randomized', n_iter=5, random_state=None, tol=0.0):
        """
        The constructor of the class creates an object for class LSA
        Parameters:
        -----------
        df           : pandas_object (dataframe) 
        vectorizer   : bow[1] , a CountVectorizer object which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the second element of the tuple is vectorizer object, which is given as input over here.Mostly for LSA modelling technique, bagOfWords is preferred. But, user can try with Tfidf technique also.
        n_components : int, default = 2 number of topics 
        algorithm    : string, default = 'randomized' SVD solver to use. Either 'arpack' for the ARPACK wrapper in SciPy (scipy.sparse.linalg.svds), or 'randomized' for the randomized algorithm
        n_iter       : int, optional (default 5) Number of iterations for randomized SVD solver
        random_state : int, RandomState instance or None, optional, default = None If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. 
        tol          : float, optional Tolerance for ARPACK. 0 means machine precision. Ignored by randomized SVD solver
   
        Examples:
        ---------
        lsaObject = LSA(df,bow[1],n_components=10, algorithm='randomized', n_iter=5, random_state=None, tol=0.0)
        """
        self.df = df
        self.n_components=n_components
        self.vectorizer = vectorizer
        self.lsa = TruncatedSVD(n_components=n_components, algorithm=algorithm, n_iter=n_iter, random_state=random_state, tol=tol)

    
    def fit(self,matrix):
        """
        fit - This function fit the LSA topic modelling to the dataset.
        Parameters:
        -----------
        matrix : bow[0] , sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques. But, mostly for LSA modelling technique, bagOfWords is preferred.    
        Examples:
        ---------
        lsa_model = lsaObject.fit(bow[0])
        >>> Output  : LSA model 
        """
        self.matrix=matrix
        return self.lsa.fit(matrix)
    

        
