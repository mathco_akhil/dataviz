import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from bisect import bisect_left, bisect_right
from sklearn.metrics import confusion_matrix, roc_curve, auc, accuracy_score

@pd.api.extensions.register_dataframe_accessor("ModelEvaluationMetrices")
class ModelEvaluationMetrices(object):
    """
     Contains functions that helps in evaluating the model built
    1. createGains() - generates gains table as default and plot if parameter plot = True
    2. createKs() - generates gains table as default and plot if parameter plot = True
    3. getConcordance() - Generates dataframe with concordance, discordance and tie pairs
    4. generateRoc() - Generates Roc curve and optimal threshold
    5. generateConfusionMatrix() - Generates confusion matrix 

    """

    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def createGains(self,scoring,depVar,plot=False):
        """
        This function takes dataframe and dependent variable as input and 
        generates gains table or plot depending on the parameter passed

        Parameters
        ----------
        scoring : Predicted probability of model.
        depVar : Dependent variable to be used in the model.
        plot : False by default,
               if changed to True ,it will return gains plot.             
        
        Usage
        --------
        df.modelEvaluationMetrices.createGains(scoring,
                                             depVar="<dependent column name>")
        Here, scoring is the Pred_Prob column of the dataset. Example: scoreDfLogistic['Pred_Prob']
        Returns
        -------
        Gains table if parameter plot=False
        Gains plot if parameter plot=True
        """
        self._obj["Pred_Prob"] = scoring
        test = self._obj.sort_values(by='Pred_Prob', ascending=False)
        test['bucket'] = pd.qcut(test.Pred_Prob, 10, duplicates='drop')
        test['rank'] = test['Pred_Prob'].rank(method='first')
        test['bucket'] = pd.qcut(test['rank'].values, 10).codes
        test["DepVar"] = test[depVar]
        test["Non_Event"] = np.where(test["DepVar"] == 0, 1, 0)
        grouped = test.groupby('bucket', as_index=True)
        agg1 = pd.DataFrame()
        agg1['min_score'] = grouped.min().Pred_Prob
        agg1['max_scr'] = grouped.max().Pred_Prob
        agg1['DepVar'] = grouped.sum().DepVar
        agg1['Non_Event'] = grouped.sum().Non_Event
        agg1['total'] = agg1.DepVar + agg1.Non_Event
        agg2 = agg1.iloc[:: -1]
        pd.options.mode.chained_assignment = None
        agg2.is_copy
        agg2['Cumulative_Sum'] = agg2['DepVar'].cumsum()
        agg2['Perc_of_Events'] = (agg2.DepVar / agg2.DepVar.sum()) * 100
        agg2['Gain'] = agg2['Perc_of_Events'].cumsum()
        agg2.reset_index(drop=True, inplace=True)
        agg2["Bucket"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        agg2.min_score = agg2.min_score.round(3)
        agg2.max_scr = agg2.max_scr.round(3)
        agg2.Perc_of_Events = agg2.Perc_of_Events.round(3)
        agg2.Gain = agg2.Gain.round(3)
        cols = ['min_score', 'max_scr', 'DepVar', 'Non_Event', 'total',
                'Cumulative_Sum', 'Perc_of_Events', 'Gain', 'Bucket']
        df2 = pd.DataFrame(columns=cols, index=range(1))
        for a in range(1):
            df2.loc[a].min_score = 0
            df2.loc[a].max_scr = 0
            df2.loc[a].DepVar = 0
            df2.loc[a].Non_Event = 0
            df2.loc[a].total = 0
            df2.loc[a].Cumulative_Sum = 0
            df2.loc[a].Perc_of_Events = 0
            df2.loc[a].Gain = 0
            df2.loc[a].Bucket = 0
        frames = [df2, agg2]
        gains = pd.concat(frames, ignore_index=True)
        if plot:
            figgains, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            line_up, = ax1.plot(gains['Gain'], 'o-',
                                color="#ED7038", linewidth=2, label="Gain")
            line_down, = ax2.plot(gains['Bucket'], 'k--', linewidth=2, label="Random")
            ax1.grid(b=True, which='both', color='0.65', linestyle='-')
            ax1.set_xlabel('Buckets')
            ax1.set_ylabel("Gain with model")
            ax2.set_ylabel('Random gain')
            plt.title('Gains Chart')
            plt.legend(handles=[line_up, line_down], loc="lower right")
            ax2.set_yticklabels(["0", "0", "20", "40", "60", "80", "100"])
        if plot==False:
            return(gains)
        elif plot==True:
            return(figgains)


    def createKs(self, scoring, depVar,plot=False):
        """
        This function takes dataframe and dependent variable as input
        and generates ks table to extract ks value from it,
        it plots ks table depending on the parameter passed i.e plot=True
        
        Parameters
        ----------
        depVar : Dependent variable to be used in the model.
        plot : False by default,
               if changed to true ,it will return Ks plot                                 
        Usage
        --------
        df.modelEvaluationMetrices.createKs(scoring,depVar="<dependent column name>")
        Here, scoring is the Pred_Prob column of the dataset. Example: scoreDfLogistic['Pred_Prob']
        Returns
        -------
        Ks value by default
        Ks plot if parameter plot = True
        """    
        self._obj["Pred_Prob"] = scoring        
        ks = self._obj.sort_values(by='Pred_Prob', ascending=False)
        ks['bucket'] = pd.qcut(self._obj.Pred_Prob, 10, duplicates='drop')
        ks['rank'] = ks['Pred_Prob'].rank(method='first')
        ks['bucket'] = pd.qcut(ks['rank'].values, 10).codes
        ks["Event"] = self._obj[depVar]
        ks['NonEvent'] = np.where(ks[depVar] == 0, 1, 0)
        grouped = ks.groupby('bucket', as_index=True)
        ks_table1 = pd.DataFrame()
        ks_table1['min_score'] = grouped.min().Pred_Prob
        ks_table1['max_scr'] = grouped.max().Pred_Prob
        ks_table1['Event'] = grouped.sum().Event
        ks_table1['NonEvent'] = grouped.sum().NonEvent
        ks_table1['total'] = ks_table1.Event + ks_table1.NonEvent
        ks_table2 = ks_table1.iloc[:: -1]
        pd.options.mode.chained_assignment = None
        ks_table2.is_copy
        ks_table2['Cumulative_Sum_Event'] = ks_table2['Event'].cumsum()
        ks_table2['Perc_of_Events'] = (
        ks_table2.Event / ks_table2.Event.sum()) * 100
        ks_table2['Cumulative_Percent_Event'] = ks_table2['Perc_of_Events'].cumsum()
        ks_table2['Cumulative_Sum_NonEvent'] = ks_table2['NonEvent'].cumsum()
        ks_table2['Perc_of_NonEvents'] = (
        ks_table2.NonEvent / ks_table2.NonEvent.sum()) * 100
        ks_table2['Cumulative_Percent_NonEvent'] = ks_table2['Perc_of_NonEvents'].cumsum()
        ks_table2['ks'] = np.round(
        ks_table2.Cumulative_Percent_Event -
        ks_table2.Cumulative_Percent_NonEvent)
        cols = ['min_score','max_scr',
                'Event','Non_Event','total',
                'Cumulative_Sum_Event',
                'Perc_of_Events','Cumulative_Percent_Event',
                'Cumulative_Sum_NonEvent','Perc_of_NonEvents',
                'Cumulative_Percent_NonEvent','ks']
        df2 = pd.DataFrame(columns=cols, index=range(1))
        for a in range(1):
            df2.loc[a].min_score = 0
            df2.loc[a].max_scr = 0
            df2.loc[a].DepVar = 0
            df2.loc[a].Non_Event = 0
            df2.loc[a].total = 0
            df2.loc[a].Cumulative_Sum_Event = 0
            df2.loc[a].Perc_of_Events = 0
            df2.loc[a].Cumulative_Percent_Event = 0
            df2.loc[a].Cumulative_Sum_NonEvent = 0
            df2.loc[a].Perc_of_NonEvents = 0
            df2.loc[a].Cumulative_Percent_NonEvent = 0
            df2.loc[a].ks = 0
        frames = [df2, ks_table2]
        result = pd.concat(frames, ignore_index=True)
        ks_table = pd.DataFrame(ks_table2)
        ks_table["Bucket"] = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
        ks_table = ks_table.set_index('Bucket')
        ksvalue = ks_table.ks.max()
        ksvalue = round(ksvalue, 2)
        def flag(x): return '<----(max)' if x == result.ks.max() else ''
        ks_table['max_ks'] = result.ks.apply(flag)
        #print(ks_table)
        if plot == True:
            figks, ax1 = plt.subplots()
            ax2 = ax1.twinx()
            line_down, = ax2.plot(
                            result['Cumulative_Percent_NonEvent'], 'k-',
                            linewidth=2, label="Non event")
            line_up, = ax1.plot(result['Cumulative_Percent_Event'],
                            'g-', color="#ED7038", linewidth=2, label="Event")
            ax1.grid(b=True, which='both', color='0.65', linestyle='-')
            ax1.set_xlabel('Buckets')
            ax1.set_ylabel("Event")
            ax2.set_ylabel('Non Event')
            plt.title('K-S Chart')
            ksvalue = str(ksvalue)
            bbox = dict(boxstyle="round", fc="white")
            plt.annotate("KS: " + ksvalue, xy=(9.6, 22),
                         ha='center', va='center', bbox=bbox)
            plt.legend(handles=[line_up, line_down], loc="lower right")
            ax2.set_yticklabels([])
        if plot==False:
            print("Ks value :",ksvalue)
            return(ksvalue)
        elif plot==True:
            return(figks)


    def getConcordance(self, scoring,depVar):
        """
        This function takes dataframe and dependent variable name as input 
        and generates - concordance, discordance and tie pairs
        
        Parameters
        ----------
        depVar : Dependent variable to be used in the model.
        
        Usage
        --------
        df.ModelEvaluationMetrices.getConcordance(scoring,depVar="<dependent column name>")
        Here, scoring is the Pred_Prob column of the dataset. Example: scoreDfLogistic['Pred_Prob']
        Returns
        -------
        pandas dataframe with concordance, discordance and tie pairs.
        """    
        
        self._obj["Pred_Prob"] = scoring        
        zeros = self._obj[(self._obj[depVar] == 0)].reset_index().drop(
            ['index'], axis=1)
        ones = self._obj[(self._obj[depVar] == 1)].reset_index().drop(['index'], axis=1)
        pred_prob = "Pred_Prob"
        zeros2 = zeros[[depVar, pred_prob]].copy()
        ones2 = ones[[depVar, pred_prob]].copy()
        zeros2_list = sorted([zeros2.iloc[j, 1] for j in zeros2.index])
        ones2_list = sorted([ones2.iloc[i, 1] for i in ones2.index])
        zeros2_length = len(zeros2_list)
        ones2_length = len(ones2_list)
        conc = 0
        ties = 0
        disc = 0
        for i in zeros2.index:
            cur_disc = bisect_left(ones2_list, zeros2.iloc[i, 1])
            cur_ties = bisect_right(ones2_list, zeros2.iloc[i, 1]) - cur_disc
            disc += cur_disc
            ties += cur_ties
            conc += ones2_length - cur_ties - cur_disc
        pairs_tested = zeros2_length * ones2_length
        concordance = conc / pairs_tested
        discordance = disc / pairs_tested
        ties_perc = ties / pairs_tested
        print("Concordance = ", (concordance * 100), "%")
        print("Discordance = ", (discordance * 100), "%")
        print("Tied = ", (ties_perc * 100), "%")
        print("Pairs = ", pairs_tested)
        dict1 = {"Concordance": concordance, 'Discordance': discordance, 'Ties': ties_perc}
        return(pd.DataFrame.from_dict(dict1,orient='index'))
    
    def generateRoc(self,scoring,depVar):
        """
        This function takes dataframe,prediction and dependent variable column
        name as input to generate ROC curve and optimal threshold.
        
        Parameters
        ----------
        depVar : Dependent variable to be used in the model.
        scoring : Predicted probability of model.
            
        Usage
        --------
        df.ModelEvaluationMetrices.generateRoc(scoring,depVar="<dependent column name>")
        Here, scoring is the Pred_Prob column of the dataset. Example: scoreDfLogistic['Pred_Prob']
        Returns
        -------
        Roc Plot
        """    
        y_test = self._obj[depVar]
        fpr, tpr, thresholds = roc_curve(y_test, scoring)
        roc_auc = auc(fpr, tpr)
        print("\n Area under the ROC curve : %f" % roc_auc)
        i = np.arange(len(tpr))  # index for df
        roc = pd.DataFrame({'fpr': pd.Series(fpr,
                                             index=i),
                            'tpr': pd.Series(tpr,
                                             index=i),
                            '1-fpr': pd.Series(1 - fpr,
                                               index=i),
                            'tf': pd.Series(tpr - (1 - fpr),
                                            index=i),
                            'thresholds': pd.Series(thresholds,
                                                    index=i)})
        print("\n \n", roc.iloc[(roc.tf - 0).abs().argsort()[:1]])
        r = roc.iloc[(roc.tf - 0).abs().argsort()[:1]]
        plot_threshold = r["thresholds"]
        plot_threshold = list(plot_threshold)
        plot_threshold = " ".join(str(x) for x in plot_threshold)
        plot_threshold = float(plot_threshold)  
        fig, ax1 = plt.subplots()
        ax2 = ax1.twinx()
        ax1.plot(roc['tpr'], 'g-', color="#ED7038", linewidth=2)
        ax2.plot(roc['1-fpr'], 'k-', label='Threshold=(%0.2f)' %
                 plot_threshold, linewidth=2)
        ax1.grid(b=True, which='both', color='0.65', linestyle='-')
        ax1.set_xlabel('Probability')
        ax1.set_ylabel("Sensitivity")
        ax2.set_ylabel('Specificity')
        plt.title('Receiver Operating Characteristic')
        plt.legend(loc="lower center")
        ax1.set_xticklabels([])
        return(fig)    
    
    def generateConfusionMatrix(self,scoring, depVar,threshold=0.5):
        """
        This function takes dataframe,prediction, dependent variable
        and threshold as user input as input to generate Confusion Matric.
        
        Parameters
        ----------
        depVar : Dependent variable to be used in the model.
        scoring : Predicted probability of model.
        Threshold : optimal threshold to generate confusion matrix,
                    can be obtained from ROC
        Usage
        --------
        df.ModelEvaluationMetrices.generateRoc(scoring,depVar="<dependent column name>"
                                 ,threshold)
        Here, scoring is the Pred_Prob column of the dataset. Example: scoreDfLogistic['Pred_Prob']
        Returns
        -------
        Confusion Matrix
        """    
        y_true = self._obj[depVar]
        y_pred = np.where(scoring >= threshold, 1, 0)
        test_confuse = confusion_matrix(y_true, y_pred)
        print("Confusion Matrix : ")
        conf_matrix = pd.DataFrame(test_confuse)
        print(conf_matrix)
        print(" ")
        tp = conf_matrix[1][1]
        fp = conf_matrix[1][0]
        fn = conf_matrix[0][1]
        tn = conf_matrix[0][0]
        accuracy_test = accuracy_score(y_true, y_pred)
        precision_test = tp / (tp + fp)
        sensitivity_test = tp / (tp + fn)
        specificity_test = tn / (tn + fp)
        print("--------------------------------------------")
        print("Accuracy : ", accuracy_test)
        print("Precision : ", precision_test)
        print("Sensitivity: ", sensitivity_test)
        print("Specificity: ", specificity_test)
        print("--------------------------------------------")
        return(conf_matrix)
