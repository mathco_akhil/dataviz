from sklearn.decomposition import LatentDirichletAllocation
import pandas as pd



class LDA:
    """    
    This class contains functions for building the Topic Modelling using LDA (Latent Dirichlet Allocation) technique,calculating evaluation metrics like perplexity and log likelihood
    """
    def __init__(self,df,vectorizer,n_components=10, doc_topic_prior=None, topic_word_prior=None, learning_method='batch', learning_decay=0.7, learning_offset=10.0, max_iter=10, batch_size=128, evaluate_every=-1, total_samples=1000000.0, perp_tol=0.1, mean_change_tol=0.001, max_doc_update_iter=100, n_jobs=None, verbose=0, random_state=None):
        """
        The constructor of the class creates an object for class LDA 
        Parameters:
        -----------
        df                  :  pandas_object (dataframe) (the dataset) 
        vectorizer          :  bow[1] , a countVectorizer object which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the second element of the tuple is vectorizer object, which is given as input over here.Mostly for LDA modelling technique, bagOfWords is preferred as it is a probabilistic model.
        n_components        :  int, optional (default=10) Number of topics.
        doc_topic_prior     :  float, optional (default=None) Prior of document topic distribution- theta.If the value is None, defaults to 1 / n_components 
        topic_word_prior    :  float, optional (default=None) Prior of topic word distribution - beta.If the value is None, defaults to 1 / n_components
        learning_method     : 'batch' | 'online', default='batch'  Method used to update _component. Only used in fit method. In general, if the data size is large, the online update will be much faster than the batch update.
        learning_decay      :  float, optional (default=0.7) It is a parameter that control learning rate in the online learning method. The value should be set between (0.5, 1.0] 
        learning_offset     :  float, optional (default=10.) A (positive) parameter that downweights early iterations in online learning. It should be greater than 1.0 
        max_iter            :  integer, optional (default=10) The maximum number of iterations
        batch_size          :  int, optional (default=128) Number of documents to use in each EM iteration. Only used in online learning.
        evaluate_every      :  int, optional (default=0) How often to evaluate perplexity. Only used in fit method. set it to 0 or negative number to not evalute perplexity in training at all. Evaluating perplexity can help you check convergence in training process, but it will also increase total training time.
        total_samples       :  int, optional (default=1e6) Total number of documents. Only used in the partial_fit method.
        perp_tol            :  float, optional (default=1e-1) Perplexity tolerance in batch learning. Only used when evaluate_every is greater than 0. 
        mean_change_tol     :  float, optional (default=1e-3) Stopping tolerance for updating document topic distribution in E-step
        max_doc_update_iter :  int (default=100) Max number of iterations for updating document topic distribution in the E-step.
        n_jobs              :  int or None, optional (default=None) The number of jobs to use in the E-step.
        verbose             :  int, optional (default=0)  Verbosity level.
        random_state        :  int, RandomState instance or None, optional (default=None)  If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. 

            
        Examples:
        ---------
        ldaObject = LDA(df,bow[1],n_components=10, doc_topic_prior=None, topic_word_prior=None, learning_method='batch', learning_decay=0.7, learning_offset=10.0, max_iter=10, batch_size=128, evaluate_every=-1, total_samples=1000000.0, perp_tol=0.1, mean_change_tol=0.001, max_doc_update_iter=100, n_jobs=None, verbose=0, random_state=None)
        """
        self.df = df
        self.n_components=n_components
        self.vectorizer = vectorizer
        self.lda = LatentDirichletAllocation(n_components=n_components, doc_topic_prior=doc_topic_prior, topic_word_prior=topic_word_prior, learning_method=learning_method, learning_decay=learning_decay, learning_offset=learning_offset, max_iter=max_iter, batch_size=batch_size, evaluate_every=evaluate_every, total_samples=total_samples, perp_tol=perp_tol, mean_change_tol=mean_change_tol, max_doc_update_iter=max_doc_update_iter, n_jobs=n_jobs, verbose=verbose, random_state=random_state)          
    
    def fit(self,matrix):
        """
        fit - This function fit the LDA topic modelling to the dataset.
        Parameters:
        -----------
        matrix : bow[0] , sparsed matrix which is generated from vectorizeBOW function earlier. vectorizeBOW generates a tuple, where the first element of the tuple is matrix, which is given as input over here. User can give matrix generated from other vectorization techniques. But, mostly for LDA modelling technique, bagOfWords is preferred as it is a probabilistic model.

            
        Examples:
        ---------
        lda_model = ldaObject.fit(bow[0])
        >>> Output  : LDA model 
        """
        self.matrix=matrix
        return self.lda.fit(matrix)

     
    def evaluateModel(self):
        """
        evaluateModel - This function generates evaluation metrics for LDA model. A model with higher log-likelihood and lower perplexity (exp(-1. * log-likelihood per word)) 
        is considered to be good.
        Parameters:
        -----------
        No parameters required to pass.
        
        Examples:
        ---------
        ldaObject.evaluateModel()
        >>> Output: generates log likelihood and perplexity metrics
        """
        ldaModel=self.lda
        matrix=self.matrix
        # Log Likelihood: Higher the better
        print("Log Likelihood: ", ldaModel.score(matrix))
 
        # Perplexity: Lower the better. Perplexity = exp(-1. * log-likelihood per word)
        print("Perplexity: ", ldaModel.perplexity(matrix))
 
        # See model parameters
        print(ldaModel.get_params())

        
         
    




 
