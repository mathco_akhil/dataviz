from setuptools import setup, find_packages
import re, ast

#get version from version variable in plaid_integration/init.py
_version_re = re.compile(r'version\s+=\s+(.*)')

def parse_requirements(filename= "requirements.txt"):
    """ load requirements from a pip requirements file """
    lineiter = (line.strip() for line in open(filename))
    return [line for line in lineiter if line and not line.startswith("#")]
version = '0.1.0'

setup(
	name='mathcomock',
	version=version,
	description='MathCo Mock',
	author='Akhil Saraswat',
	author_email='akhil.saraswat@themathcompany.com',
	packages=find_packages(),
	zip_safe=False,
	include_package_data=True,
	install_requires=parse_requirements(),
	dependency_links=["https://github.com/explosion/spacy-models/releases/download/en_core_web_sm-2.0.0/en_core_web_sm-2.0.0.tar.gz#egg=en_core_web_sm"]
	)