import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import AffinityPropagation

@pd.api.extensions.register_dataframe_accessor("affinityPropagation")

class affinityPropagation:
    """
    Contains functions that help with building, predicting and visualising affinity propagation model
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualizeCluster() - returns visualised plot with cluster labels
    Parameters
    ----------
    pandas_obj : A pandas DataFrame
    """
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def buildModel(self, affinity_ap='euclidean', convergence_iter_ap=15, copy_ap=True, damping_ap=0.5, max_iterations=250, preference_ap=None, verbose_ap=False):
        
        """
        This function builds affinity prpoagation model with given hyperparameters.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        affinity_ap : Which affinity to use. At the moment precomputed and euclidean are supported. euclidean uses the negative squared euclidean distance between points.
        
        convergence_iter_ap : Number of iterations with no change in the number of estimated clusters that stops the convergence.
        
        copy_ap : Make a copy of input data.
        
        damping_ap : Damping factor (between 0.5 and 1) is the extent to which the current value is maintained relative to incoming values (weighted 1 - damping). This in order to avoid numerical oscillations when updating these values (messages).
        
        max_iterations : Maximum number of iterations.
        
        preference_ap : Preferences for each point - points with larger values of preferences are more likely to be chosen as exemplars. The number of exemplars, ie of clusters, is influenced by the input preferences value. If the preferences are not passed as arguments, they will be set to the median of the input similarities.
        
        verbose_ap : Boolean input - Whether to be verbose.
        
        Usage
        --------
        df.AffinityPropagation.buildModel()

        Returns
        -------
        Model object
   
        """        
        ap = AffinityPropagation(affinity = affinity_ap, convergence_iter = convergence_iter_ap, copy = copy_ap, damping=damping_ap, max_iter=max_iterations, preference = preference_ap, verbose = verbose_ap)
        ap_model = ap.fit(self._obj)
        #filename = 'buildModelAP.pkl'
        #pickle.dump(ap_model, open(filename, 'wb'))
        return(ap_model)

        
    def predictCluster(self,ap_model):
        """
        Generates dataframe with appended cluster labels.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        ap_model : model object
        
        Usage
        --------
        df.AffinityPropagation.predictCluster()

        Returns
        -------
        Dataframe with cluster labels
   
        """ 
        df = self._obj.copy()
        clust_labels = ap_model.predict(df)
        ap_centers = ap_model.cluster_centers_
        self.clusters = pd.DataFrame(clust_labels)
        df.insert((df.shape[1]),'clusters',self.clusters)
        self.centers = pd.DataFrame(ap_centers, columns= df.columns[:-1])
        return(df)
        
        
    def visualizeCluster(self,var1,var2,model):
        """
        Generates visualised plot for affinity propagation.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        var1 : x axis
        
        var2 : y axis
            
        Usage
        --------
        df.AffinityPropagation.visualizeCluster()

        Returns
        -------
        Figure object
   
        """   
        df = self.predictCluster(model)
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7) 
        cmap = plt.cm.get_cmap('jet')
        
        for i, clusters in df.groupby('clusters'):
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
              
        ax2.legend()
    
        ax2.scatter(self.centers.loc[:, var1], self.centers.loc[:, var2], marker='*',c="black", alpha=1, s=200, edgecolor='k',cmap='viridis')
         
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("Affinity Propagation"),fontsize=14, fontweight='bold')
        
        return(plt)