import pandas as pd
import matplotlib.pyplot as plt
from sklearn.cluster import MeanShift, estimate_bandwidth
import pickle

@pd.api.extensions.register_dataframe_accessor("meanShift")

class meanShift:
    """
    Contains functions that help with building, predicting and visualising mean shift model
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualizeCluster() - returns visualised plot with cluster labels
    Parameters
    ----------
    pandas_obj : A pandas DataFrame
    """
    def __init__(self,pandas_obj):
        self._obj = pandas_obj
     
        
    def buildModel(self,bandwidth_ms = 1, seeds_ms = None, bin_seeding_ms = True, min_bin_freq_ms = 1, cluster_all_ms = True, n_jobs_ms = 1):
        """
        This function builds mean shift model with given hyperparameters.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        bandwidth_ms : Bandwidth used in the RBF kernel.
        
        seeds_ms : Seeds used to initialize kernels. If not set, the seeds are calculated by clustering.
        
        bin_seeding_ms : If true, initial kernel locations are not locations of all points, but rather the location of the discretized version of points, where points are binned onto a grid whose coarseness corresponds to the bandwidth. Setting this option to True will speed up the algorithm because fewer seeds will be initialized. default value: False Ignored if seeds argument is not None.
        
        min_bin_freq_ms : To speed up the algorithm, accept only those bins with at least min_bin_freq points as seeds. If not defined, set to 1.
        
        cluster_all_ms : If true, then all points are clustered, even those orphans that are not within any kernel. Orphans are assigned to the nearest kernel. If false, then orphans are given cluster label -1.
        
        n_jobs_ms : The number of jobs to use for the computation. This works by computing each of the n_init runs in parallel.
        
        Usage
        --------
        df.meanShift.buildModel()

        Returns
        -------
        Model object
   
        """        

        ms = MeanShift(bandwidth = bandwidth_ms, seeds = seeds_ms, bin_seeding = bin_seeding_ms, min_bin_freq = min_bin_freq_ms, cluster_all = cluster_all_ms, n_jobs = n_jobs_ms)
        ms_model = ms.fit(self._obj)
        #filename = 'buildModelMS.pkl'
        #pickle.dump(ms_model, open(filename, 'wb'))
        return(ms_model)
        
        
    def predictCluster(self,ms_model):
        """
        Generates dataframe with appended cluster labels.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        ap_model : model object
        
        Usage
        --------
        df.meanShift.predictCluster()

        Returns
        -------
        Dataframe with cluster labels
   
        """         
        df = self._obj.copy()
        clust_labels = ms_model.predict(df)
        ms_centers = ms_model.cluster_centers_
        self.clusters = pd.DataFrame(clust_labels)
        df.insert((df.shape[1]),'clusters',self.clusters)
        self.centers = pd.DataFrame(ms_centers, columns= df.columns[:-1])
        return(df)
    
    
    def visualizeCluster(self,var1,var2,model):
        """
        Generates visualised plot for affinity propagation.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        var1 : x axis
        var2 : y axis
            
        Usage
        --------
        df.meanShift.visualizeCluster()

        Returns
        -------
        Figure object
   
        """ 
        df = self.predictCluster(model)
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7) 
        cmap = plt.cm.get_cmap('jet')
        
        for i, clusters in df.groupby('clusters'):
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
              
        ax2.legend()
    
        ax2.scatter(self.centers.loc[:, var1], self.centers.loc[:, var2], marker='*',c="black", alpha=1, s=200, edgecolor='k',cmap='viridis')
         
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("Mean Shift"),fontsize=14, fontweight='bold')
        
        return(plt)