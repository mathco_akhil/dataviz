"""
Created on Wed Mar 27 14:51:29 2019

@author: Akhil Saraswat
"""
# pandas is a Python package providing fast, flexible, and expressive data structures designed to make working with “relational” or “labeled” data both easy and intuitive
import pandas as pd
# numpy is a Python library that provides a multidimensional array object, various derived objects (such as masked arrays and matrices), and an assortment of routines for fast operations on arrays
import numpy as np
# pickle is a python library that saves the model into a file


# plotting packages
                                              
# matplotlib.pyplot is a collection of command style functions
import matplotlib.pyplot as plt
# This module provides a large set of colormaps, functions for registering new colormaps and for getting a colormap by name, and a mixin class for adding color mapping functionality
import matplotlib.cm as cm
# plotly.graph_objs is a collection of commands that can be used to plot various visualizations
import plotly.graph_objs as go
# plotly.offline allows you to create graphs offline and save them locally
from plotly.offline import plot,iplot

# Model building packages
# collections module implements high-performance container datatypes (beyond the built-in types list, dict and tuple) and contains many useful data structures
import collections as cc
# sklearn.cluster contains modules that can be used for clustering
from . import KMeds

# MISC
# sklearn.metrics module implements several loss, score, and utility functions to measure classification performance
from sklearn.metrics import silhouette_samples, silhouette_score

@pd.api.extensions.register_dataframe_accessor("KMedoids")
class KMedoidsCluster(object):
    """
    This class bulds predict and visualize KMedoids clustering
    Contains functions to build, predict and visualise birch 
    1. buildModel() - Builds KMedoids model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns visualised plot with cluster labels
    4. visualizeSilhouette() - This function is used to display silhouette plot
    5. buildClusterInfo() - This user defined function prints the summary of the cluster such as population and centers of the cluster
    
    """   
    def __init__(self, pandas_obj):
        self._obj = pandas_obj


    def buildModel(self, n_clusters=8, max_iter=300, tol=0.0001, random_state=None):
        """
        Apply KMedoids by fitting data with number of clusters and compute kmedoids clustering.
        
        Parameters
        ----------
        self : pandas dataframe
        
        n_clusters : int, optional, default: 8
        The number of clusters to form as well as the number of centroids to generate.
        
        max_iter : int, default: 300
        Maximum number of iterations of the k-means algorithm for a single run.
    
        tol : float, default: 1e-4
        Relative tolerance with regards to inertia to declare convergence
        
        random_state : int, RandomState instance or None (default)
        Determines random number generation for centroid initialization. Use an int to make the randomness deterministic.
        
        Usage
        --------
        df.KMedoids.buildModel()

        
        Returns
        -------
        fitted model object
        """
        # Make a copy of this object’s indices and data
        df = self._obj.copy()
    # building KMeans module with the user defined Number of clusters
        kmeds = KMeds.KMedoids(n_clusters=n_clusters, max_iter=max_iter, tol=tol, random_state=random_state)
    # fitting the KMeans on the dataframe 
        kmeds.fit(df.values)
        return(kmeds)
        
        
    def predictCluster(self, model):
        """
        This function will retrun the dataframe with cluster column that
        contains cluster number of each row.
        
        Parameters
        ----------
        self : pandas dataframe
        
        model : Fitted Kmedoids cluster model
        
        Usage
        --------
        df.KMedoids.predictCluster()

        
        Returns
        -------
        pandas dataframe
        """
        labels = model.predict(self._obj)
        df = self._obj.copy()
        df['clusters'] = labels
        return df
       
    def visualizeCluster(self,var1,var2,model):
        """
        This function used to plot KMedoids cluster visualization
        
        Parameters
        ----------
        self : pandas dataframe
        
        var1: X axis variable
        
        var2: Y axis variable
        
        model: Instance of fitted KMedoids model 
        
        Usage
        --------
        df.KMedoids.visualizeCluster()

        
        Returns
        -------
        matplotlib object
        
        """
        centers = pd.DataFrame(model.cluster_centers_,columns=self._obj.columns)
        df = self.predictCluster(model)
    # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7)
    # returns a jet colormap   
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):  
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.legend()
        ax2.scatter(centers.loc[:, var1], centers.loc[:, var2], marker='*',
                    c="black", alpha=1, s=200, edgecolor='k',cmap='viridis')
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt
    
    def buildClusterInfo(self,model):
        """
        This user defined function prints the summary of the cluster such as population and centers of the cluster
        
        Parameters
        ----------
        self: pandas dataframe
        
        model: Instance of fitted KMedoids model 
        
        Usage
        --------
        df.KMedoids.buildClusterInfo()

        
        Returns
        -------
        pandas dataframe   
        """
        
        df = self.predictCluster(model)
        centers = model.cluster_centers_
    # counts population for each cluster
        population = dict(cc.Counter(df.clusters))
        cluster_info = pd.DataFrame(list(population.keys()))
    # storing centers of clusters in a dataframe
        cluster_info['Cluster Center'] = list(centers.round(4))
    # storing population of clusters in a dataframe
        cluster_info["Population"] = list(population.values())
        return cluster_info
    
    def visualizeElbow(self, min_clust=2, max_clust=11):
        """
        This user  definrd function is display elbow plot.
        
        Parameters
        ----------
        self: pandas dataframe
        
        min_clust: minimum number of clusters. Default value is 2
        
        max_clust: maximum number of clusters. Default value is 11
        
        Usage
        --------
        df.KMedoids.visualizeElbow()

        
        Returns
        -------
        Plotly object  
        """
        df = self._obj.copy()
    # creating empty dictionary to store within sum of square error for each cluster
        sse = {}
        for k in range(min_clust, max_clust):
    # Creates k clusters for the given data frame 
            kmeds = KMeds.KMedoids(n_clusters=k, max_iter=1000)
            kmeds.fit(df.values)
            labels = kmeds.labels_
            df["clusters"] = labels
            sse[k] = kmeds.tot_cost
            
        layout = go.Layout(width = 1100,height = 500,title = 'Elbow Plot for Clustering',yaxis=dict(title='Sum of Square Error'),xaxis=dict(title='Cluster Count'))
        trace = go.Scatter(x=list(sse.keys()),y=list(sse.values()))
        data = [trace]
        fig = go.Figure(data=data,layout=layout)
        return fig
    
    def visualizeSilhouette(self,n_clusters,var1,var2):
        """
        This function is used to display silhouette plot a measure of how close each point in one cluster is to points in the neighboring clusters and thus provides a way to assess parameters like number of clusters visually. 
        
        Parameters
        ----------
        df: dataframe on which KMedoids clustering needs to be run
        
        n_clusters: max number of number of clusters
        
        var1: X axis variable
        
        var2: Y axis variable
        
        Usage
        --------
        df.KMedoids.visualizeSilhouette()

        
        Returns
        -------
        matplotlib object 
        """
        
        df = self._obj.copy()
        for n_clusters in range(2,n_clusters+1):
    # Create a subplot with 1 row and 2 columns
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(18, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
            ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
            ax1.set_ylim([0, len(df) + (n_clusters + 1) * 10])
    
    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
            clusterer = KMeds.KMedoids(n_clusters=n_clusters)
            clusterer.fit(df.values)
            cluster_labels = clusterer.labels_
    
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
            silhouette_avg = silhouette_score(df, cluster_labels)
            print("For n_clusters =", n_clusters,
                   "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(df, cluster_labels)
            y_lower = 10
            for i in range(n_clusters):
    # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
                ith_cluster_silhouette_values = \
                    sample_silhouette_values[cluster_labels == i]
    
                ith_cluster_silhouette_values.sort()
    
                size_cluster_i = ith_cluster_silhouette_values.shape[0]
                y_upper = y_lower + size_cluster_i
                color = cm.nipy_spectral(float(i) / n_clusters)
                    
                ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)
    
    # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
    # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples
    
            ax1.set_title("The silhouette plot for the various clusters.")
            ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
            ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
                    
            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    # 2nd Plot showing the actual clusters formed
            colors = cm.jet(cluster_labels.astype(float) / n_clusters)
            
            ax2.scatter(df[var1], df[var2], marker='o', s=30, lw=0, alpha=0.7,
                            c=colors, edgecolor='k')
    # Labeling the clusters
            centers = clusterer.cluster_centers_
    # Draw black stars at cluster centers
            centers1 = pd.DataFrame(centers,columns=df.columns)
            
            ax2.scatter(centers1.loc[:, var1], centers1.loc[:, var2], marker='*',
                            c="black", alpha=1, s=200, edgecolor='k')
    
            ax2.set_title("The visualization of the clustered data.")
            ax2.set_xlabel("Feature space for the %s"% var1)
            ax2.set_ylabel("Feature space for the %s"% var2)
    
            plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                              "with n_clusters = %d" % n_clusters),
                             fontsize=14, fontweight='bold')
        return plt
    
    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)
            else:
                plot(figure, config=config)

