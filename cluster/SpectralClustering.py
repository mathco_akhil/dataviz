import pandas as pd
from sklearn.cluster import SpectralClustering
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
      

@pd.api.extensions.register_dataframe_accessor("SpectralClustering")
class spectralClustering(object):
    """
    Contains functions to build, predict and visualise spectral clustering 
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns visualised plot with cluster labels
    4. eigenDecomposition() - This method performs the eigen decomposition on a given affinity matrix
    
    """    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def buildModel(self,n_clusters=8, eigen_solver=None, random_state=None, n_init=10, gamma=1.0, affinity='rbf', n_neighbors=10, eigen_tol=0.0, assign_labels='kmeans', degree=3, coef0=1, kernel_params=None, n_jobs=None):
        """
        Builds a model for agglomertaive using given hyperparameters
        
        Parameters
        ----------
        n_clusters : Number of clusters after the final clustering step, which treats the subclusters from the leaves as new samples.
        
        eigen_solver : The eigenvalue decomposition strategy to use. AMG requires pyamg to be installed. It can be faster on very large, sparse problems, but may also lead to instabilities
        
        random_state : A pseudo random number generator used for the initialization of the lobpcg eigen vectors decomposition when eigen_solver == ‘amg’ and by the K-Means initialization. Use an int to make the randomness deterministic
        
        n_init : Number of time the k-means algorithm will be run with different centroid seeds. The final results will be the best output of n_init consecutive runs in terms of inertia.
        
        gamma : Kernel coefficient for rbf, poly, sigmoid, laplacian and chi2 kernels. Ignored for affinity='nearest_neighbors'.
        
        affinity : If a string, this may be one of ‘nearest_neighbors’, ‘precomputed’, ‘rbf’ or one of the kernels supported by sklearn.metrics.pairwise_kernels.Only kernels that produce similarity scores (non-negative values that increase with similarity) should be used. This property is not checked by the clustering algorithm.
        
        n_neighbors : Number of neighbors to use when constructing the affinity matrix using the nearest neighbors method. Ignored for affinity='rbf'.
        
        eigen_tol : Stopping criterion for eigendecomposition of the Laplacian matrix when using arpack eigen_solver.
        
        assign_labels : The strategy to use to assign labels in the embedding space. There are two ways to assign labels after the laplacian embedding. k-means can be applied and is a popular choice. But it can also be sensitive to initialization. Discretization is another approach which is less sensitive to random initialization.
        
        degree : Degree of the polynomial kernel. Ignored by other kernels.
        
        coef0 : Zero coefficient for polynomial and sigmoid kernels. Ignored by other kernels.
        
        kernel_params : Parameters (keyword arguments) and values for kernel passed as callable object. Ignored by other kernels.
        
        n_jobs : The number of parallel jobs to run. None means 1 unless in a joblib.parallel_backend context. -1 means using all processors. 
            
        Usage
        --------
        df.SpectralClustering.buildModel()

        Returns
        -------
        fitted model

        """
        clustering = SpectralClustering(n_clusters=n_clusters, eigen_solver=eigen_solver, random_state=random_state, n_init=n_init, gamma=gamma, affinity=affinity, n_neighbors=n_neighbors, eigen_tol=eigen_tol, assign_labels=assign_labels, degree=degree, coef0=coef0, kernel_params=kernel_params, n_jobs=n_jobs)
        clustering.fit(self._obj)   
        return(clustering)
    
    def predictCluster(self,model):
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        model : fitted model object
        
        Usage
        --------
        df.SpectralClustering.predictCluster()

        Returns
        -------
        pandas dataframe
        """
        df = self._obj.copy()
        labels = model.labels_
        df['clusters'] = labels
        return df
    
    def visualizeCluster(self,var1,var2,model):
        """
        Generates visualised plot for spectral clustering
        
        Parameters
        ----------
        
        var1: X axis variable
        
        var2: Y axis variable
        
        model: fitted model object
        
        Usage
        --------
        df.SpectralClustering.visualizeCluster()

        Returns
        -------
        Matplotlib object
        """
        df = self.predictCluster(model)
    # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(14, 7)
    # returns a jet colormap
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.legend()            
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt
        
