import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import SpectralClustering


class TestSpectralClusterMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.DataFrame([[1,2,3,4], [4,4,4,4], [5,6,7,8],[13,25,61,1],[34,23,12,10]])
        self.df_mixed = pd.DataFrame([[1,2,3,4], [3,4,5,6], ['x',6,7,8]])
        self.df1 = pd.read_csv("custScaledData.csv")
        self.df2 = pd.read_csv("Dataset.csv")
        self.df3 = pd.read_csv("MergedDataset.csv")

        
    def test_buildModel(self):
        log2 = pickle.load(open("buildModelSpectral.pkl", 'rb'))
        out = self.df2.SpectralClustering.buildModel(n_clusters=3,random_state=1313)
        numpy.testing.assert_string_equal(str(out),str(log2))   

    def test_predictCluster(self):
        log = pickle.load(open("buildModelSpectral.pkl", 'rb'))
        clusterdf=self.df3.SpectralClustering.predictCluster(model=log)
        clusterdf = clusterdf.astype(int)
        pd.util.testing.assert_frame_equal(clusterdf,pd.read_csv(r"MergedDataset.csv").astype(int))
        