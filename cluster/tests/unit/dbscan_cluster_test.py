import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import DBSCAN


class TestDBSCANClusterMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.DataFrame([[1,2,3,4], [4,4,4,4], [5,6,7,8],[13,25,61,1],[34,23,12,10]])
        
    def test_buildModel(self):
        model = self.df_nos.dbscan.buildModel(eps =0.5, min_samples =10,metric='euclidean',n_jobs=1)
        pickleFile = open(r"dbscan.txt", 'rb')
        dbscan = pickle.load(pickleFile)
        pd.util.testing.assert_attr_equal("metric",model,dbscan)
    
    def test_predictCluster(self):
        model = self.df_nos.dbscan.buildModel(eps =0.5, min_samples =10,metric='euclidean',n_jobs=1)
        clusterdf = self.df_nos.dbscan.predictCluster(model)
        clusterdf.columns = ['a','b','c','d','clusters']
        clusterdf['clusters'] = clusterdf['clusters'].astype('int64')
        pd.util.testing.assert_frame_equal(clusterdf,pd.read_csv(r"clusterdf_dbscan.csv", dtype = 'int64'))
        
        