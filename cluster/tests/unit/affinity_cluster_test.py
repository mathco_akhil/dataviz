import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

import AffinityPropagation

class TestAffinityPropagationMethods(unittest.TestCase):

   def setUp(self):
       self.df_ap = pd.read_csv('cluster.csv')

   def test_buildModelAP(self):
        ap = pickle.load(open("buildModelAP.pkl", 'rb'))
        out = self.df_ap.affinityPropagation.buildModel(damping_ap = 0.5)
        numpy.testing.assert_string_equal(str(out),str(ap))    

   def test_predictModelAP(self):
       ap_model = pickle.load(open('buildModelAP.pkl','rb'))
       cluster_ap_out = self.df_ap.affinityPropagation.predictCluster(ap_model)
       cluster_ap_out = cluster_ap_out.astype(int)
       pd.util.testing.assert_frame_equal(cluster_ap_out,pd.read_csv(r'cluster_ap_op.csv').astype(int))