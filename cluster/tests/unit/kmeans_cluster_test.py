import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import KMeansCluster


class TestKMeansClusterMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.DataFrame([[1,2,3,4], [4,4,4,4], [5,6,7,8],[13,25,61,1],[34,23,12,10]])

    def test_buildModel(self):
        model = self.df_nos.kmeans.buildModel(n_clusters=3, random_state=3)
        pickleFile = open(r"kmeans.txt", 'rb')
        kmeans = pickle.load(pickleFile)
        pd.util.testing.assert_attr_equal("n_clusters",model,kmeans)
        
    def test_predictCluster(self):
        model = self.df_nos.kmeans.buildModel(n_clusters=3, random_state=3)
        clusterdf = self.df_nos.kmeans.predictCluster(model)
        clusterdf.columns = ['0','1','2','3','clusters']
        clusterdf['clusters'] = clusterdf['clusters'].astype('int64')
        pd.util.testing.assert_frame_equal(clusterdf,pd.read_csv(r"clusterdf_kmeans.csv", dtype = 'int64'))