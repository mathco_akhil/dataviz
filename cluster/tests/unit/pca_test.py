import unittest
import pandas as pd
import sys
import os
import pickle

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import PCA

class TestPCAMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.DataFrame([[1,2,3,4], [4,4,4,4], [5,6,7,8],[13,25,61,1],[34,23,12,10]])

    def test_buildModel(self):
        model = self.df_nos.pca.buildModel(n_components=2,random_state=113)
        pickleFile = open(r"pca.txt", 'rb')
        pca = pickle.load(pickleFile)
        pd.util.testing.assert_attr_equal("n_components",model,pca)
        
    def test_getReducedData(self):
        model = self.df_nos.pca.buildModel(n_components=2,random_state=113)
        data = model = self.df_nos.pca.getReducedData(model)
        pd.util.testing.assert_frame_equal(data,pd.read_csv(r"pcaReducedDf.csv"))