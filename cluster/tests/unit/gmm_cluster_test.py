import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import GMMCluster

class TestGMMClusterMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.read_csv('cust1.csv')

    def test_buildModel(self):
        model = self.df_nos.GaussianMixture.buildModel(n_components=3,random_state=101)
        pickleFile = open(r"gmm.pkl", 'rb')
        gmm = pickle.load(pickleFile)
        pd.util.testing.assert_attr_equal("n_components",model,gmm)
        
        
    def test_predictCluster(self):
        model = pickle.load(open("gmm.pkl", 'rb'))
        clusterdf=self.df_nos.GaussianMixture.predictCluster(model)
        clusterdf = clusterdf.astype(int)
        pd.util.testing.assert_frame_equal(clusterdf,pd.read_csv(r"gmmMergedDataset.csv").astype(int))