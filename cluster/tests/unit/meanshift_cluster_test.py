import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

import MeanShift

class TestMeanShiftMethods(unittest.TestCase):

   def setUp(self):
       self.df_ms = pd.read_csv('cluster.csv')

   def test_buildModelMS(self):
        ms = pickle.load(open("buildModelMS.pkl", 'rb'))
        out = self.df_ms.meanShift.buildModel(bandwidth_ms=1)
        numpy.testing.assert_string_equal(str(out),str(ms))    

   def test_predictModelMS(self):
       #ms_model = pickle.load(open('buildModelMS.pkl','rb'))
       ms_model = self.df_ms.meanShift.buildModel(bandwidth_ms=1)
       cluster_ms_out = self.df_ms.meanShift.predictCluster(ms_model)
       cluster_ms_out = cluster_ms_out.astype(int)
       pd.util.testing.assert_frame_equal(cluster_ms_out,pd.read_csv(r'cluster_ms_op.csv').astype(int))