import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import KMedoids

class TestKmedoidsMethods(unittest.TestCase):
    def setUp(self):
        self.df_nos = pd.DataFrame([[2, 60, 21], [3, 40, 31], [3, 80, 2], [4, 70, 53], [6, 20, 32], [6, 40, 11], [7, 30, 76], [7, 40, 29], [15, 100, 8], [7, 60, 19],[5,3,2],[20,100,40]], columns=['col1','col2','col3'])
        self.df_mixed = pd.DataFrame([[1,2,3,4], [3,4,5,6], ['x',6,7,8]])
        self.model = self.df_nos.KMedoids.buildModel(2, random_state=4)
        self.clusterdf = self.df_nos.KMedoids.mergeCluster(self.model)

    def test_build(self):
        pickleFile = open(r"kmedoids.txt", 'rb')
        kmeds = pickle.load(pickleFile)
        pd.util.testing.equalContents(str(self.model),str(kmeds))
        
    def test_buildClusterInfo(self):
        clusterInfo = self.clusterdf.KMedoids.buildClusterInfo(self.model.cluster_centers_)
        clusterInfo.astype(str)
        clusterInfo.columns = ['0', 'Cluster Center', 'Population']
        pd.util.testing.assert_frame_equal(clusterInfo[['0','Population']],pd.read_csv(r"kmedsClusterInfo.csv", index_col = False)[['0','Population']], check_dtype=False, check_column_type=False)