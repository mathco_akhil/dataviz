import unittest
from meanshift_cluster_test import TestMeanShiftMethods
from affinity_cluster_test import TestAffinityPropagationMethods
from birch_cluster_test import TestBirchClusterMethods
from spectral_cluster_test import TestSpectralClusterMethods
from kmeans_cluster_test import TestKMeansClusterMethods
from dbscan_cluster_test import TestDBSCANClusterMethods
from gmm_cluster_test import TestGMMClusterMethods
from hierarchical_cluster_test import TestHierarchicalClusterMethods
from pca_test import TestPCAMethods
from kmedoids_test import TestKmedoidsMethods

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestMeanShiftMethods))
    suite.addTest(unittest.makeSuite(TestAffinityPropagationMethods))
    suite.addTest(unittest.makeSuite(TestBirchClusterMethods))
    suite.addTest(unittest.makeSuite(TestSpectralClusterMethods))
    suite.addTest(unittest.makeSuite(TestKMeansClusterMethods))
    suite.addTest(unittest.makeSuite(TestDBSCANClusterMethods))
    suite.addTest(unittest.makeSuite(TestPCAMethods))
    suite.addTest(unittest.makeSuite(TestGMMClusterMethods))
    suite.addTest(unittest.makeSuite(TestHierarchicalClusterMethods))
    suite.addTest(unittest.makeSuite(TestKmedoidsMethods))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())