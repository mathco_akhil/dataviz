import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import HierarchicalCluster

class TestHierarchicalClusterMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.read_csv('cust1.csv')

    def test_buildModel(self):
        model = self.df_nos.hierarchical.buildModel(n_clusters=3)
        pickleFile = open(r"hierarchical.pkl", 'rb')
        hierarchical = pickle.load(pickleFile)
        pd.util.testing.assert_attr_equal("n_clusters",model,hierarchical)
        
        
    def test_predictCluster(self):
        model = pickle.load(open("hierarchical.pkl", 'rb'))
        clusterdf=self.df_nos.hierarchical.predictCluster(model)
        clusterdf = clusterdf.astype(int)
        pd.util.testing.assert_frame_equal(clusterdf,pd.read_csv(r"hierarchicalMergedDataset.csv").astype(int))