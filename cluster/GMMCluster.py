import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import seaborn as sns; sns.set()
from sklearn.mixture import GaussianMixture
from sklearn.metrics import silhouette_samples, silhouette_score

@pd.api.extensions.register_dataframe_accessor("GaussianMixture")
class GMMCluster(object):
    """
    Contains functions to build, predict and visualise birch 
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns plot with cluster labels
    4. visualizeSilhouette() - This function is used to display silhouette plot
    
    """
    def __init__(self, pandas_obj):
        self._obj = pandas_obj


    def buildModel(self,n_components=1, covariance_type='full', tol=0.001, reg_covar=1e-06, max_iter=100, n_init=1, init_params='kmeans', weights_init=None, means_init=None, precisions_init=None, random_state=None, warm_start=False, verbose=0, verbose_interval=10):
        """
        This function builds the gaussian mixture model
        
        Parameters
        ----------	
        n_components : int, defaults to 1.
        The number of mixture components.
        
        covariance_type : {‘full’ (default), ‘tied’, ‘diag’, ‘spherical’}
        String describing the type of covariance parameters to use. Must be one of:
        
        ‘full’
        each component has its own general covariance matrix
        
        ‘tied’
        all components share the same general covariance matrix
        
        ‘diag’
        each component has its own diagonal covariance matrix
        
        ‘spherical’
        each component has its own single variance
        
        tol : float, defaults to 1e-3.
        The convergence threshold. EM iterations will stop when the lower bound average gain is below this threshold.
        
        reg_covar : float, defaults to 1e-6.
        Non-negative regularization added to the diagonal of covariance. Allows to assure that the covariance matrices are all positive.
        
        max_iter : int, defaults to 100.
        The number of EM iterations to perform.
        
        n_init : int, defaults to 1.
        The number of initializations to perform. The best results are kept.
        
        init_params : {‘kmeans’, ‘random’}, defaults to ‘kmeans’.
        The method used to initialize the weights, the means and the precisions. Must be one of:
        
        'kmeans' : responsibilities are initialized using kmeans.
        'random' : responsibilities are initialized randomly.
        weights_init : array-like, shape (n_components, ), optional
        The user-provided initial weights, defaults to None. If it None, weights are initialized using the init_params method.
        
        means_init : array-like, shape (n_components, n_features), optional
        The user-provided initial means, defaults to None, If it None, means are initialized using the init_params method.
        
        precisions_init : array-like, optional.
        The user-provided initial precisions (inverse of the covariance matrices), defaults to None. If it None, precisions are initialized using the ‘init_params’ method. The shape depends on ‘covariance_type’:
        
        (n_components,)                        if 'spherical',
        (n_features, n_features)               if 'tied',
        (n_components, n_features)             if 'diag',
        (n_components, n_features, n_features) if 'full'
        random_state : int, RandomState instance or None, optional (default=None)
        If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.
        
        warm_start : bool, default to False.
        If ‘warm_start’ is True, the solution of the last fitting is used as initialization for the next call of fit(). This can speed up convergence when fit is called several times on similar problems. In that case, ‘n_init’ is ignored and only a single initialization occurs upon the first call. See the Glossary.
        
        verbose : int, default to 0.
        Enable verbose output. If 1 then it prints the current initialization and each iteration step. If greater than 1 then it prints also the log probability and the time needed for each step.
        
        verbose_interval : int, default to 10.
        Number of iteration done before the next print.
        
        Usage
        --------
        df.GaussianMixture.buildModel()
        
        Return
        --------
        Fitted model object
        """
        gmm = GaussianMixture(n_components=n_components, covariance_type=covariance_type, tol=tol, reg_covar=reg_covar, max_iter=max_iter, n_init=n_init, init_params=init_params, weights_init=weights_init, means_init=means_init, precisions_init=precisions_init, random_state=random_state, warm_start=warm_start, verbose=verbose, verbose_interval=verbose_interval)
        gmm.fit(self._obj)
        return gmm
    
    def predictCluster(self,model):
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        model : fitted model object
        
        Usage
        --------
        df.GaussianMixture.predictCluster()

        Returns
        -------
        pandas dataframe
        """
        labels = model.fit(self._obj).predict(self._obj)
        df = self._obj.copy()
        df['clusters'] = labels
        return df
    
    
    def visualizeCluster(self,var1,var2,model):
        """
        This function used in vizualizeClusterIpy() to plot KMeans cluster visualization
        
        Parameters
        ----------
        var1: X axis variable
        
        var2: Y axis variable
        
        model : fitted model object
        
        Usage
        --------
        df.GaussianMixture.visualizeCluster()

        Returns
        -------
        Matplotlib object
        """
        df = self.predictCluster(model)
    # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7)
    # returns a jet colormap
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):
            
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.legend()
            
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt
    
    def visualizeSilhouette(self,n_clusters,var1,var2):
        """
        This function is used to display silhouette plot a measure of how close each point in one cluster is to points in the neighboring clusters and thus provides a way to assess parameters like number of clusters visually. 
        
        Parameters
        ----------
        n_clusters: max number of number of clusters
        
        var1: X axis variable
        
        var2: Y axis variable
        
        Usage
        --------
        df.GaussianMixture.visualizeSilhouette()

        Returns
        -------
        Matplotlib Object
        """
        df = self._obj.copy()
        for n_clusters in range(2,n_clusters+1):
    # Create a subplot with 1 row and 2 columns
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(18, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
            ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
            ax1.set_ylim([0, len(df) + (n_clusters + 1) * 10])
    
    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
            clusterer = GaussianMixture(n_components=n_clusters)
            cluster_labels = clusterer.fit(df).predict(df)   
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
            silhouette_avg = silhouette_score(df, cluster_labels)
            print("For n_clusters =", n_clusters,
                   "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(df, cluster_labels)
            y_lower = 10
            for i in range(n_clusters):
    # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
                ith_cluster_silhouette_values = \
                    sample_silhouette_values[cluster_labels == i]
    
                ith_cluster_silhouette_values.sort()
    
                size_cluster_i = ith_cluster_silhouette_values.shape[0]
                y_upper = y_lower + size_cluster_i
                
                try:
                    color = cm.nipy_spectral(float(i) / n_clusters)
                except Exception as python_error_message:
                    print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                    print(python_error_message) 
                    
                ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)
    
    # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
    # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples
    
            ax1.set_title("The silhouette plot for the various clusters.")
            ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
            ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
                    
            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    # 2nd Plot showing the actual clusters formed
            try:
                colors = cm.jet(cluster_labels.astype(float) / n_clusters)
            except Exception as python_error_message:
                print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                print(python_error_message)
            
            ax2.scatter(df[var1], df[var2], marker='o', s=30, lw=0, alpha=0.7,
                            c=colors, edgecolor='k')
    
            ax2.set_title("The visualization of the clustered data.")
            ax2.set_xlabel("Feature space for the %s"% var1)
            ax2.set_ylabel("Feature space for the %s"% var2)
    
            plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                              "with n_clusters = %d" % n_clusters),
                             fontsize=14, fontweight='bold')
        return plt