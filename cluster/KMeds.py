'''
Light Weight K-Medoids Implementation
'''

import numpy as np
import matplotlib
from copy import deepcopy
import math
import numpy
from scipy.spatial import distance


def linalg_norm(a, b):
    return numpy.linalg.norm(a-b)


def sqrt_sum(a, b):
    return numpy.sqrt(numpy.sum((a-b)**2, axis=1))


def scipy_distance(a, b):
    return list(map(distance.euclidean, a, b))


def mpl_dist(a, b):
    return list(map(matplotlib.mlab.dist, a, b))


def sqrt_einsum(a, b):
    a_min_b = a - b
    return numpy.sqrt(numpy.einsum('ij,ij->i', a_min_b, a_min_b))


def _get_init_centers(n_clusters, n_samples, random_state = None):
    '''return random points as initial centers'''
    init_ids = []
    np.random.seed(random_state)
    while len(init_ids) < n_clusters:
        _ = np.random.randint(0,n_samples)
        if not _ in init_ids:
            init_ids.append(_)
    return init_ids

def _get_distance(data1, data2):
    '''example distance function'''
    return np.sqrt(np.sum((data1 - data2)**2))

def _get_cost(X, centers_id, dist_function):
    '''return total cost and cost of each cluster'''
    dist_mat = np.zeros((len(X),len(centers_id)))
    
    if dist_function == "linalg_norm":
        dist_func = linalg_norm
    elif dist_function == "sqrt_sum":
        dist_func = sqrt_sum
    elif dist_function == "scipy_distance":
        dist_func = scipy_distance
    elif dist_function == "mpl_dist":
        dist_func = mpl_dist
    else:
        dist_func = sqrt_einsum
    # compute distance matrix
    for j in range(len(centers_id)):
        center = X[centers_id[j],:]
        for i in range(len(X)):
            if i == centers_id[j]:
                dist_mat[i,j] = 0.
            else:
                dist_mat[i,j] = dist_func(X[i,:], center)
    #print 'cost ', -st+time.time()
    mask = np.argmin(dist_mat,axis=1)
    members = np.zeros(len(X))
    costs = np.zeros(len(centers_id))
    for i in range(len(centers_id)):
        mem_id = np.where(mask==i)
        members[mem_id] = i
        costs[i] = np.sum(dist_mat[mem_id,i])
    return members, costs, np.sum(costs), dist_mat

def _kmedoids_run(X, n_clusters, dist_function, random_state=None, max_iter=1000, tol=0.001, verbose=True):
    '''run algorithm return centers, members, and etc.'''
    # Get initial centers
    n_samples, n_features = X.shape
    init_ids = _get_init_centers(n_clusters,n_samples, random_state=random_state)
    if verbose:
        print('Initial centers are ', init_ids)
    centers = init_ids
    members, costs, tot_cost, dist_mat = _get_cost(X, init_ids,dist_function)
    cc,SWAPED = 0, True
    cent = []
    while True:
        SWAPED = False
        for i in range(n_samples):
            if not i in centers:
                for j in range(len(centers)):
                    centers_ = deepcopy(centers)
                    centers_[j] = i
                    members_, costs_, tot_cost_, dist_mat_ = _get_cost(X, centers_,dist_function)
                    if tot_cost_-tot_cost < tol:
                        members, costs, tot_cost, dist_mat = members_, costs_, tot_cost_, dist_mat_
                        centers = centers_
                        SWAPED = True
                        if centers in cent:
                            SWAPED=False
                            break
                        else:
                            cent.append(centers)
                        if verbose:
                            print('Change centers to ', centers)
        if cc > max_iter:
            if verbose:
                print('End Searching by reaching maximum iteration', max_iter)
            break
        if not SWAPED:
            if verbose:
                print('End Searching by no swaps')
            break
        cc += 1
    return centers,members, costs, tot_cost, dist_mat

class KMedoids(object):
    '''
    Main API of KMedoids Clustering
    Parameters
    --------
        n_clusters: number of clusters
        dist_func : distance function ("linalg_norm","sqrt_sum","scipy_distance","mpl_dist")
        max_iter: maximum number of iterations
        tol: tolerance
    Attributes
    --------
        labels_    :  cluster labels for each data item
        centers_   :  cluster centers id
        costs_     :  array of costs for each cluster
        n_iter_    :  number of iterations for the best trail
    Methods
    -------
        fit(X): fit the model
            - X: 2-D numpy array, size = (n_sample, n_features)
        predict(X): predict cluster id given a test dataset.
    '''
    def __init__(self, n_clusters, dist_func="linalg_norm", max_iter=10000, tol=0.0001, random_state=None):
        self.n_clusters = n_clusters
        self.dist_func = dist_func
        self.max_iter = max_iter
        self.tol = tol
        self.random_state = random_state

    def fit(self, X, verbose=True):
        centers,members, costs,tot_cost, dist_mat = _kmedoids_run(
                X,self.n_clusters, self.dist_func, max_iter=self.max_iter, tol=self.tol,verbose=verbose, random_state = self.random_state)
        center_list=[]
        for i in centers:
            center_list.append(X[i].tolist())  
        cluster_centers_ = np.array(center_list)
        self.cluster_centers_ = cluster_centers_
        self.labels_ = members
        self.costs = costs
        self.tot_cost = tot_cost
        self.dist_mat = dist_mat
        return

    def predict(self,X, dist_function = "linalg_norm"):
        
        if dist_function == "linalg_norm":
            dist_func = linalg_norm
        elif dist_function == "sqrt_sum":
            dist_func = sqrt_sum
        elif dist_function == "scipy_distance":
            dist_func = scipy_distance
        elif dist_function == "mpl_dist":
            dist_func = mpl_dist
        else:
            dist_func = sqrt_einsum
        
        label_list = []
        for i in X.values:
            min_dist = math.inf
            k = 0
            label = -1
            for j in self.cluster_centers_:
                current_dist = dist_func(i, j)
                if current_dist<min_dist:
                    min_dist=current_dist
                    label = k
                k = k+1
            label_list.append(label)
        return label_list