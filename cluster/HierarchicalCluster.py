import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import plotly.figure_factory as ff
from plotly.offline import plot, iplot
import seaborn as sns; sns.set()

from sklearn.cluster import AgglomerativeClustering
from sklearn.metrics import silhouette_samples, silhouette_score
from scipy.cluster.hierarchy import linkage

@pd.api.extensions.register_dataframe_accessor("hierarchical")
class HierarchicalCluster(object):
    """
    Contains functions to build, predict and visualise Hierarchial cluster
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns visualised plot with cluster labels
    4. visualizeSilhouette() - This function is used to display silhouette plot
    5. visualizeDendro() - This function displays Dendrogram plot.
    
    """    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj


    def buildModel(self, n_clusters=2, affinity='euclidean', memory=None, connectivity=None, compute_full_tree='auto', linkage='ward', pooling_func='deprecated'):
        """
        Parameters:	
        n_clusters : int, default=2
        The number of clusters to find.
        
        affinity : string or callable, default: “euclidean”
        Metric used to compute the linkage. Can be “euclidean”, “l1”, “l2”, “manhattan”, “cosine”, or ‘precomputed’. If linkage is “ward”, only “euclidean” is accepted.
        
        memory : None, str or object with the joblib.Memory interface, optional
        Used to cache the output of the computation of the tree. By default, no caching is done. If a string is given, it is the path to the caching directory.
        
        connectivity : array-like or callable, optional
        Connectivity matrix. Defines for each sample the neighboring samples following a given structure of the data. This can be a connectivity matrix itself or a callable that transforms the data into a connectivity matrix, such as derived from kneighbors_graph. Default is None, i.e, the hierarchical clustering algorithm is unstructured.
        
        compute_full_tree : bool or ‘auto’ (optional)
        Stop early the construction of the tree at n_clusters. This is useful to decrease computation time if the number of clusters is not small compared to the number of samples. This option is useful only when specifying a connectivity matrix. Note also that when varying the number of clusters and using caching, it may be advantageous to compute the full tree.
        
        linkage : {“ward”, “complete”, “average”, “single”}, optional (default=”ward”)
        Which linkage criterion to use. The linkage criterion determines which distance to use between sets of observation. The algorithm will merge the pairs of cluster that minimize this criterion.
        
        ward minimizes the variance of the clusters being merged.
        average uses the average of the distances of each observation of the two sets.
        complete or maximum linkage uses the maximum distances between all observations of the two sets.
        single uses the minimum of the distances between all observations of the two sets.
        
        Return
        ------
        Model object
        
        """
        hierarchical = AgglomerativeClustering(n_clusters=n_clusters,affinity=affinity, memory=memory, connectivity=connectivity, compute_full_tree=compute_full_tree, linkage=linkage, pooling_func=pooling_func)
        hierarchical.fit(self._obj)
        return hierarchical
    
    def predictCluster(self,model):
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        model : model object
        
        Usage
        --------
        df.BirchClustering.buildModelLogistic()

        Returns
        -------
        The model fit 
        
        """
        labels = model.fit_predict(self._obj)
        df = self._obj.copy()
        df['clusters'] = labels
        return df

    
    def visualizeCluster(self,var1,var2,model):
        """
        This function used in vizualizeClusterIpy() to plot KMeans cluster visualization
        
        df: dataframe with the KMeans clustering to be used for visualization
        
        var1: X axis variable
        
        var2: Y axis variable
        
        centers: centers of the KMeans clustering    
        """
        df = self.predictCluster(model)
    # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7)
    # returns a jet colormap
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):
            
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.legend()
            
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt
    
    def visualizeDendro(self):
        """
        This user  defined function displays Dendrogram plot.
        
        df: dataframe on which KMeans clustering needs to be run
        
        min_clust: minimum number of clusters. Default value is 2
        
        max_clust: maximum number of clusters. Default value is 11
        """
        dendro = ff.create_dendrogram(self._obj,orientation='bottom',linkagefun=lambda x: linkage(self._obj, 'ward', metric='euclidean'))
        dendro['layout'].update({'width':1000,'height':750,'title':'Dendrogram'})
        
        # Edit xaxis
        dendro['layout']['xaxis'].update({#'domain': [.15, 1],
                                  'title':'Observations',
                                  'mirror': True,
                                  'showgrid': False,
                                  'showline': True,
                                  'zeroline': True,
                                  'ticks':""})
        
        dendro['layout']['yaxis'].update({#'domain': [0, .85],
                                  'title':'Similarity',
                                  'mirror': True,
                                  'showgrid': False,
                                  'showline': True,
                                  'zeroline': True,
                                  'showticklabels': True,
                                  'ticks': ""})
    
        return dendro
    
    def visualizeSilhouette(self,n_clusters,var1,var2):
        """
        This function is used to display silhouette plot a measure of how close each point in one cluster is to points in the neighboring clusters and thus provides a way to assess parameters like number of clusters visually. 
        
        df: dataframe on which KMeans clustering needs to be run
        
        n_clusters: max number of number of clusters
        
        var1: X axis variable
        
        var2: Y axis variable
        """
        df = self._obj.copy()
        for n_clusters in range(2,n_clusters+1):
    # Create a subplot with 1 row and 2 columns
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(18, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
            ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
            ax1.set_ylim([0, len(df) + (n_clusters + 1) * 10])
    
    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
            clusterer = AgglomerativeClustering(n_clusters=n_clusters)
            cluster_labels = clusterer.fit_predict(df)   
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
            silhouette_avg = silhouette_score(df, cluster_labels)
            print("For n_clusters =", n_clusters,
                   "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(df, cluster_labels)
            y_lower = 10
            for i in range(n_clusters):
    # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
                ith_cluster_silhouette_values = \
                    sample_silhouette_values[cluster_labels == i]
    
                ith_cluster_silhouette_values.sort()
    
                size_cluster_i = ith_cluster_silhouette_values.shape[0]
                y_upper = y_lower + size_cluster_i
                
                try:
                    color = cm.nipy_spectral(float(i) / n_clusters)
                except Exception as python_error_message:
                    print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                    print(python_error_message) 
                    
                ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)
    
    # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
    # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples
    
            ax1.set_title("The silhouette plot for the various clusters.")
            ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
            ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
                    
            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    # 2nd Plot showing the actual clusters formed
            try:
                colors = cm.jet(cluster_labels.astype(float) / n_clusters)
            except Exception as python_error_message:
                print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                print(python_error_message)
            
            ax2.scatter(df[var1], df[var2], marker='o', s=30, lw=0, alpha=0.7,
                            c=colors, edgecolor='k')
    
            ax2.set_title("The visualization of the clustered data.")
            ax2.set_xlabel("Feature space for the %s"% var1)
            ax2.set_ylabel("Feature space for the %s"% var2)
    
            plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                              "with n_clusters = %d" % n_clusters),
                             fontsize=14, fontweight='bold')
        return plt
    
    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)
            else:
                plot(figure, config=config)
    