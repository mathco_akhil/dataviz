import pandas as pd
import numpy as np
from sklearn.decomposition import PCA
import seaborn as sns; sns.set()
from matplotlib.ticker import MaxNLocator
from plotly.graph_objs import *
import matplotlib.pyplot as plt
import plotly.graph_objs as go
from plotly.offline import plot,iplot
#import matplotlib.cm as cm

@pd.api.extensions.register_dataframe_accessor("pca")
class PrincipalComponentAnalysis(object):
    """
    This class helps in performing dimensionalty reduction using Principal component analysis
    1. buildModel() - Builds PCA model with given hyperparameters and fits data
    2. getReducedData() - this function creates dimensionality reduced data from original dataframe
    3. visualizeBiplot() - Produces a biplot that shows a scatterplot of the reduced data and the projections of the original features.
    4. pcaResults() - Create a DataFrame of the PCA results,Includes dimension feature weights and explained variance and Visualizes the PCA results
    5. visualizeParetoPlot() - Plot a Combo-Plot for Variance per dimension and Cumulative Variance
    6. visualizeScreePlot() - Creates a Scree plot of the PCA to determine optimum number of pricipal components
    7. visualizeScoreplot() - This function Creates a Score plot of the PCA to to assess the data structure and detect clusters
    """

    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
    def buildModel(self,n_components=None, copy=True, whiten=False, svd_solver='auto', tol=0.0, iterated_power='auto', random_state=None):
        """
        Apply PCA by fitting data with number of components
        Transform the data using the PCA fit above
        Generate principal components
        
        Parameters
        ----------
        n_components: Number of components to keep. if n_components is not set all components are kept
        
        copy : bool (default True)
        If False, data passed to fit are overwritten and running fit(X).transform(X) will not yield the expected results, use fit_transform(X) instead.
    
        whiten : bool, optional (default False)
        When True (False by default) the components_ vectors are multiplied by the square root of n_samples and then divided by the singular values to ensure uncorrelated outputs with unit component-wise variances.
    
        Whitening will remove some information from the transformed signal (the relative variance scales of the components) but can sometime improve the predictive accuracy of the downstream estimators by making their data respect some hard-wired assumptions.
    
        svd_solver : string {‘auto’, ‘full’, ‘arpack’, ‘randomized’}
        auto :
        the solver is selected by a default policy based on X.shape and n_components: if the input data is larger than 500x500 and the number of components to extract is lower than 80% of the smallest dimension of the data, then the more efficient ‘randomized’ method is enabled. Otherwise the exact full SVD is computed and optionally truncated afterwards.
    
        full :
        run exact full SVD calling the standard LAPACK solver via scipy.linalg.svd and select the components by postprocessing
    
        arpack :
        run SVD truncated to n_components calling ARPACK solver via scipy.sparse.linalg.svds. It requires strictly 0 < n_components < min(X.shape)
    
        randomized :
        run randomized SVD by the method of Halko et al.
    
        New in version 0.18.0.
    
        tol : float >= 0, optional (default .0)
        Tolerance for singular values computed by svd_solver == ‘arpack’.
    
        New in version 0.18.0.
    
        iterated_power : int >= 0, or ‘auto’, (default ‘auto’)
        Number of iterations for the power method computed by svd_solver == ‘randomized’.
    
        New in version 0.18.0.
    
        random_state : int, RandomState instance or None, optional (default None)
        If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.
        
        Usage
        --------
        df.pca.buildModel(n_components = 4)
        
        Returns
        -------
        fitted model object
        """
    # implementing PCA by fitting the data with n_components
        pca = PCA(n_components=n_components,copy=copy,whiten=whiten,svd_solver=svd_solver,tol=tol,iterated_power=iterated_power,random_state=random_state)
        model = pca.fit(self._obj)
        return model
    
    def getReducedData(self,model):
        """
        This function creates dimensionality reduced data from original dataframe
        
        Parameters
        ----------
        model : fitted model object
        
        Usage
        --------
        df.pca.getReducedData()

        Returns
        -------
        pandas dataframe
        """
    # Transform the good data using the PCA fit above
        data_transformed = model.transform(self._obj)
    # Creates a DataFrame for the reduced data
        data_transformed = pd.DataFrame(data_transformed)
        data_reduced = pd.DataFrame()
        for i,k in enumerate(data_transformed):
            data_reduced['Dimension %d'%(i+1)] = data_transformed.iloc[:,i]
        return data_reduced
    
    def visualizeBiplot(self, data_reduced, model):
        '''
        Produce a biplot that shows a scatterplot of the reduced
        data and the projections of the original features.
        
        Parameters
        ----------
        data_reduced: the reduced data (the first two dimensions are plotted)
        
        model: fitted model object
        
        Usage
        --------
        df.pca.visualizeBiplot()

        Returns
        -------
        matplotlib object
        '''
        fig, ax = plt.subplots(figsize = (25,10))
        # scatterplot of the reduced data    
        ax.scatter(x=data_reduced.loc[:, 'Dimension 1'], y=data_reduced.loc[:, 'Dimension 2'], 
                facecolors='b', edgecolors='b', s=70, alpha=0.5)
        feature_vectors = model.components_.T
        # we use scaling factors to make the arrows easier to see
        arrow_size, text_pos = 1.0, 1.1,
    
        # projections of the original features
        for i, v in enumerate(feature_vectors):
            ax.arrow(0, 0, arrow_size*v[0], arrow_size*v[1], 
                          head_width=0.02, head_length=0.02, linewidth=2, color='red')
            ax.text(v[0]*text_pos, v[1]*text_pos, self._obj.columns[i], color='black', 
                         ha='center', va='center', fontsize=18)
            
        ax.set_xlabel("Dimension 1", fontsize=14)
        ax.set_ylabel("Dimension 2", fontsize=14)
        ax.set_title("PC plane with original feature projections.", fontsize=16);
        return ax
    
    def pcaResults(self, model):
        '''
        Create a DataFrame of the PCA results
        Includes dimension feature weights and explained variance
        Visualizes the PCA results
        
        Parameters
        ----------
        model: fitted model object
        
        Usage
        --------
        df.pca.pcaResults()

        Returns
        -------
        pandas dataframe
        '''
        # Dimension indexing
        dimensions = dimensions = ['Dimension {}'.format(i) for i in range(1,len(model.components_)+1)]
        # PCA components
        components = pd.DataFrame(np.round(model.components_, 4), columns = list(self._obj.keys()))
        
        components.index = dimensions

        # PCA explained variance
        ratios = model.explained_variance_ratio_.reshape(len(model.components_), 1)
        variance_ratios = pd.DataFrame(np.round(ratios, 4), columns = ['Explained Variance'])
        variance_ratios.index = dimensions

        # Create a bar plot visualization
        fig, ax = plt.subplots(figsize = (14,8))
        # Plot the feature weights as a function of the components
        components.plot(ax = ax, kind = 'bar');
        ax.set_ylabel("Feature Weights")
        ax.set_xticklabels(dimensions, rotation=0)
        # Display the explained variance ratios
        for i, ev in enumerate(model.explained_variance_ratio_):
            ax.text(i-0.40, ax.get_ylim()[1] + 0.05, "Explained Variance\n          %.4f"%(ev))
            # Return a concatenated DataFrame
        return pd.concat([variance_ratios, components], axis = 1)
    
    def visualizeParetoPlot(self,model):
        '''
        Plot a Combo-Plot for Variance per dimension and Cumulative Variance
        
        Parameters
        ----------
        model: fitted model object
        
        Usage
        --------
        df.pca.visualizeParetoPlot()

        Returns
        -------
        Matplotlib object
        '''
    #Calculating variance per dimension for fitted data
        exp_var_ratio = pd.DataFrame(model.explained_variance_ratio_)
        exp_var_ratio.columns = ['Variance']
    #Computing cumulative variance for dimension reduction
        try:
            cum_var_exp = np.cumsum(exp_var_ratio.Variance)*100
        except Exception as python_error_message:
            print("Error while computing cumulative variance")
            print(python_error_message)
            
        exp_var_ratio['Cumulative Variance'] = cum_var_exp
    #Combo-Plot for Variance per dimension and Cumulative Variance
        fig, ax = plt.subplots(figsize = (15,6))
        sns.barplot(x=[i+1 for i in range(len(exp_var_ratio))], y='Variance',  data = exp_var_ratio, ax = ax, color = 'grey')
        ax.twinx()
        sns.pointplot(x = [i+1 for i in range(len(exp_var_ratio))], y = 'Cumulative Variance', data = exp_var_ratio, color = 'orange')
        return ax
            
    def visualizeScreePlot(self,model):
        '''
        Creates a Scree plot of the PCA to determine optimum number of pricipal components
        
        Parameters
        ----------
        model: fitted model object
        
        Usage
        --------
        df.kmeans.visualizeScreePlot()

        Returns
        -------
        Matplotlib object
        '''
        fig = plt.figure(figsize=(20,10)).gca()
        # make the x ticks integers
        fig.xaxis.set_major_locator(MaxNLocator(integer=True))
        #storing eigenvalues of corresponding principal component
        try:
            eigvals = model.explained_variance_ratio_ * 100
        except Exception as python_error_message:
            print("Error while calculating eigenvalues")
            print(python_error_message)
        sing_vals = np.arange(len(eigvals)) + 1
        plt.plot(sing_vals, eigvals, 'ro-', linewidth=2)
        plt.title('Scree Plot')
        plt.xlabel('Principal Component')
        plt.ylabel('Percentage of explained variance')
        return fig
        
    def visualizeScoreplot(self,data_reduced,target):
        '''
        This function Creates a Score plot of the PCA to to assess the data structure and detect clusters
        
        Parameters
        ----------
        data_reduced: input transformed data through PCA
        
        target: target variiable from original dataframe
        
        Usage
        --------
        df.kmeans.visualizeScoreplot()

        Returns
        -------
        Plotly Object
        '''
        # creating empty list to store trace   
        traces = []
        # casting numpy integers to python integers
        if(self._obj[target].unique().dtype=='O'):
            data1 = self._obj[target].unique()
        else:
            data1 = [int(item) for item in self._obj[target].unique()]
    
        for name in data1:
        #create trace
            trace = go.Scatter(
                x=data_reduced.values[self._obj[target]==name,0],
                y=data_reduced.values[self._obj[target]==name,1],
                mode='markers',
                name=name,
                marker=go.scatter.Marker(
                    size=12,
                    line=scatter.marker.Line(
                        color='rgba(217, 217, 217, 0.14)',
                        width=0.5),
                    opacity=0.8))
            traces.append(trace)
    
    
        data = Data(traces)
        layout = go.Layout(xaxis=go.layout.XAxis(title='PC1', showline=False),
                            yaxis=go.layout.YAxis(title='PC2', showline=False))
        fig = go.Figure(data=data, layout=layout)
        return fig
    
    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)