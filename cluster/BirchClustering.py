import pandas as pd
from sklearn.cluster import Birch
import matplotlib.pyplot as plt
import seaborn as sns; sns.set()
from IPython.core.interactiveshell import InteractiveShell
InteractiveShell.ast_node_interactivity = "all"                  

@pd.api.extensions.register_dataframe_accessor("BirchClustering")
class birchClustering(object):
    """
    Contains functions to build, predict and visualise birch 
    1. buildModel() - Builds affinity prpoagation model with given hyperparameters
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - returns visualised plot with cluster labels
    
    """
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def buildModel(self,threshold=0.1, branching_factor=5, n_clusters=3, compute_labels=True, copy=True):
        """
        Builds a model for birch using given hyperparameters
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        threshold : The radius of the subcluster obtained by merging a new sample and the closest subcluster should be lesser than the threshold. Otherwise a new subcluster is started. Setting this value to be very low promotes splitting and vice-versa.
        
        branching_factor : Maximum number of CF subclusters in each node. If a new samples enters such that the number of subclusters exceed the branching_factor then that node is split into two nodes with the subclusters redistributed in each. The parent subcluster of that node is removed and two new subclusters are added as parents of the 2 split nodes.
        
        n_clusters : Number of clusters after the final clustering step, which treats the subclusters from the leaves as new samples.
        
        compute_labels : Whether or not to compute labels for each fit.
        
        copy : Whether or not to make a copy of the given data. If set to False, the initial data will be overwritten.
            
        Usage
        --------
        df.BirchClustering.buildModel()

        Returns
        -------
        The model fit 

        
        """
        clustering = Birch(threshold=threshold, branching_factor=branching_factor,n_clusters=n_clusters,compute_labels=compute_labels,copy=copy)
        clustering.fit(self._obj)   
        return(clustering)
    
    
    def predictCluster(self,model):
        
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        
        model : model object
        
        Usage
        --------
        df.BirchClustering.buildModelLogistic()

        Returns
        -------
        The model fit 
        
        """
        df = self._obj.copy()
        labels = model.labels_
        df['clusters'] = labels
        return df

       
    def visualizeCluster(self,var1,var2,model):
        """
        This function used in vizualizeClusterIpy() to plot Birch cluster visualization
        
        Parameters
        ----------
        df: dataframe with the KMeans clustering to be used for visualization
        
        var1: X axis variable
        
        var2: Y axis variable
        
        centers: centers of the KMeans clustering 
        
        Usage
        --------
        df.BirchClustering.visualizeCluster()

        Returns
        -------
        The model fit 
        
        """
        df = self.predictCluster(model)
        # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(14, 7)
        # returns a jet colormap
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)

        ax2.legend()            
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt