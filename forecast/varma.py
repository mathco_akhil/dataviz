# -*- coding: utf-8 -*-
"""
Created on Fri Mar  7 12:18:26 2019

@author: Akhil Saraswat
"""

import pandas as pd
import numpy as np
from statsmodels.tsa.statespace.varmax import VARMAX

@pd.api.extensions.register_dataframe_accessor("varma")
class Varma(object):
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def build(self, depVar, exog=None, order=(1, 0), trend='c', error_cov_type='unstructured', measurement_error=False, enforce_stationarity=True, enforce_invertibility=True, start_params=None, transformed=True, cov_type='opg', cov_kwds=None, method='lbfgs', maxiter=50, full_output=1, disp=5, callback=None, return_params=False, optim_score=None, optim_complex_step=None, optim_hessian=None, flags=None):
        """
        Parameters
        ----------
        df : pandas dataframe or array-like
            The endogenous variable.
        depVar : list of dependent variable
            The column name of the dependent variable
        exog : array_like, optional
            Array of exogenous regressors, shaped nobs x k.
        order : iterable
            The (p,q) order of the model for the number of AR and MA parameters to
            use.
        trend : {'nc', 'c'}, optional
            Parameter controlling the deterministic trend polynomial.
            Can be specified as a string where 'c' indicates a constant intercept
            and 'nc' indicates no intercept term.
        error_cov_type : {'diagonal', 'unstructured'}, optional
            The structure of the covariance matrix of the error term, where
            "unstructured" puts no restrictions on the matrix and "diagonal"
            requires it to be a diagonal matrix (uncorrelated errors). Default is
            "unstructured".
        measurement_error : boolean, optional
            Whether or not to assume the endogenous observations `endog` were
            measured with error. Default is False.
        enforce_stationarity : boolean, optional
            Whether or not to transform the AR parameters to enforce stationarity
            in the autoregressive component of the model. Default is True.
        enforce_invertibility : boolean, optional
            Whether or not to transform the MA parameters to enforce invertibility
            in the moving average component of the model. Default is True.
            kwargs
        Keyword arguments may be used to provide default values for state space
        matrices or for Kalman filtering options. See `Representation`, and
        `KalmanFilter` for more details.
      
        Notes
        -----
        Generically, the VARMAX model is specified (see for example chapter 18 of
        [1]_):
      
        .. math::
        
            y_t = \nu + A_1 y_{t-1} + \dots + A_p y_{t-p} + B x_t + \epsilon_t +
            M_1 \epsilon_{t-1} + \dots M_q \epsilon_{t-q}
      
        where :math:`\epsilon_t \sim N(0, \Omega)`, and where :math:`y_t` is a
        `k_endog x 1` vector. Additionally, this model allows considering the case
        where the variables are measured with error.
        
        Note that in the full VARMA(p,q) case there is a fundamental identification
        problem in that the coefficient matrices :math:`\{A_i, M_j\}` are not
        generally unique, meaning that for a given time series process there may
        be multiple sets of matrices that equivalently represent it. See Chapter 12
        of [1]_ for more informationl. Although this class can be used to estimate
        VARMA(p,q) models, a warning is issued to remind users that no steps have
        been taken to ensure identification in this case.
        """
        df = self._obj
        if depVar is not None:
            try:
                model = VARMAX(df[depVar], exog=exog, order=order, trend=trend, error_cov_type=error_cov_type, measurement_error=measurement_error, enforce_stationarity=enforce_stationarity, enforce_invertibility=enforce_invertibility)
            except Exception as e:
                print("Unable to plot arima with the given depVar")
                print(e)
                
        else:
            try:
                model = VARMAX(df[depVar], exog=exog, order=order, trend=trend, error_cov_type=error_cov_type, measurement_error=measurement_error, enforce_stationarity=enforce_stationarity, enforce_invertibility=enforce_invertibility)
            except Exception as e:
                print("Unable to create Arima model, please provide depVar")
                print(e)
        try:
            model_fit = model.fit(start_params=start_params, transformed=transformed, cov_type=cov_type, cov_kwds=cov_kwds, method=method, maxiter=maxiter, full_output=full_output, disp=disp, callback=callback, return_params=return_params, optim_score=optim_score, optim_complex_step=optim_complex_step, optim_hessian=optim_hessian, flags=flags)
        except Exception as e:
            print("Unable to fit the model")
            print(e)
        model1 = "VARMA"
        return model_fit
    
    
    def getSummary(self,model_fit,depVar):
        """
        This Function retruns the summary of Sarima function containing
        MAPE, residuals description etc.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of Varma
        
        depVar           : column name of the dependent variable as string
        
        """
        df = self._obj
        print(model_fit.summary())
        return model_fit.summary()
    
    
    def scoreTrain(self,model_fit,depVar):
        """
        This function return the predicted values for train set and print
        MAPE for the Train set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of Varma
        
        depVar           : column name of the dependent variable as string
        """
        
        df=self._obj
        mape={}
        predicted=[]
        for i in depVar:
            predicted.append(i+'predicted')
        try:
            residuals = model_fit.resid
        except Exception as e:
            print("model_fit don't have residual function")
            print(e)
        try:
            # Calculating predicted values using predict function
            df[predicted] =model_fit.predict(start=0,end=len(df))
        except Exception as e:
            print("unable to run predict function the dataframe")
            print(e)
        try:
            for i in range(len(depVar)):
                mape[depVar[i]]= np.mean(np.abs((residuals[depVar[i]])/df[depVar[i]])) * 100
        except Exception as e:
            print("Error occured while calculating mape")
            print(e)
            
            
        print("Train MAPE : ",mape)
        return df
    
    def scoreTest(self,model_fit,depVar):
        """
        This function return the forecasted values for test set and print
        MAPE for the test set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of Varma
        
        depVar           : column name of the dependent variable as string
        """
        df=self._obj
        predicted = []
        mape = {}
        for i in depVar:
            predicted.append(i+'predicted')
        if depVar is not None:
            try:
                df[predicted]=model_fit.forecast(len(df))
            except Exception as e:
                print("Unable to forecast with the model")
                print(e)
            
            
            try:
                for i in range(len(depVar)):
                    mape[depVar[i]] = np.mean(np.abs((df[depVar[i]]-df[predicted[i]]) / df[depVar[i]]))* 100
            except Exception as e:
                print("Error occured while calculating mape")
                print(e)
            
            
            try:
                print("Test MAPE : ",mape)
            except Exception as e:
                print("Mape value does not exist")
                print(e)
        else:
            
            try:
                print("Test MAPE : ",mape)
            except Exception as e:
                print("Mape value does not exist")
                print(e)
        return df