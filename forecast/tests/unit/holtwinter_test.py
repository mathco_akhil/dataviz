# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 01:23:33 2019

@author: Akhil Saraswat
"""

import unittest
import pandas as pd
import sys
import os
import pickle
import numpy
import holtWinter

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

class TestHoltWinterMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.holtWinter.build('value',dates=self.train.index, seasonal_periods=15, seasonal='add')

    def test_build(self):
        model_fit = self.build_model
        pickleFile = open("hw", 'rb')
        model_file = pickle.load(pickleFile)
        pickleFile.close()
        pd.util.testing.equalContents(str(model_fit),str(model_file))
        
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.holtWinter.scoreTrain(model_fit,'value')
        pd.util.testing.assert_frame_equal(df_train, pd.read_csv("hwTrainPred.csv", index_col='date', parse_dates=['date']))
        
    def test_scoreTest(self):
        model_fit = self.build_model
        df_test = self.test.holtWinter.scoreTest(model_fit,'value')
        pd.util.testing.assert_frame_equal(df_test, pd.read_csv("hwTestPred.csv", index_col='date', parse_dates=['date']))
        