# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 10:23:33 2019

@author: Akhil Saraswat
"""

import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir, os.getcwd())))
import sarima

class TestSarimaMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.sarima.build(depVar='value', order = (4,0,1), seasonal_order = (6,0,5,3))

    def test_build(self):
        model_fit = self.build_model
        pickleFile = open("sarima", 'rb')
        model_file = pickle.load(pickleFile)
        pickleFile.close()
        pd.util.testing.equalContents(str(model_fit),str(model_file))
        
    def test_getSummary(self):
        model_fit = self.build_model
        summaryfile = open(r"summarySarima.txt", 'rb')
        pd.util.testing.equalContents(str(self.train.sarima.getSummary(model_fit)),str(summaryfile.read()))
        summaryfile.close()
    
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.sarima.scoreTrain(model_fit,'value')
  #      df_train = df_train.round(0)
        expected = pd.read_csv("sarimaTrainPred.csv", index_col='date', parse_dates=['date'])
   #     expected = expected.round(0)
        pd.util.testing.assert_frame_equal(df_train,expected,check_less_precise=1)
        
#    def test_scoreTest(self):
#        model_fit = self.build_model
#        df_test = self.test.sarima.scoreTest(model_fit,'value')
#        df_test = df_test.round(0)
#        expected_out = pd.read_csv("sarimaTestPred.csv", index_col='date', parse_dates=['date'])
#        expected_out = expected_out.round(0)
#        pd.util.testing.assert_frame_equal(df_test,expected_out,check_less_precise=1)