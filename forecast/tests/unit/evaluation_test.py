# -*- coding: utf-8 -*-
"""
Created on Wed Mar 20 10:17:34 2019

@author: TheMathCompany
"""
import unittest
import pandas as pd
import sys
import os
import pickle
import numpy
import evaluation
import arima

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

class TestEvaluationMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.arima.build(order=(6,0,4),depVar='value')
        
    def test_mape(self):
        model_fit = self.build_model
        pd.util.testing.equalContents(str(self.train.evaluation.mape(model_fit,'value')),str(5.562567536456646))
    
    def test_aic(self):
        model_fit = self.build_model
        pd.util.testing.equalContents(str(self.train.evaluation.aic(model_fit)),str(526.3239506754219))
        
    def test_residuals(self):
        model_fit = self.build_model
        summaryfile = open("residuals.txt")
        pd.util.testing.equalContents(str(self.train.evaluation.residuals(model_fit)),summaryfile.read())
        summaryfile.close()