# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 01:23:33 2019

@author: Akhil Saraswat
"""
import unittest
import pandas as pd
import sys
import os
import pickle
import numpy
import UCM

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

class TestUCMMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.ucm.build(depVar='value',seasonal=6,autoregressive=2,irregular=True)

    def test_build(self):
        model_fit = self.build_model
        pickleFile = open("UCM", 'rb')
        model_file = pickle.load(pickleFile)
        pickleFile.close()
        pd.util.testing.equalContents(str(model_fit),str(model_file))
        
    def test_getSummary(self):
        model_fit = self.build_model
        summaryfile = open(r"summaryUCM.txt", 'r')
        pd.util.testing.equalContents(str(self.train.ucm.getSummary(model_fit)),summaryfile.read())
        summaryfile.close()
    
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.ucm.scoreTrain(model_fit,'value')
        df_train = df_train.round(1)
        expe = pd.read_csv("UCMTrainPred.csv", index_col='date', parse_dates=['date'])
        expe = expe.round(1)
        pd.util.testing.assert_frame_equal(df_train,expe)
        
    def test_scoreTest(self):
        model_fit = self.build_model
        df_test = self.test.ucm.scoreTest(model_fit,'value')
        df_test = df_test.round(1)
        expe2 = pd.read_csv("UCMTestPred.csv", index_col='date', parse_dates=['date'])
        expe2 = expe2.round(1)
        pd.util.testing.assert_frame_equal(df_test,expe2)