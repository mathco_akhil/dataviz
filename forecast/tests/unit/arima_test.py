import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import arima

class TestArimaMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.arima.build(order=(6,0,4),depVar='value')
        
    def test_getSummary(self):
        model_fit = self.build_model
        summaryfile = open(r"summaryArima.txt", 'rb')
        pd.util.testing.equalContents(str(self.train.arima.getSummary(model_fit)),summaryfile.read())
        summaryfile.close()
    
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.arima.scoreTrain(model_fit,'value')
        df_train = df_train.round(1)
        expect = pd.read_csv("trainPred.csv", index_col='date', parse_dates=['date'])
        expect=expect.round(1)
        pd.util.testing.assert_frame_equal(df_train,expect)
        
    def test_scoreTest(self):
        model_fit = self.build_model
        df_test = self.test.arima.scoreTest(model_fit,'value')
        df_test = df_test.round(1)
        print(df_test)
        expect=pd.read_csv("testPred.csv", index_col='date', parse_dates=['date'])
        expect=expect.round(1)
        print(expect)
        pd.util.testing.assert_frame_equal(df_test,expect)
        