import unittest
import pandas as pd
import sys
import os
import pickle
import numpy
import varma

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

class TestVarmaMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_v2.csv", index_col='date', parse_dates=['date'])
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.varma.build(depVar=['value','value2'])
        
    def test_getSummary(self):
        model_fit = self.build_model
        summaryfile = open(r"summaryVarma.txt", 'rb')
        pd.util.testing.equalContents(str(self.train.varma.getSummary(model_fit,['value','value2'])),summaryfile.read())
        summaryfile.close()
    
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.varma.scoreTrain(model_fit,['value','value2'])
        pd.util.testing.assert_frame_equal(df_train, pd.read_csv("varmaTrainPred.csv", index_col='date', parse_dates=['date']))
        
    def test_scoreTest(self):
        model_fit = self.build_model
        df_test = self.test.varma.scoreTest(model_fit,['value','value2'])
        pd.util.testing.assert_frame_equal(df_test, pd.read_csv("varmaTestPred.csv", index_col='date', parse_dates=['date']))
        