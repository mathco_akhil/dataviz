import unittest
from sample_test import TestStringMethods
from arima_test import TestArimaMethods
from sarima_test import TestSarimaMethods
from ucm_test import TestUCMMethods
from holtwinter_test import TestHoltWinterMethods
from expsmoothing_test import TestExpSmoothingMethods
from varma_test import TestVarmaMethods
from evaluation_test import TestEvaluationMethods

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestStringMethods))
    suite.addTest(unittest.makeSuite(TestArimaMethods))
    suite.addTest(unittest.makeSuite(TestSarimaMethods))
    suite.addTest(unittest.makeSuite(TestUCMMethods))
    suite.addTest(unittest.makeSuite(TestHoltWinterMethods))
    suite.addTest(unittest.makeSuite(TestExpSmoothingMethods))
    suite.addTest(unittest.makeSuite(TestVarmaMethods))
    suite.addTest(unittest.makeSuite(TestEvaluationMethods))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())