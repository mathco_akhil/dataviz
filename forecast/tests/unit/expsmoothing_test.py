# -*- coding: utf-8 -*-
"""
Created on Tue Mar 10 01:23:33 2019

@author: Akhil Saraswat
"""

import unittest
import pandas as pd
import sys
import os
import pickle
import numpy
import expSmoothing

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))

class TestExpSmoothingMethods(unittest.TestCase):

    def setUp(self):
        self.df = pd.read_csv(r"unemp_rate.csv")
        self.df.date = pd.to_datetime(self.df.date)
        self.df.index = self.df.date
        self.df.drop(['date'],axis=1,inplace=True)
        self.train = pd.DataFrame(self.df[:int(len(self.df)*7/10)])
        self.test = pd.DataFrame(self.df[int(len(self.df)*7/10):])
        self.build_model =self.train.expSmoothing.build(depVar='value', optimized=True)

    def test_build(self):
        model_fit = self.build_model
        pickleFile = open("exp", 'rb')
        model_file = pickle.load(pickleFile)
        pickleFile.close()
        pd.util.testing.equalContents(str(model_fit),str(model_file))
    
    def test_scoreTrain(self):
        model_fit = self.build_model
        df_train = self.train.expSmoothing.scoreTrain(model_fit,'value')
        df_train=df_train.round(2)
        exp = pd.read_csv("expTrainPred.csv", index_col='date', parse_dates=['date'])
        exp = exp.round(2)
        pd.util.testing.assert_frame_equal(df_train,exp)
        
    def test_scoreTest(self):
        model_fit = self.build_model
        df_test = self.test.expSmoothing.scoreTest(model_fit,'value')
        df_test = df_test.round(2)
        exp2 = pd.read_csv("expTestPred.csv", index_col='date', parse_dates=['date'])
        exp2 = exp2.round(2)
        pd.util.testing.assert_frame_equal(df_test,exp2)