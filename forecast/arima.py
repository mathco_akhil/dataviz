# -*- coding: utf-8 -*-
"""
Created on Fri Mar  7 12:18:26 2019

@author: Akhil Saraswat
"""

import pandas as pd
import numpy as np
from statsmodels.tsa.arima_model import ARIMA

@pd.api.extensions.register_dataframe_accessor("arima")
class Arima(object):
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def build(self, depVar=None, order=(0,0,0), exog=None, dates=None, freq=None, missing='none'):
        """
        Parameters
        ----------
        df : pandas dataframe or array-like
            The endogenous variable.
        depVar : dependent variable
            The column name of the dependent variable
        exog   : array_like or None, optional
            Array of exogenous regressors, shaped nobs x k.
            Exogenous variables.
            Series or dataframe of independent variables :math `x`
        order : iterable
            The (p,d,q) order of the model for the number of AR parameters,
            differences, and MA parameters to use.
        dates : array-like of datetime, optional
            An array-like object of datetime objects. If a pandas object is given
            for endog or exog, it is assumed to have a DateIndex.
        freq : str, optional
            The frequency of the time-series. A Pandas offset or 'B', 'D', 'W',
            'M', 'A', or 'Q'. This is optional if dates are given.
            'B' - business day, ie., Mon. - Fri.
            'D' - daily
            'W' - weekly
            'M' - monthly
            'A' - annual
            'Q' - quarterly
        """
        df = self._obj
        if depVar is not None:
            try:
                model = ARIMA(df[depVar], order=order, exog=exog, dates=dates, freq=freq, missing=missing)
            except Exception as e:
                print("Unable to plot arima with the given depVar")
                print(e)
                
        else:
            try:
                model = ARIMA(df, order=order, dates=dates, freq=freq, missing=missing)
            except Exception as e:
                print("Unable to create Arima model, please provide depVar")
                print(e)
        try:
            model_fit = model.fit(disp=0)
        except Exception as e:
            print("Unable to fit the model")
            print(e)
        return(model_fit)
    
    
    def getSummary(self,model_fit):
        summary = model_fit.summary()
        print(summary)
        return(summary)
    
    def scoreTrain(self,model_fit,depVar):
        """
        This function return the predicted values for train set and print
        MAPE for the Train set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of Arima
        
        depVar           : column name of the dependent variable as string
        """
        
        df=self._obj
        try:
            residuals = model_fit.resid
        except Exception as e:
            print("model_fit don't have residual function")
            print(e)
        if depVar is not None:
            # Calculating predicted values using predict function
            try:
                df['predicted'] =df[depVar] - model_fit.resid
            except:
                print("")
            try:
                mape = np.mean(np.abs(model_fit.resid/ df[depVar])) * 100
            except Exception as e:
                print("Error occured while calculating mape")
                print(e)
                
                
            print("Train MAPE : %f"%mape)
        else:
            
            
            try:
                mape = np.mean(np.abs((df[depVar]-df['predicted']) / df)) * 100
            except Exception as e:
                print("Unable to calculate mape, please provide depVar")
                print(e)
            
            
            try:
                # Calculating predicted values by subtracting residual errors in train set
                df['predicted'] =df-residuals
            except Exception as e:
                print("residuals values cannot be found")
                print(e)
            try:
                print("Train MAPE : %f"%mape)
            except Exception as e:
                print("Mape value does not exist")
                print(e)
        return df
    
    def scoreTest(self,model_fit,depVar):
        """
        This function return the forecasted values for test set and print
        MAPE for the test set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of Arima
        
        depVar           : column name of the dependent variable as string
        """
        df=self._obj
        if depVar is not None:
            try:
                df['predicted']=model_fit.forecast(len(df))[0]
            except Exception as e:
                print("Unable to forecast with the model")
                print(e)
            
            
            try:
                mape = np.mean(np.abs((df[depVar]-df['predicted']) / df[depVar]))* 100
            except Exception as e:
                print("Error occured while calculating mape")
                print(e)
            
            
            try:
                print("Test MAPE : %f"%mape)
            except Exception as e:
                print("Mape value does not exist")
                print(e)
        else:
            
            try:
                print("Test MAPE : %f"%mape)
            except Exception as e:
                print("Mape value does not exist")
                print(e)
        return df
    