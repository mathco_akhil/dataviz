# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 14:42:53 2019

@author: Akhil Saraswat
"""

import pandas as pd
import numpy as np
import itertools
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.statespace.sarimax import SARIMAX


class gridSearch:
    
    def searchArima(df,depVar, bestBy='MAPE',max_p=3,max_d=3,max_q=3,exog=None):
        """
        This function will search for best order for arima in range (0,0,0) to (max_p-1,max_d-1,max_q-1)
        Parameters
        ----------
        df                      : Pandas object
        bestBy                  : MAPE or AIC, basis for best model selection
        exog                    : dataframe of exogenous columns
        df                      : Pandas object
        bestBy                  : MAPE or AIC, basis for best model selection
        exog                    : dataframe of exogenous columns
        max_p                   : Maximum allowed value for p. If maximum allowed
                                  p is 4 then max_p=5
        max_d                   : Maximum allowed value for d. If maximum allowed d is 1 then
                                  max_d=2
        max_q                   : Maximum allowed value for q. If maximum allowed q is 3 then
                                  max_q=4
        max_q                   : Maximum allowed value for q. If maximum allowed m is 3 then
                                  max_q=4
        """
        try:
            p=range(0,max_p)
        except Exception as e:
            print("max_p has to be an integer greater than 0")
            print(e)
        try:
            d=range(0,max_d)
        except Exception as e:
            print("max_d has to be an integer greater than 0")
            print(e)
        try:
            q=range(0,max_q)
        except Exception as e:
            print("max_q has to be an integer greater than 0")
            print(e)
        # Generate all different combinations of p, q and q triplets
        try:
            pdq = list(itertools.product(p, d, q))
        except Exception as e:
            print("Unable to create list of all combination of pdq from the given value")
            print(e)
    
        
        try:
            bestAIC = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        try:
            bestMAPE = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        bestParam = None
        
        print('Running GridSearch')
        
        #use gridsearch to look for optimial arima parameters
        try:
            for param in pdq:       
                try:
                    model = ARIMA(df[depVar],order=param,exog=exog).fit(disp=0)
    
                    #if current run of AIC is better than the best one so far, overwrite it
                    if bestBy=='AIC':
                        if model.aic<bestAIC:
                            bestAIC = model.aic
                            bestParam = param
                    else:
                        mape = np.mean(np.abs(model.resid/ df[depVar])) * 100
                        if mape<bestMAPE:
                            bestMAPE = mape
                            bestParam = param
                except:
                    continue
        except Exception as e:
            print("unable to iterate through pdq")
            print(e)
                    
        return {'Best AIC':bestAIC,'order':bestParam}
        
    def searchSarima(df,depVar, bestBy='MAPE',max_p=3,max_d=3,max_q=3,max_m=12, exog=None, iter_m = False, seasonal_m=12, time_varying_regression=False, mle_regression=True, enforce_invertibility=False, enforce_stationarity=False):
        """
        This function will search for best order for sarima in range (0,0,0)(0,0,0,seasonal_m) to (max_p-1,max_d-1,max_q-1)(max_p-1,max_d-1,max_q-1,seasonal_m)
        Parameters
        ----------
        df                      : Pandas object
        bestBy                  : MAPE or AIC, basis for best model selection
        exog                    : dataframe of exogenous columns
        max_p                   : Maximum allowed value for p. If maximum allowed
                                  p is 4 then max_p=5
        max_d                   : Maximum allowed value for d. If maximum allowed d is 1 then
                                  max_d=2
        max_q                   : Maximum allowed value for q. If maximum allowed q is 3 then
                                  max_q=4
        max_m                   : Maximum allowed value for m. If maximum allowed m is 3 then
                                  max_m=4
        iter_m                  : boolean, If true, iterate m of seasonal_order from 0 to
                                : max_m
        seasonal_m              : int, if iter_m is False, fixed value of seasonal_m will be
                                  used in seasonal_order. Default value is 12
        enforce_stationarity    : boolean, optional
                                  Whether or not to transform the AR parameters to enforce
                                  stationarity in the autoregressive component of the model.
                                  Default is True.
        enforce_invertibility   : boolean, optional
                                  Whether or not to transform the MA parameters to enforce
                                  invertibility in the moving average component of the model.
                                  Default is True.
        time_varying_regression : boolean, optional
                                  Used when an explanatory variables, `exog`, are provided
                                  provided
                                  to select whether or not coefficients on the exogenous
                                  regressors are allowed to vary over time. Default is False.
        mle_regression          : boolean, optional
                                  Whether or not to use estimate the regression coefficients
                                  for the exogenous variables as part of maximum likelihood 
                                  estimation or through the Kalman filter (i.e. recursive 
                                  least squares). If `time_varying_regression` is True, this
                                  must be set to False. Defaultis True.

        Usage:
        ------
        from mathmodels.forecast.GridSearch import gridSearch
        gridSearch.searchSarima(df,depVar,max_p=3,max_d=2,max_q=4,enforce_invertibility=True)

        Output:
        -------
        return dictionary
        {'Best AIC':bestAIC,'order':(1,1,3),'seasonal_order':(2,1,2,12)}
        or
        {'Best MAPE':bestMAPE,'order':(1,1,3),'seasonal_order':(2,1,2,12)} 
        """
        try:
            p=range(0,max_p)
        except Exception as e:
            print("max_p has to be an integer greater than 0")
            print(e)
        try:
            d=range(0,max_d)
        except Exception as e:
            print("max_d has to be an integer greater than 0")
            print(e)
        try:
            q=range(0,max_q)
        except Exception as e:
            print("max_q has to be an integer greater than 0")
            print(e)
        m=range(0,max_m)
        # Generate all different combinations of p, q and q triplets
        try:
            pdq = list(itertools.product(p, d, q))
        except Exception as e:
            print("Unable to create list of all combination of pdq from the given value")
            print(e)
    
        # Generate all different combinations of seasonal p, q and q triplets
        try:
            if iter_m:
                seasonalPDQ = [(x[0], x[1], x[2], x[3]) for x in list(itertools.product(p, d, q, m))]
            else:
                seasonalPDQ = [(x[0], x[1], x[2], seasonal_m) for x in list(itertools.product(p, d, q))]
        except Exception as e:
            print("Unable to create list of all combination of seasonalPDQ from the given value")
            print(e)
    
        
        try:
            bestAIC = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        try:
            bestMAPE = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        bestParam = None
        bestSParam = None
        
        print('Running GridSearch')
        
        #use gridsearch to look for optimial arima parameters
        try:
            for param in pdq:
                
                try:
                    for paramSeasonal in seasonalPDQ:
                        try:
                            model = SARIMAX(df[depVar],
                                         order=param,
                                         seasonal_order=paramSeasonal,
										 exog = exog,
                                         enforce_stationarity=enforce_stationarity,
                                         enforce_invertibility=enforce_invertibility,
                                         time_varying_regression = time_varying_regression,
                                         mle_regression =mle_regression).fit()
    
    
                            #if current run of AIC is better than the best one so far, overwrite it
                            if bestBy=='AIC':
                                if model.aic<bestAIC:
                                    bestAIC = model.aic
                                    bestParam = param
                                    bestSParam = paramSeasonal
                            else:
                                try:
                                    mape = np.mean(np.abs(model_fit.resid/ df[depVar])) * 100
                                except:
                                    print("Unable to calcualte mape with parameters"+param+paramSeasonal)
                                if mape<bestMAPE:
                                    bestMAPE = mape
                                    bestParam = param
                                    bestSParam = paramSeasonal
    
                        except:
                            continue
                except Exception as e:
                    print("unable to iterate through seasonalPDQ")
                    print(e)
        except Exception as e:
            print("unable to iterate through pdq")
            print(e)
                    
        return {'Best AIC':bestAIC,'order':bestParam,'seasonal_order':bestSParam} if bestBy=='AIC' else {'Best MAPE':bestMAPE,'order':bestParam,'seasonal_order':bestSParam}