# -*- coding: utf-8 -*-
"""
Created on Tue Mar 19 11:41:51 2019

@author: TheMathCompany
"""
import pandas as pd
import numpy as np
@pd.api.extensions.register_dataframe_accessor("evaluation")
class evaluation(object):
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def mape(self,model_fit,depVar):
        """
        This Function retruns the MAPE of model given.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model
        
        depVar           : column name of the dependent variable as string
        
        """
        df = self._obj
        residuals = model_fit.resid
        mape = np.mean(np.abs((residuals)/df[depVar])) * 100
        print("MAPE : %f"%mape)
        return(mape)    

    def aic(self,model_fit):
        """
        This Function retruns the AIC of model given.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model
                
        """
        aic = model_fit.aic
        print("AIC : ",aic)
        return(aic)    
      
    def residuals(self,model_fit):
        """
        This function provide Descriptive statistics of residuals (actual - predicted).
        
        model_fit    : fitted model
        
        Example
        -------
        >>>df = pandas.read_csv("somefile.csv")
        >>>df.evaluation.residuals(model_fit)
        
        Residuals description
        ---------------------
        count    568.000000
        mean      -0.006776
        std        0.496796
        min       -1.157494
        25%       -0.358457
        50%       -0.091370
        75%        0.274799
        max        2.020931
        
        """
        print("Residuals description")
        print("---------------------")
        resids = model_fit.resid
        residuals = resids.describe()
        print(residuals)
        return(residuals)