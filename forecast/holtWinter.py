# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 14:23:38 2019

@author: TheMathCompany
"""

import pandas as pd
import numpy as np
from statsmodels.tsa.holtwinters import ExponentialSmoothing

@pd.api.extensions.register_dataframe_accessor("holtWinter")
class HoltWinter(object):
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
    
    def build(self,depVar,trend=None,damped=False,seasonal=None,seasonal_periods=None,dates=None,freq=None,missing='none'):
        """
        Exponential Smoothing
    
        Parameters
        ----------
        endog : array-like
            Time series
        trend : {"add", "mul", "additive", "multiplicative", None}, optional
            Type of trend component.
        damped : bool, optional
            Should the trend component be damped.
        seasonal : {"add", "mul", "additive", "multiplicative", None}, optional
            Type of seasonal component.
        seasonal_periods : int, optional
            The number of seasons to consider for the holt winters.
        freq : str, optional
        The frequency of the time-series. A Pandas offset or 'B', 'D', 'W',
        'M', 'A', or 'Q'. This is optional if dates are given.
        'B' - business day, ie., Mon. - Fri.
        'D' - daily
        'W' - weekly
        'M' - monthly
        'A' - annual
        'Q' - quarterly
            
        Notes
        -----
        This is a full implementation of the holt winters exponential smoothing.
        This includes all the unstable methods as well as the stable methods.
        The implementation of the library covers the functionality of the R 
        library as much as possible whilst still being pythonic.
        
        """
        df = self._obj
        try:
            model =ExponentialSmoothing(df[depVar], trend=trend, damped=damped, seasonal=seasonal, seasonal_periods=seasonal_periods, dates=dates, freq=freq, missing=missing)
        except Exception as e:
            print("Unable to create holt's winter model with the given data")
            print(e)
        try:
            model_fit = model.fit(use_boxcox=True)
        except Exception as e:
            print("Unable to fit holt's winter model to the dataset")
            print(e)
        return model_fit
    
    def scoreTrain(self,model_fit,depVar):
        """
        This function return the predicted values for train set and print
        MAPE for the Train set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of holt's winter
        
        depVar           : column name of the dependent variable as string
        """
        
        df = self._obj
        try:
            df['predicted']=model_fit.predict(start=0,end=len(df))
        except Exception as e:
            print("unable to run predict function the dataframe")
            print(e)
            
        try:
            mape = np.mean(np.abs((df[depVar]-df['predicted']) / df[depVar])) * 100
        except Exception as e:
            print("Error occured while calculating mape")
            print(e)
        try:
            print("Train MAPE : %f"%mape)
        except Exception as e:
            print("Mape value does not exist")
            print(e)
        return df
    
    def scoreTest(self,model_fit,depVar):
        """
        This function return the forecasted values for test set and print
        MAPE for the test set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of holt's winter
        
        depVar           : column name of the dependent variable as string
        """
        df = self._obj
        try:
            df['predicted']=np.array(model_fit.forecast(len(df)))
        except Exception as e:
            print("Unable to forecast for the given length of dataframe")
            print(e)
        try:
            mape = np.mean(np.abs((df[depVar]-df['predicted']) / df[depVar])) * 100
        except Exception as e:
            print("Error occured while calculating mape")
            print(e)
        try:
            print("Test MAPE : %f"%mape)
        except Exception as e:
            print("Mape value does not exist")
            print(e)
        return df