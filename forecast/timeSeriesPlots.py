# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 10:58:00 2019

@author: Akhil Saraswat
"""
import pandas as pd
import plotly.graph_objs as go
from plotly.offline import plot, iplot, init_notebook_mode
init_notebook_mode(connected=True)
from matplotlib import pyplot
import statsmodels.api as sm


@pd.api.extensions.register_dataframe_accessor("timeSeriesPlots")
class timeSeriesPlot:
    
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
    
###############################################################################
##FUNCTION: Gives a popup dialog box to select csv file
##INPUT: None
##OUTPUT: Dataframe selected by user
###############################################################################
    def plotAcfPacf(self,depVar,nlags=1):
        """
        This function calculates and plot Autocorrelation and Partial-Autocorrelation
        for the series with the difference of zero, one and two
        
        pandas_obj       : A pandas DataFrame
        
        depVar           : column name of the series
        
        nlags            : maximum number of desired lags
        
                      
         Usage
        --------
       
        df.univariate.generatePercentileDistributionPlot([continuousColumnName])
        """
        df = self._obj
        # ploting acf and pacf without any differencing
        print("Autocorrelation and Partial Autocorrelation")
        print("-------------------------------------------")
        try:
            fig = pyplot.figure(figsize=(12,8))
        except Exception as e:
            print("Unable to create pyplot object")
            print(e)
        try:
            ax1 = fig.add_subplot(211)
        except Exception as e:
            print("Unable to add subplot axes 211 to the figure")
            print(e)
        try:
            fig = sm.graphics.tsa.plot_acf(df[depVar].dropna(), lags=nlags, ax=ax1)
        except Exception as e:
            print("Cannot add acf plot from statsmodels library to the figure object")
            print(e)
        try:
            ax2 = fig.add_subplot(212)
        except Exception as e:
            print("Unable to add subplot axes 212 to the figure")
            print(e)
        try:
            fig = sm.graphics.tsa.plot_pacf(df[depVar].dropna(), lags=nlags, ax=ax2)
        except Exception as e:
            print("Cannot add pacf plot from statsmodels library to the figure object")
            print(e)
        try:
            pyplot.show()
        except Exception as e:
            print("Unable to display pyplot")
            print(e)
        return fig
            
    
    
    def stl(self, depVar, model='additive', filt=None, freq=None, two_sided=True, extrapolate_trend=0):
        """
        This function decompose and plot series into trend, seasonality and irregularity
        
        Parameters:
        -----------
        
        pandas_obj       : A pandas DataFrame
        
        depVar           : column name of the series
        
        model            : str {"additive", "multiplicative"}
                           Type of seasonal component. Abbreviations are accepted.
        filt             : array-like
                           The filter coefficients for filtering out the seasonal component.
                           The concrete moving average method used in filtering is determined by two_sided.
        freq             : int, optional
                           Frequency of the series. Must be used if x is not a pandas object.
                           Overrides default periodicity of x if x is a pandas
                           object with a timeseries index.
        two_sided        : bool
                           The moving average method used in filtering.
                           If True (default), a centered moving average is computed using the filt.
                           If False, the filter coefficients are for past values only.
        extrapolate_trend: int or 'freq', optional
                           If set to > 0, the trend resulting from the convolution is
                           linear least-squares extrapolated on both ends (or the single one
                           if two_sided is False) considering this many (+1) closest points.
                           If set to 'freq', use `freq` closest points. Setting this parameter
                           results in no NaN values in trend or resid components.
        
                      
         Usage
        --------
       
        df.univariate.generatePercentileDistributionPlot([continuousColumnName])
        """
        df = self._obj
        # decomposing data into trend, sesonal and irregular component
        try:
            stlDecompose = sm.tsa.seasonal_decompose(df[depVar], model=model, filt=filt, freq=freq, two_sided=two_sided, extrapolate_trend=extrapolate_trend)
        except Exception as e:
            print("Unable to run Seasonal Decompose with the given input")
            print(e)
        return stlDecompose.trend, stlDecompose.seasonal, stlDecompose.resid
    
    
    
    def stlFigure(self, trend, seasonal, resid):
        """
        This function plot trend, seasonality and irregularity
        
        Parameters:
        -----------
        self        : A pandas DataFrame
        trend       : trend series
        seasonal    : seasonal series
        resid       : Residual/ noise/ Irregularity series
        
        """
        # setting diffrent component of data into figure
        try:
            data = [go.Scatter(x=trend.index,y=trend)]
            layout = go.Layout(
                        title="Trend",
                        yaxis=dict(
                            title="Trend"
                        ),
                        xaxis=dict(
                            title="Timeline"
                        )
                    )
            trendPlot = go.Figure(data=data,layout=layout)
        except Exception as e:
            print("Cannot plot trend of the given data")
            print(e)
        try:
            data = [go.Scatter(x=seasonal.index,y=seasonal)]
            layout = go.Layout(
                        title="Seasonality",
                        yaxis=dict(
                            title="Seasonality"
                        ),
                        xaxis=dict(
                            title="Timeline"
                        )
                    )
            seasonalPlot = go.Figure(data=data,layout=layout)
        except Exception as e:
            print("Cannot plot seasonality of the given data")
            print(e)
        try:
            data = [go.Scatter(x=resid.index,y=resid)]
            layout = go.Layout(
                        title="Irregularity",
                        yaxis=dict(
                            title="Irregularity"
                        ),
                        xaxis=dict(
                            title="Timeline"
                        )
                    )
            irregularPlot = go.Figure(data=data,layout=layout)
        except Exception as e:
            print("Cannot plot irregularity of the given data")
            print(e)
        return trendPlot, seasonalPlot, irregularPlot
        
    
    
    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)
            else:
                plot(figure, config=config)