# Mathco Analysis Package

### Steps
- `git clone <repo>`
- `git checkout -b feature-dataclean-<method-name>`
- Work on your feature
- `git status`
- Check the changed files to make sure nothing extra is being committed
- `git add <filename>`
- `git commit -m "<commit message>"`
- `git status`
- Check whether commit is registered
- `git push`
- Then come to the web UI and raise a merge request 