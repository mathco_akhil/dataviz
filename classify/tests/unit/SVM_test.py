# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 11:15:58 2019

@author: Lenovo
"""

import unittest
import pandas as pd
import sys
import os
import numpy
import pickle
import array
testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import SVM

class TestSVMMethods(unittest.TestCase):

    def setUp(self):
        self.df1= pd.read_csv(r"data_train.csv")
        self.df2= pd.read_csv(r"data_test.csv")
        
    def test_buildModel(self):
        log2 = pickle.load(open("buildModelSVM.pkl", 'rb'))
        #log = pd.read_pickle(r"finalized_model.pkl")
        out = self.df1.supportVectorMachine.buildModel(depVar="y")
        numpy.testing.assert_string_equal(str(out),str(log2))    

    def test_scoreDataset(self):
         log = pickle.load(open("buildModelSVM.pkl", 'rb'))
         #log = pd.read_pickle(r"finalized_model.pkl")
         out = self.df2.supportVectorMachine.scoreDataset(svmModelResult=log,depVar="y")
         print(out)
         pd.util.testing.assert_frame_equal(out, pd.read_csv("dataset_testing.csv"))

        

        