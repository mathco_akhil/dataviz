import unittest
import pandas as pd
import sys
import os
import pickle
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import Logistic

class TestLogisticMethods(unittest.TestCase):

    def setUp(self):
        self.df1= pd.read_csv(r"data1.csv")
        
    
    def test_getSummary(self):
        log = pickle.load(open("finalized_model.pkl", 'rb'))
        out1 = self.df1.Logistic.getSummary(logModel=log)
        out2 = out1.as_text()
        f=open("LogisticSummary21.txt", "r")
        pd.util.testing.equalContents(out2,f.read())
           
        
    def test_scoreDataset(self):
        log = pickle.load(open("finalized_model.pkl", 'rb'))
        out = self.df1.Logistic.scoreDataset(logModel=log,depVar="y")
        pd.util.testing.assert_frame_equal(out,pd.read_csv("scoreDatasetOutput.csv"))
        
        
        