# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:50:12 2019

@author: Lenovo
"""

import pandas as pd
import sys
import os
import numpy
import pickle
import array
import unittest
testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import gradientBoosting

class TestGBMMethods(unittest.TestCase):

    def setUp(self):
        self.df1= pd.read_csv(r"data_train.csv")
        self.df2= pd.read_csv(r"data_test.csv")
        
    def test_buildModel(self):
        log2 = pickle.load(open("buildModelGradientBoosting.pkl", 'rb'))
        #log = pd.read_pickle(r"finalized_model.pkl")
        out = self.df1.gradientBoosting.buildModel(depVar="y",random_state=100)
        numpy.testing.assert_string_equal(str(out),str(log2))    
        
        
    def test_getSummary(self):
        log = pickle.load(open("buildModelGradientBoosting.pkl", 'rb'))
        out1 = self.df1.gradientBoosting.getSummary(gbmModelResult=log)
        print(out1)
        pd.util.testing.assert_frame_equal(out1,pd.read_csv("dataset_featureimportance.csv"))
   
        


    def test_scoreDataset(self):
        log = pickle.load(open("buildModelGradientBoosting.pkl", 'rb'))
          #log = pd.read_pickle(r"finalized_model.pkl")
        out = self.df2.gradientBoosting.scoreDataset(gbmModelResult=log,depVar="y")
        print(out)
        pd.util.testing.assert_frame_equal(out, pd.read_csv("dataset_testing_GBM.csv"))

