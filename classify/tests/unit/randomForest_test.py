import unittest
import pandas as pd
import sys
import os
import numpy
import pickle

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
os.chdir(os.path.dirname(__file__))
import randomForest

class TestRandomForestMethods(unittest.TestCase):

    def setUp(self):
        self.df1= pd.read_csv(r"data1.csv")
        
        
    def test_buildModel(self):
        log2 = pickle.load(open("buildModelRandom.pkl", 'rb'))
        #log = pd.read_pickle(r"finalized_model.pkl")
        out = self.df1.RandomForest.buildModel(depVar="y",random_state=113)
        numpy.testing.assert_string_equal(str(out),str(log2))    
        
        
    
    def test_getSummary(self):
        log = pickle.load(open("buildModelRandom.pkl", 'rb'))
        out1 = self.df1.RandomForest.getSummary(rfModelResult=log)
         #      out2 = out1.as_csv()
  #      print(out2)
  #      out=pd.DataFrame(out2)
        pd.util.testing.assert_frame_equal(out1,pd.read_csv("dataset_feature_Importance.csv"))
           
        
    def test_scoreDataset(self):
        log = pickle.load(open("buildModelRandom.pkl", 'rb'))
        #log = pd.read_pickle(r"finalized_model.pkl")
        out = self.df1.RandomForest.scoreDataset(rfModelResult=log,depVar="y")
        print(out)
        pd.util.testing.assert_frame_equal(out,pd.read_csv("dataset_testing_randomForest.csv"))
        
        
        