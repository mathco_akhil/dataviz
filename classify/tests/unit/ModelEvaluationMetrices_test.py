import unittest
import pandas as pd
import sys
import os
import numpy as np

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import ModelEvaluationMetrices

class TestModelEvaluationMetrices(unittest.TestCase):

    def setUp(self):        
        
        self.df = pd.read_csv("dataframe.csv")
    def test_createGains(self):
        output = self.df.ModelEvaluationMetrices.createGains(scoring =pd.DataFrame([[0.961659318],[0.733406],[0.600746],
                                        [0.445609],[0.477037],[0.961659318],
                                        [0.733406],[0.600746],[0.445609],
                                        [0.477037],[0.961659318],[0.733406],
                                        [0.600746],[0.445609],[0.477037]]),depVar="insuranceclaim")
        expected = pd.read_csv("gainsOut.csv")
        expected = expected.astype('object')
        pd.util.testing.assert_frame_equal(output,expected)
        
    def test_createKs(self):
        output_ks = self.df.ModelEvaluationMetrices.createKs(scoring =pd.DataFrame([[0.961659318],[0.733406],[0.600746],
                                        [0.445609],[0.477037],[0.961659318],
                                        [0.733406],[0.600746],[0.445609],
                                        [0.477037],[0.961659318],[0.733406],
                                        [0.600746],[0.445609],[0.477037]]),depVar="insuranceclaim")
        np.testing.assert_almost_equal(output_ks,50.0)
        
    def test_getConcordance(self):
        output_Concordance = self.df.ModelEvaluationMetrices.getConcordance(scoring =pd.DataFrame([[0.961659318],[0.733406],[0.600746],
                                        [0.445609],[0.477037],[0.961659318],
                                        [0.733406],[0.600746],[0.445609],
                                        [0.477037],[0.961659318],[0.733406],
                                        [0.600746],[0.445609],[0.477037]]),depVar="insuranceclaim").reset_index()
        pd.util.testing.assert_frame_equal(output_Concordance,pd.DataFrame([["Concordance",0.5],["Discordance",0.5],["Ties",0. ]],columns=["index",0]))
 
    def test_generateConfusionMatrix(self):
        output_ConfusionMatrix = self.df.ModelEvaluationMetrices.generateConfusionMatrix(scoring =pd.DataFrame([[0.961659318],[0.733406],[0.600746],
                                        [0.445609],[0.477037],[0.961659318],
                                        [0.733406],[0.600746],[0.445609],
                                        [0.477037],[0.961659318],[0.733406],
                                        [0.600746],[0.445609],[0.477037]]),depVar="insuranceclaim",threshold=0.5)
        pd.util.testing.assert_frame_equal(output_ConfusionMatrix,pd.DataFrame([[0, 3],[6, 6]]))       
        