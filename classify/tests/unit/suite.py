import unittest
from SVM_test import TestSVMMethods
from GBM_test import TestGBMMethods
from randomForest_test import TestRandomForestMethods
from Logistic_test import TestLogisticMethods
from ModelEvaluationMetrices_test import TestModelEvaluationMetrices

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestRandomForestMethods))
    suite.addTest(unittest.makeSuite(TestSVMMethods))
    suite.addTest(unittest.makeSuite(TestGBMMethods))
    suite.addTest(unittest.makeSuite(TestRandomForestMethods))
    suite.addTest(unittest.makeSuite(TestLogisticMethods))
    suite.addTest(unittest.makeSuite(TestModelEvaluationMetrices))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())