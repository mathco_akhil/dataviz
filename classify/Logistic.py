import pandas as pd
import statsmodels.api as sm
import pickle
@pd.api.extensions.register_dataframe_accessor("Logistic")
class Logistic(object):
    """
    
    Contains functions to build, score and generate summary for different models
    
    """


    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def buildModel(self, depVar, method ='newton'):        
        """
        This function builds the model for logistic regression
        It could be used to build an logistic model on the train data
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        depVar : Dependent variable from the dataset
        method : str, used to fit model
        The method determines which solver from scipy.optimize is used, and it can be chosen from among the following strings:’newton’ for Newton-Raphson, ‘nm’ for Nelder-Mead, ’bfgs’ for Broyden-Fletcher-Goldfarb-Shanno (BFGS),’lbfgs’ for limited-memory BFGS with optional box constraints,’powell’ for modified Powell’s method, ’cg’ for conjugate gradient, ’ncg’ for Newton-conjugate gradient, ’basinhopping’ for global basin-hopping solver, ’minimize’ for generic wrapper of scipy minimize (BFGS by default)
        The explicit arguments in fit are passed to the solver, with the exception of the basin-hopping solver. Each solver has several optional arguments that are not the same across solvers.
        
        Usage
        --------
        df.Logistic.buildModelLogistic(depVar='p')

        Returns
        -------
        The model fit 
        
        """ 
        y_train = self._obj[depVar]
        x_train = self._obj.drop(depVar, axis=1)
        logistic_model = sm.Logit(y_train, x_train)
        # add method=bfgs for checking perfect fit
        result = logistic_model.fit(method=method)
        # save the model to disk
        filename = 'buildModelLogistic.pkl'
        pickle.dump(result, open(filename, 'wb'))
        return(result)
        
        
    def getSummary(self,logModel):
        """
        This function gives a logistic model summary in Python
        The module takes input from logit model fit
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        logModel : logistic model 
        
        Usage
        --------
        df.Logistic.getLogisticSummary(logModel=a)

        Returns
        -------
        The model summary
                
        """
        a=logModel.summary()
        f = open(r"LogisticSummary21.txt",'w')
        f.write(a.as_text())
        f.close()
        return(logModel.summary())
        
        
    def scoreDataset(self,logModel,depVar=None):
        """
        This function predicts logistic model probabilities
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        logModel : logistic model 
        
        Usage
        --------
        df.Logistic.scoreDataset(logModel='a',depVar='p')

        Returns
        -------
        A dataframe with each of the data points scored
        
        """
        if depVar != None:
            x4 = self._obj.drop(depVar, axis=1)
            scoring_test = logModel.predict(x4)
            x4["Pred_Prob"] = scoring_test
            return(x4)
        else:
            scoring_test = logModel.predict(self._obj)
            self._obj["Pred_Prob"] = scoring_test
            return(self._obj)