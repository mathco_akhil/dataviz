import unittest
from dataclean_test import TestDatacleanMethods
from bivariate_test import TestBivariateMethods
from univariate_test import TestUnivariateMethods

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(TestDatacleanMethods))
    suite.addTest(unittest.makeSuite(TestBivariateMethods))
    suite.addTest(unittest.makeSuite(TestUnivariateMethods))
    return suite

if __name__ == '__main__':
    runner = unittest.TextTestRunner()
    runner.run(suite())