import unittest
import pandas as pd
import sys
import os
import numpy
from scipy import stats
testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import dataclean



class TestDatacleanMethods(unittest.TestCase):

    def setUp(self):
        self.df_nos = pd.DataFrame([[1,2,3,4], [3,4,5,6], [5,6,7,8]])
        self.df_str = pd.DataFrame([['a', 'b', 'c', 'd'], ['x', 'y', 'z', 't']])
        self.df_date = pd.DataFrame([['2018-01-01', '2019-01-01'], ['2017-02-02', '2015/01/01'], ['2015-12-12', '2222/01/01']])
        self.df_mixed = pd.DataFrame([[1,'a',3,4], [3,'h',5,6], ['x','p',7,8]])
        self.df_duplicates=pd.DataFrame([[1,4,3,4],[2,4,3,4],[1,4,9,4]])
        self.df_df1= pd.read_csv(r"findContinuousInput.csv")
        self.df_df2 = pd.read_csv(r"findDuplicatesInput.csv")
        self.df_df3 = pd.read_csv(r"removeDuplicatesInput.csv")
        self.df_df4= pd.read_csv(r"findConstantsInput.csv")
        self.df_df5= pd.read_csv(r"removeConstantsInput.csv")
        self.df_outliers = pd.read_csv("Test_Outlier_Input.csv")
        self.df_impute = pd.read_csv('Test_Imputation_Input.csv')
        self.df_scale = pd.read_csv('Test_Scale_Input.csv')
        self.df_ids = pd.DataFrame([['12mm015',3,5], ['12mm016',4,5], ['12mm017',6,7]], columns=['ID','col1','col2'])
        self.df_time = pd.read_csv('database_Input.csv')
        self.df_string=pd.read_csv("findCategorical_Input.csv")
        self.df_df6 = pd.read_csv(r"inputToCategorical.csv")
        self.df_missingPercent = pd.read_csv(r"database_input.csv")
        
    def test_findContinuousColumns(self):
        self.df_nos.dataclean.findContinuousColumns(threshold=2)
        self.df_mixed.dataclean.findContinuousColumns(threshold=2)
#        self.assertEqual(self.df_nos.dataclean.findContinuousColumns(),[0, 1, 2, 3])
#        self.assertEqual(self.df_mixed.dataclean.findContinuousColumns(),[0, 2, 3])
        self.assertEqual(self.df_df1.dataclean.findContinuousColumns(),['Life expectancy ','Adult Mortality','infant deaths','Alcohol','percentage expenditure','Hepatitis B','Measles ',' BMI ','under-five deaths ','Life expectancy ','under-five deaths '])
        
        
    def test_findDuplicatesColumns(self):
        pd.util.testing.assert_frame_equal(self.df_df2.dataclean.findDuplicateColumns(), pd.read_csv("findDuplicatesOutput.csv"),check_column_type = False)
             
    def test_removeDuplicates(self):
        pd.util.testing.assert_frame_equal( self.df_df3.dataclean.removeDuplicates(),pd.read_csv(r"removeDuplicatesOutput.csv"),check_dtype=False)
        
#    def test_findConstantColumns(self):       
#        self.assertEqual(self.df_df4.dataclean.findConstantColumns(),['Hyperlinks2', 'constant', 'constant2'])
#                
#    def test_removeConstants(self):   
#        pd.util.testing.assert_frame_equal(self.df_df5.dataclean.removeConstants(), pd.read_csv(r"removeConstantsOutput.csv"))
        
    def test_removeOutliers(self):
        pd.util.testing.assert_frame_equal(self.df_outliers.dataclean.removeOutliers(), pd.read_csv("Test_Outlier_Output.csv"))
        
    def test_imputeData(self):
        df1=pd.read_csv("Test_Imputation_Output.csv",dtype={'A':'int64','B':'float64','C':'object','D':'float64','E':'object'})
        pd.util.testing.assert_frame_equal(self.df_impute.dataclean.imputeData(),df1)
        
    def test_scaleData(self):
        pd.util.testing.assert_frame_equal(pd.read_csv("Test_Scale_Output.csv"),self.df_scale.dataclean.scaleData(contcols=['Revenue']))
        
    def test_removePrimaryKey(self):
        self.df_ids.dataclean.removePrimaryKey(columnList=['ID'])
        pd.util.testing.assert_frame_equal(self.df_ids, pd.DataFrame([[3,5], [4,5], [6,7]], columns = ['col1','col2']))
        
    def test_findDatetime(self):
        self.assertEqual(self.df_time.dataclean.findDatetime(),['Date','Time'])

    def test_removeDatetime(self):
        #self.df_time.dataclean.removeDatetime()
        pd.util.testing.assert_frame_equal(self.df_time.dataclean.removeDatetime(utc=True),pd.read_csv('database_Output.csv'))

        
    def test_findPrimaryKey(self):
        list_primarykey = self.df_time.dataclean.findPrimaryKey(threshold = 0.90)
        self.assertEqual(list_primarykey,["ID"])
        
    def test_findCategoricalColumns(self):
        self.assertEqual(self.df_string.dataclean.findCategoricalColumns(),['Contract','PaperlessBilling','PaymentMethod','Contract','PaperlessBilling','PaymentMethod','MonthlyCharges'])
        
    def test_toCategorical(self):
        self.assertEqual(self.df_df6.dataclean.toCategorical(contCols=['BALANCE','PURCHASES','TENURE','Rating'],catCols=['CUST_ID','Gender'],toCatCols=['Rating']),(['TENURE', 'PURCHASES', 'BALANCE'], ['CUST_ID', 'Gender', 'Rating']))    
        
    def test_getMissingPercent(self):
        a=self.df_missingPercent.dataclean.getMissingPercent()
        pd.util.testing.assert_frame_equal(a, pd.read_csv(r"getMissingPercentOutput.csv"),check_index_type=False)    
        
#    def test_removeMissingColumns(self):
#        self.df_removeMissingColumns.univariate.removeMissingColumns()
#        pd.util.testing.assert_frame_equal(self.df_removeMissingColumns.univariate.removeMissingColumns(),pd.read_csv('outputRemoveMissingColumns.csv'))    
#        
#    