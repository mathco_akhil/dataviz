import datetime

INTS = ['int16', 'int32', 'int64', 'int']
FLOATS = ['float16', 'float32', 'float64', 'float']
COMPLEX = ['complex64', 'complex128']
NUMS = ['int', 'int16', 'int32', 'int64', 'float16', 'float32', 'float64','float']
STRINGS = []
DATES = []
NAN = ['None', '', ' ', 'nan', 'Nan', 'NaN']