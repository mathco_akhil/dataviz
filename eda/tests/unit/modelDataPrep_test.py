import unittest
import pandas as pd
import sys
import os
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import ModelDataPrep

class TestModelDataPrepMethods(unittest.TestCase):

	def setUp(self):
		self.df = pd.read_csv("Insurance_Data.csv")
		self.df_train = pd.read_csv("insurance_subset.csv")

	def test_VIF(self):
		pd.util.testing.assert_frame_equal(self.df.ModelDataPrep.getVif("insuranceclaim"), pd.read_csv("VIF.csv"))

	def test_splitTrainAndTest(self):
		pd.util.testing.assert_frame_equal(self.df_train.ModelDataPrep.splitTrainAndTest("insuranceclaim")[0].reset_index(drop = True), pd.read_csv("trainset.csv").reset_index(drop = True), check_dtype = False)