import unittest
import pandas as pd
import sys
import os
import numpy

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import bivariate

class TestBivariateMethods(unittest.TestCase):

    def setUp(self):
        self.df_basic = pd.DataFrame([[1,2,1,0],[3,4,0,0],[5,6,1,1],[7,8,0,0],[9,10,1,0],[11,12,0,0],[16,13,1,1],[3,7,0,1],[22,12,1,0],[9,61,1,1],[15,88,1,1],[13,31,1,0],[8,21,1,1],[10,23,1,0],[26,29,1,1],[55,24,1,0],[16,3,0,1]],columns=['col1','col2','col3','col4'])

    def test_generateBivaraiteReport(self):
        self.df_basic.bivariate.generateBivariateReport(dependent = 'col3',event=0,categoryLimit=3)
        try:
            pd.read_csv("Reports/bivariateReport.csv")
        except Exception as e:
            print("bivariateReport.csv not found at location Reports")
            print(e)
