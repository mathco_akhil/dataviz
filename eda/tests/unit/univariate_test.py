import unittest
import pandas as pd
import sys
import os
import numpy as np

testdir = os.path.dirname(__file__)
srcdir = '../..'
sys.path.insert(0, os.path.abspath(os.path.join(testdir, srcdir)))
import univariate


class TestUnivariateMethods(unittest.TestCase):

    def setUp(self):
        self.df_basic = pd.DataFrame([[np.nan,2,0,1],[3,4,np.nan,0],[5,np.nan,0,1],[7,np.nan,1,np.nan],[9,10,1,0],[11,12,0,1],[13,14,1,0],[15,16,1,0],[18,19,0,0],[20,21,1,1],[21,22,0,0],[23,24,1,1]],columns=['col1','col2','col3','col4'])
        self.df_stats = pd.DataFrame([[1,2,'job',4],[1001,30,'unemployed',0], [1002,50,'unemployed',60]])
        self.df_ids = pd.DataFrame([['12mm015',3,5], ['12mm016',4,5], ['12mm017',6,7]], columns=['ID','col1','col2'])
        self.df_percentile = pd.DataFrame([[1,2],[3,4],[5,6],[7,8],[9,10],[11,12],[16,13],[3,7],[9,2]],columns=['col1','col2'])
        

    def test_generateUnivariateReport(self):
        self.df_basic.univariate.generateUnivariateReport(categoryLimit=4)
        try:
            pd.read_csv("Reports/categoricalVariableReport.csv")    
        except Exception as e:
            print("categoricalVariableReport.csv not found at location Reports")
            print(e)
        try:
            pd.read_csv("Reports/continuousVariableReport.csv")
        except Exception as e:
            print("continuousVariableReport.csv not found at location Reports")
        try:
            pd.read_csv("Reports/correlationMatrix.csv")
        except Exception as e:
            print("correlationMatrix.csv not found at location Reports")
            
    def generatePercentileDistribution(self):
        self.df_ids.univariate.generatePercentileDistribution()
        pd.util.testing.assert_frame_equal(self.df_percentile, pd.read_csv("dist.csv"))
        
    def test_generateDescriptStats(self):
        output = self.df_stats.univariate.generateDescriptStats(categoryLimit=3)
        pd.util.testing.assert_frame_equal(output,pd.DataFrame([[ 0,  668.00,  1001.0, 1.0 ,1.0,  1002.0,  1001.0,  222444.67 ,471.64, -.71, -1.5],
                  [ 1,  27.33,  30.0,    2.0 ,2.0,  50.0,    48.0,    387.56   ,19.69, -.20, -1.5],
                  [ 3,  21.33,  4.0,     0.0 ,0.0,  60.0,    60.0,    750.22   ,27.39,  .70, -1.5]],
                  columns = ['Column Name', 'Mean', 'Median', 'Mode', 'Min', 'Max', 'Range','Variance', 'Standard deviation', 'Skewness', 'Kurtosis']))
        
        
    
