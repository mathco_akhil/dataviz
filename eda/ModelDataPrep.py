from statsmodels.stats.outliers_influence import variance_inflation_factor
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn import preprocessing
import category_encoders as ce

@pd.api.extensions.register_dataframe_accessor("ModelDataPrep")
class ModelDataPrep:

    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def getVif(self, dep_var):

        """
        Function to generate Variance Inflation Factor.
        
        Parameters
        ----------
        dep_var : String
            The name of the dependant variable
        
        Returns
        -------
        vif : Pandas DataFrame
            The vif dataframe consists of the VIF value for each of the columns in the provided dataset.

        """

        dep = self._obj[dep_var]
        df_num = self._obj.drop([dep_var], axis=1)
        df_num = df_num._get_numeric_data()
        vif = pd.DataFrame()
        vif["VIF Factor"] = [variance_inflation_factor(
            df_num.values, i) for i in range(df_num.shape[1])]
        vif["Features"] = df_num.columns
        return(vif)

    def splitTrainAndTest(self, dep_var, test_data_size=0.3, random_seed=42):
        """
        Function to divide dataset into train and test
        
        Parameters
        ----------
        dep_var : String
            The name of the dependant variable

        test_data_size : Float {default = 0.3}
            The ratio of the testing set with respect to the total dataset

        random_seed : Integer {default = 42}
            A random seed is provided so that one gets the same train/test split.
        
        Returns
        -------
        trainset : Pandas DataFrame
            The training set

        testset : Pandas DataFrame
            The testing set

        """
        y = self._obj[dep_var]
        temp = self._obj.drop(dep_var, axis=1)
        x_train, x_test, y_train, y_test = train_test_split(
            temp, y, test_size=test_data_size, stratify=y, random_state=random_seed)
        trainset = pd.concat([x_train, y_train], axis=1)
        testset = pd.concat([x_test, y_test], axis=1)
        print("Event rate in train :")
        print(trainset[dep_var].value_counts(normalize=True) * 100)
        print("Event rate in test :")
        print(testset[dep_var].value_counts(normalize=True) * 100)
        return(trainset, testset)
    
    def encodeDependent(self,depVar,event):
        """
        Function to encode the dependent variable.
        Parameters:
        --------------
        depVar : dependent variable
        event : name of the event of the dependent variable
        
        Example : df.ModelDataPrep.encodeDependent(depVar,"Yes")
        "Yes" is the event for the dependent variable
        Returns
        -------
        The encoded dependent variable
        """
        self._obj[depVar] = np.where(self._obj[depVar] == event,1,0)
    
    def encoding(self, col, encoding_type = "ohe",drop_first = False):
        """
        Function to provide with multiple encoding techniques based on the selection of the user
        The 4 encoding types are:
        1. Label encoder("le") : Encode labels with value between 0 and n_classes-1
        2. One hot encoder("ohe") : One hot encoding for categorical features, produces one feature per category
        3. Binary encoder("be") : Stores categories as binary bitstrings
        4. BaseNEncoder("bne") : Base-N encoder encodes the categories into arrays of their base-N representation
        5. dummies : encoding_type dummies create dummy variables for the list of columns passed in the col
        Parameters
        ----------
        col : List of String
            The list of the columns to be provided for encoding

        encoding_type : String {default = "ohe"}
            Gives the encoding technique to be applied on the dataframe
        
        drop_first : if enocding_type is dummies, then drop_first can be set True to remove one of the dummies created.
        
        Returns
        -------
        self._obj : Pandas DataFrame
            The dataset provided

        """
        if(encoding_type == "le"):
            for i in col:
                self._obj[i+str("_encoded")] = preprocessing.LabelEncoder().fit_transform(self._obj.loc[:,i])

        elif(encoding_type == "ohe"):
            ohe = ce.OneHotEncoder(cols = col, drop_invariant = True)
            self._obj = ohe.fit_transform(self._obj.iloc[:,:])

        elif(encoding_type == "be"):
            be = ce.BinaryEncoder(cols = col, drop_invariant = True)
            self._obj = be.fit_transform(self._obj.iloc[:,:])

        elif(encoding_type == "bne"):
            bne = ce.BaseNEncoder(base=3, cols = col)
            self._obj = bne.fit_transform(self._obj.iloc[:,:])
            
        elif(encoding_type == "dummies"):
            dfNew = pd.DataFrame()
            dfNew = pd.get_dummies(self._obj[col])
            self._obj.drop(col,axis=1,inplace=True)
            self._obj = pd.concat([self._obj,dfNew],axis=1)
            
        return(self._obj)
