import pandas as pd
import numpy as np
from scipy import stats
from scipy.stats import kurtosis, skew
from eda import CONSTANTS
import plotly.graph_objs as go
from plotly.offline import plot, iplot, init_notebook_mode
init_notebook_mode(connected=True)
import os
import dataclean

@pd.api.extensions.register_dataframe_accessor("univariate")
class univariate(object):
        
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
    
    
###############################################################################
# FUNCTION: Return Dataframe containing Percentile Distribution
# INPUT: df - Data Frame(ADS), column name: list of name of the columns
#             from the dataset (continous column) percentileIncrement :
#             numeric value (example 0.1)
# OUTPUT: Dataframe containing percentile distribution
# REQUIRED: pandas, numpy
###############################################################################
    def generatePercentileDistribution(self,percentileIncrement=-1, missingPercentLimit=20, categoryLimit=20):
        
        """ This function finds the percentile distribution of the specified column name from the dataset, the default values 
        displayed are with an increment of 1 percent from 0 to 10, quartiles(0,25,50,100), one-third of the data(33,67), increment
        of 1 percent from 90 to 99 and and increment of 0.1 from 99 to 100. The function can also take in a percentileIncrement 
        from the user, this increment is displayed from 0-10 and 90-100
        Parameters
        ----------
        pandas_obj          : A pandas DataFrame
        
        column name         : list of name of the columns from the dataset (continous column)
        
        percentileIncrement : numeric value (example 0.1)
        
                      
         Usage
        --------
       
        df.univariate.generatePercentileDistributionPlot([continuousColumnName])
       
        """
        df = pd.DataFrame()
        dfPercentile = self._obj.copy()
        dfPercentile = dfPercentile.univariate.removeMissingColumns(missingPercentLimit=missingPercentLimit)
        listContinuous=[]
        listCategorical=[]
        for columnName in df:
            if len(df[columnName].unique())>=categoryLimit:
                listContinuous.append(columnName)
            else:
                listCategorical.append(columnName)
        columnNames = listContinuous
        for columnName in columnNames:
            print(columnName)
            percentiles={}
            currentPercentile=0
            dfPercentile.dropna(inplace=True)
            
            
            if percentileIncrement>0:
                while currentPercentile<100:
                    try:
                        percentiles[round(currentPercentile,2)]=round(np.percentile(dfPercentile[columnName],currentPercentile),2)
                    except Exception as e:
                        print("Could not round off the current percentile")
                        print(e)
                    if currentPercentile<=9.99 or currentPercentile>=89:
                        currentPercentile+=percentileIncrement
                    else:
                        currentPercentile+=10
            else:
                listPercentile = [10,20,25,30,33,40,50,60,67,70,75,80,90]
                while currentPercentile<100:
                    if currentPercentile<10 or (currentPercentile>=90 and currentPercentile<99):
                        try:
                            percentiles[round(currentPercentile,2)]=round(np.percentile(dfPercentile[columnName],currentPercentile),2)
                        except Exception as e:
                            print("Could not round off the percentiles for default values")
                            print(e)
                        currentPercentile+=1
                    elif currentPercentile>=10 and currentPercentile<=90:
                        for j in listPercentile:
                            try:
                                percentiles[round(currentPercentile,2)]=round(np.percentile(dfPercentile[columnName],currentPercentile),2)
                            except Exception as e:
                                print("Could not round off percentiles for values greater than 10 and less than 90 for default values")
                                print(e)
                            currentPercentile=j
                    else:
                        try:
                            percentiles[round(currentPercentile,2)]=round(np.percentile(dfPercentile[columnName],currentPercentile),2)
                        except Exception as e:
                            print("Could not round off percentiles for values less than 10 and greater than 90 for default values")
                            print(e)
                        currentPercentile+=0.1
            df = df.append(pd.DataFrame(percentiles, index=[columnName]))
        return df
    
    
###############################################################################
# FUNCTION: Return Dataframe containing Descriptive Statistics
# INPUT: df - Data Frame(ADS), column name: list of name of the columns
#             from the dataset (continous column) categoryLimit  : Maximum
#             number of unique for categorical column
# OUTPUT: Dataframe containing Descriptive Statistics
# REQUIRED: pandas, numpy
###############################################################################
    def generateDescriptStats(self, columnName=None,categoryLimit=20, missingPercentLimit=20):
        """
        This functions generates descriptive statistics for the given pandas dataframe. 
		 descriptive stats include - mean, median, mode, min, max, range, variance,
		 standard deviation, skewness and kurtosis.
		
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        categoryLimit  : Maximum number of unique for categorical column

        Usage
        --------
        df.univariate.generatedescriptStats()

        Returns
        -------
        Nothing. Your original DataFrame is modified inplace
   
        """
        df = self._obj.copy()
        df = df.dataclean.removeMissingColumns(missingPercentLimit=missingPercentLimit)
        df.dropna(inplace=True)
        listContinuous = []
        listCategorical = []
        # Separating continuous and categorical column names
        for columnName in df:
            if len(df[columnName].unique())>=categoryLimit:
                listContinuous.append(columnName)
            else:
                listCategorical.append(columnName)
        # Creating list of names of continuous and categorical columns
            univariateContinuous = pd.DataFrame(
                listContinuous, columns=['Column Name'])
        if columnName is not None:
            listContinuous = columnName
            # Creating univariate analysis csv file
            for i in univariateContinuous.index:
                try:
                    # Calculating Descriptive Statistics
                    statistics = {}
                    statistics["Mean"] = round(np.mean(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Median"] = round(np.median(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Mode"] = round(stats.mode(df[univariateContinuous['Column Name'].iloc[i]])[0][0], 2)
                    statistics["Min"] = round(min(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Max"] = round(max(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Range"] = round(max(df[univariateContinuous['Column Name'].iloc[i]]) - min(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Variance"] = round(np.var(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Standard deviation"] = round(np.std(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Skewness"] = round(skew(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    statistics["Kurtosis"] = round(kurtosis(df[univariateContinuous['Column Name'].iloc[i]]), 2)
                    descriptiveStatistics = statistics
                    # Appending all descriptive statistics to dataframe
                    for statistic, value in descriptiveStatistics.items():
                        univariateContinuous.loc[i, statistic] = value
                except:
                    continue
        return(univariateContinuous)

   


###############################################################################
# FUNCTION: Return Dataframe containing Univariate Reports as csv
# INPUT: df - Data Frame(ADS), categoryLimit  : Maximum
#             number of unique for categorical column
# OUTPUT: three csv files in Reports folder at current directory.
#        1. continuousVariableReport containing descriptive statistics and
#        percentile distribution for continuous columns
#        2. categoricalVariableReport containing count of each category
#        in categorical column
#        3. Correlation Matrix
# REQUIRED: pandas, numpy
###############################################################################
    def generateUnivariateReport(self, categoryLimit = 20, percentileIncrement=-1, missingPercentLimit=20):
        """
        This functions generates descriptive statistics and percentile
        distribution for continuous columns of the given pandas dataframe,
        count of each category for categorical column and correlation matrix
        as csv's inside report directory located at current working
        directory.
        descriptive stats include:
        mean, median, mode, min, max, range, variance,
		 standard deviation, skewness and kurtosis.
		
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        categoryLimit  : Maximum number of unique for categorical column
        missingPercent : Permissible limit for np.nan above which column will be dropped

        Usage
        --------
        df.univariate.report()

        Returns
        -------
        Create new folder 'Report' containing continuousVariableReport.csv,
        categoricalVariableReport.csv and correlationMatrix.csv
   
        """
        df = self._obj.copy()
        
        if not os.path.exists('Reports'):
            os.makedirs('Reports')
        listContinuous = []
        listCategorical = []
        if categoryLimit>len(df):
            categoryLimit=len(df)/2
        
        # Separating continuous and categorical column names
        for columnName in df:
            if len(df[columnName].unique())>=categoryLimit:
                listContinuous.append(columnName)
            else:
                listCategorical.append(columnName)
        
        missingPercent = df.dataclean.getMissingPercent()

        try:
            stats = df.univariate.generateDescriptStats(columnName=listContinuous, categoryLimit=categoryLimit, missingPercentLimit=missingPercentLimit)
        except Exception as e:
            print("Unable to calculate Descriptive Statistics")
            print(e)
        try:
            dist  = df.univariate.generatePercentileDistribution(percentileIncrement=percentileIncrement, missingPercentLimit=missingPercentLimit)
            for i in dist.index:
                dist.loc[i,'Missing Value'] = np.isnan(df[i]).sum()
        except Exception as e:
            print("Unable to calculate percentile distribution")
            print(e)
        stats = stats.merge(missingPercent,left_on='Column Name',right_on=missingPercent.index, how = 'outer')
        stats = stats.merge(dist,left_on='Column Name',right_on=dist.index, how = 'outer')
        stats.to_csv("Reports/continuousVariableReport.csv", index=False)
        listCategoriesCount=[]
        
        for column in listCategorical:
            # Creating a list of columns with count in each category
            try:
                listCategoriesCount= listCategoriesCount+([(column,m,n) for m,n in dict(df[column].value_counts()).items()])
                listCategoriesCount = listCategoriesCount+[(column,'Missing Count',df[column].isnull().sum())]
            except Exception as e:
                print("Error occured while calculating count of categorical variables")
                print(e)
            try:
                pd.DataFrame(listCategoriesCount,columns=['Column Name','Category','Count']).to_csv("Reports/categoricalVariableReport.csv")
            except Exception as e:
                print("Error occured while saving csv file")
                print(e)
        
        try:
            df.corr().to_csv("Reports/correlationMatrix.csv")
        except Exception as e:
            print("Error occured while creating correlation matrix")
            print(e)


    def generateHistogram(self,columnName,nbinsx = 10):
        """
		A histogram plot is opened up in a new tab of the default web browser.

		Parameters
		----------
		columnName : String
			         The name of a numerical column

		Returns
		-------
		Nothing

		The statement 'plot(fig, config = config)' plots the histogram plot in a new tab of the default web browser

		"""
        data = [go.Histogram(x=self._obj[columnName],autobinx=False,nbinsx=nbinsx,visible=True,name=str(columnName),marker=dict(color='rgb(0,31,95)'))]
        layout = go.Layout(
                title=("Histogram of "+columnName).title(),
                		 xaxis=dict(
                                 linewidth = 2,
                                 title=(columnName+' levels').lower(),
                                 zeroline=True,
                                 showline=True,
                                 titlefont=dict(
                                     family='Courier New, monospace',
                                    size=18,
                                     color='#7f7f7f'
                                 )
                            ),
                		yaxis=dict(
                                linewidth = 2,
                                title=('Population in '+columnName+' Levels').lower(),
                                zeroline=True,
                                showline=True,
                                titlefont=dict(
                                    family='Courier New, monospace',
                                    size=18,
                                    color='#7f7f7f'
                                )
                            )
                	)
        fig=go.Figure(data=data, layout=layout)
        return fig
    
        def generateBoxplot(self,columnName):
            """ a boxplot is generated for continuous variables from which outliers can be seen.
            Parameters
            ----------
            self       : pandas dataframe
            columnName : Name of the column for which boxplot is required
                
            Examples
            --------
            create_boxplot(df="csv_filename.csv",'lat')

            create_boxplot(df="path/csv_filename.csv",'lat')

            create_boxplot(df=pandas_dataframe,'lat')

            """
            
            
            
            # providing data to boxplot
            try:
                data = [go.Box(x=self._obj[columnName],
                            boxpoints='suspectedoutliers',
                            visible=True, name=columnName,
                            marker=dict(color='rgb(0,31,95)'))]
            except Exception as e:
                print("Boxplot could not be generated")
                print(e)
            try:
                layout = go.Layout(
                title="Boxplot of "+columnName,
                titlefont=dict(
                    family='opensans',
                    size=20,
                    color='black'
                    ),
                xaxis = dict(linewidth = 2),
                yaxis = dict(linewidth = 2),
                )
            except Exception as e:
                print("Layout for boxplot could not be generated")
                print(e)
            return go.Figure(data=data,layout=layout)
        
    def generatePercentileDistributionPlot(self,columnName,percentileIncrement=0.1):
    
    
        """ This function finds the percentile distribution of the specified column name from the dataset, the default values 
        displayed are with an increment of 1 percent from 0 to 10, quartiles(0,25,50,100), one-third of the data(33,67), increment
        of 1 percent from 90 to 99 and and increment of 0.1 from 99 to 100. The function can also take in a percentileIncrement 
        from the user, this increment is displayed from 0-10 and 90-100
        Parameters
        ----------
        pandas_obj          : A pandas DataFrame
        
        column name         : name of the column from the dataset (continous column)
        
        percentileIncrement : numeric value (example 0.1)
        
                      
         Usage
        --------
       
        df.univariate.generatePercentileDistributionPlot('continuousColumnName')
       
        """
        percentiles={}
        currentPercentile=0
        self._obj = self._obj.copy()
        self._obj.dropna(inplace=True)
        
        
        if percentileIncrement>0:
            while currentPercentile<=100:
                percentiles[round(currentPercentile,2)]=round(np.percentile(self._obj[columnName],currentPercentile),2)
                if currentPercentile<=9.99 or currentPercentile>=89:
                    currentPercentile+=percentileIncrement
                else:
                    currentPercentile+=10
        else:
            listPercentile = [10,20,25,30,33,40,50,60,67,70,75,80,90]
            while currentPercentile<=100:
                if currentPercentile<10 or (currentPercentile>=90 and currentPercentile<99):
                    percentiles[round(currentPercentile,2)]=round(np.percentile(self._obj[columnName],currentPercentile),2)
                    currentPercentile+=1
                elif currentPercentile>=10 and currentPercentile<=90:
                    for i in listPercentile:
                        percentiles[round(currentPercentile,2)]=round(np.percentile(self._obj[columnName],currentPercentile),2)
                        currentPercentile=i
                else:
                    percentiles[round(currentPercentile,2)]=round(np.percentile(self._obj[columnName],currentPercentile),2)
                    currentPercentile+=0.1
        
            
            percentiles[0]=0
            percentiles[1]=0
            percentiles[100]=0
                    
        # providing data to line plot
        
        data= [go.Scatter(x = list('`'+str(i) for i in percentiles.keys()),
                              y = list(percentiles.values()),
                              marker=dict(color='rgb(0,31,95)'),
                              mode = 'lines',
                              name = 'lines')]
        
        layout = go.Layout(
            title = ("Percentile Distribution Curve"+' - '+columnName).title(),
            xaxis=dict(
                linewidth = 2,
                title=("Percentiles").lower(),
                titlefont=dict(
                    family='opensans',
                       size=16,
                        color='black'
                    )
                ),
                yaxis=dict(
                    linewidth = 2,
                    title=('Distribution').lower(),
                    titlefont=dict(
                        family='opensans',
                        size=16,
                       color='black'
                        )
                    )
            )

        fig = go.Figure(data=data,layout=layout)
        df = pd.DataFrame(list(percentiles.items()),columns=['Percentiles','Values'])
        print(df)
        return fig

    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)
            else:
                plot(figure, config=config)