import pandas as pd
import plotly.graph_objs as go
from plotly.offline import plot, iplot, init_notebook_mode
init_notebook_mode(connected=True)
import os
from collections import namedtuple

CON={'INTS': ['int16', 'int32', 'int64', 'int'],
    'FLOATS': ['float16', 'float32', 'float64', 'float'],
    'COMPLEX': ['complex64', 'complex128'],
    'NUMS': ['int', 'int16', 'int32', 'int64', 'float16', 'float32', 'float64','float'],
    'STRINGS': [],
    'DATES': [],
    'NAN': ['None', '', ' ', 'nan', 'Nan', 'NaN']}
CON = namedtuple("CONSTANTS", CON.keys())(*CON.values())

@pd.api.extensions.register_dataframe_accessor("bivariate")
class BivariateAnalysis(object):
        
    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
    def generateStackedBarPlot(self,colX,colY):
    
        """A stacked-bar plotly graph with categorical variable on both
        X and Y axis.
    
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
    
        colX      : Column Name of categorical variable in dataframe.
    
        colY      : Column Name of categorical variable in dataframe.
    
        Usage
        --------
    
        df.bivariate.createStackedBarGraph("categoricalVariable1","categoricalVariable2")
        
        """
       
        if colX!=colY:
    
            layout = go.Layout(
                title = (colY+' - '+colX).title(),
                xaxis=dict(
                    title=(colX).lower(),
                    showline=True
                    ),
                    yaxis=dict(
                        title=(colY).lower(),
                        showline=True
                    ),
                    barmode='stack'
                    )
            
            mybar=self._obj.groupby([colX,colY])[colY].count().reset_index(name='count')
            trace=[]
            
            for i in self._obj[colY].unique():
                trace.append(go.Bar(
                    x=mybar[colX][mybar[colY]==i],
                    y=mybar['count'][mybar[colY]==i],
                    text=[round(i,2) for i in mybar['count'][mybar[colY]==i]], textposition='auto',textfont=dict(family='opensans',size=10),
                    name=str(i).lower()))
            fig = go.Figure(data=trace,layout=layout)
            return fig
        else:
            print('Could not generate Stackedbar plot. Enter different categorical columns')
            return {}
    
    def generateScatterPlot(self, col1, col2):
        """
        A scatter plot is opened up in a new tab of the default web browser.
        
        Parameters
        ----------
        col1 : String
            The name of a first numerical column

        col2 : String
            Then name of the second numerical column
        
        Returns
        -------
        Nothing

        The statement 'plot(fig, config = config)' plots the scatter plot in a new tab of the default web browser

        """

        try:
            trace = go.Scatter(
                x = self._obj[col1],
                y = self._obj[col2],
                mode = 'markers',
                marker = dict(
                    color = '#EB984E')
                )
        except Exception as e:
            print("Unable to create trace for the scatter plot")
            print(e)

        data = [trace]


        try:
            layout = go.Layout(
                title=('Scatter plot between ' + str(col1) + " and " + str(col2)).title(),
                xaxis=dict(
                        title= col1,
                        titlefont=dict(
                                family='Courier New, monospace',
                                size=18,
                                color='#7f7f7f'
                                )
                        ),
                yaxis=dict(
                        title= col2,
                        titlefont=dict(
                                family='Courier New, monospace',
                                size=18,
                                color='#7f7f7f'
                                ),
                        zeroline=True,
                        showline=True,
                        )
                )
        except Exception as e:
            print("Error in creating layout for scatter plot")
            print(e)
        try:
            fig = go.Figure(data=data, layout=layout)
        except Exception as e:
            print("Exception in creating the Figure object")
            print(e)
        return fig
            
    def generatePlotDependent(self,colX,colY,event,dependent=None,nbins=10, categoryLimit=20):
        
        """
        A bar-line plotly graph with independent variable on X-Axis
        and percentage of event of dependent variable in each category/decile.
        
        Parameters
        ----------
        df        : pandas dataframe containing columns colX and colY
        
        colX      : Name of column in dataframe df passed as string.
        
        colY      : Column name of dependent variable passed as string.
        
        dependent : Column name of categorical variable
                    passed as string. Can be used only
                    when event is specified.
                    
        event     : Category in the dependent column passed
                    as string.
                    
        Examples
        --------
        
        createGraphDependent(df,"exColumnName1","exColumnName2","exColumnName2","categoryInexColumn2")
        
        or
        
        createGraphDependent(df,"exColumnName1","exColumnName2","exColumnName1","categoryInexColumn1")
        
        """
        data=[]
        if dependent==None:
            dependent=colY
        # Layout
        try:
            layout = go.Layout(
                title=("Percentage of "+colY+" in "+colX).title(),
                xaxis = dict(zeroline=False,
                    showline=True
                        ),
                yaxis=dict(
                    title=("Population in "+colX).lower(),
                    zeroline=False,
                    showline=True
                ),
                yaxis2=dict(
                    title= ("% of event").lower(),
                    titlefont=dict(
                        color='rgb(0, 0, 0)'
                    ),
                    tickfont=dict(
                        color='rgb(0, 0, 0)'
                    ),
                    overlaying='y',
                    side='right',
                    zeroline=False,
                    showline=True
                )
            )
        except Exception as e:
            print("Error occured while creating layout if dependent and event is specified")
            print(e)
        if event!=None and (len(self._obj[colX].unique()) > categoryLimit or len(self._obj[colY].unique()) > categoryLimit) and event in self._obj[dependent].unique():
            try:
                if colX==dependent:
                    colX=colY
                    colY=dependent
                try:
                    bar = self._obj.groupby(pd.qcut(self._obj[colX],nbins,duplicates='drop',precision=2))[colX].count().reset_index(name='count')
                except Exception as e:
                    print("Error occcured while grouping colX ")
                    print(e)
                mxbar = bar[colX].astype(str)
                mybar = bar['count']
                try:
                    line=self._obj.groupby(pd.qcut(self._obj[colX],nbins,duplicates='drop',precision=0))[colY].value_counts().reset_index(name='count')
                except Exception as e:
                    print("Error occured while grouping value counts of colY by colX")
                    print(e)
                try:
                    myline = line.groupby([colX])['count'].sum().reset_index()
                except Exception as e:
                    print("Error occured while calculating sum of value counts of colY")
                    print(e)
                try:
                    line=pd.merge(line,myline,how='left',on=colX)
                except Exception as e:
                    print("Error occured while merging lists")
                    print(e)
                try:
                    line['percent']=line['count_x']/line['count_y']*100
                except Exception as e:
                    print("Error occured while calculating percentage of value counts of colY in each decile of colX")
                    print(e)
                line['percent'][line[colY]!=event]=0
                try:
                    myline = line.groupby([colX])['percent'].sum().reset_index()
                except Exception as e:
                    print("Error occued while grouping by colX with the event rate")
                    print(e)
            # Creating bar and line graphs
                try:
                    data = [go.Bar(x=mxbar,y=mybar, name=(colX).lower(), marker=dict(color='rgb(62,64,62)')),go.Scatter(x=mxbar,y =myline['percent'], marker=dict(color='rgb(237,112,56)'),name =(colY).lower(), text=[round(i,2) for i in myline['percent']], textposition='bottom center',textfont=dict(family='opensans',color='rgb(245,168,65)'),mode='lines+text',yaxis='y2')]
                except Exception as e:
                    print("Error oocured while creating bar and line graph")
                    print(e)
            except Exception as e:
                print("Unable to show barline graph with the selected variable")
                print(e)
            return go.Figure(data=data, layout=layout)
        elif event!=None and event in self._obj[dependent].unique():
            try:
                if colX==dependent:
                    colX=colY
                    colY=dependent
                try:
                    bar = pd.crosstab(self._obj[colX],self._obj[colY]).reset_index()
                except Exception as e:
                    print("Error while creating crosstab for colX and colY selected")
                    print(e)
                try:
                    bar['sum'] = bar.sum(axis=1)
                except Exception as e:
                    print("Error occured while calculating sum of the categories")
                    print(e)
                try:
                    bar['percent']=(bar[event]/bar['sum'])*100
                except Exception as e:
                    print("Error while calculating percentage of event")
                    print(e)
                try:
                    data = [go.Bar(x=bar[colX],y=bar['sum'], name=(colX).lower(), marker=dict(color='rgb(62,64,62)')),go.Scatter(x=bar[colX],y =bar['percent'], name=(colY).lower(), marker=dict(color='rgb(237,112,56)'), text=[round(i,2) for i in bar['percent']], textposition='bottom center',textfont=dict(family='opensans',color='rgb(245,168,65)'),mode='lines+text',yaxis='y2')]
                except Exception as e:
                    print("Error while creating bar line graph with a categorical column chosen")
                    print(e)
                config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
                return plot(go.Figure(data=data, layout=layout), config=config)
            except Exception as e:
                print("Unable to show barline graph with the selected variable")
                print(e)
            except Exception as e:
                print("Error while creating layout for bar line graph")
                print(e)
            return go.Figure(data=data, layout=layout)  
        else:
            print(" Provided event is not in dependent variable")
            return go.Figure(data=data, layout=layout)
    
    def generateBubblePlot(self,colX,colY,bubbleSize=None,bubbleColor=None,
                          bubbleSizeScale=1):
    
        """A bubble graph from plotly with continuous or categorical variable
        column for X-Axis and Y-Axis. A Categorical variables for bubble color
        and bubble group. A continuous column for bubble size.
        
        Parameters
        ----------
        df          : pandas dataframe containing columns colX and colY
        
        colX        : Column Name of another continuous variable in dataframe.
        
        colY        : Column Name of categorical variable in dataframe.
                              
        bubbleColor : Column Name of categorical variable; used to set color of
                      the bubble before plotting.
                      
        bubbleSize  : Column Name of a continuous variable; used to set the
                      of bubble for each variable
                      
        bubbleSizeScale   : To scale the bubble size in the graph,
                            this number will be considered as reference.
                      
        Examples
        --------
        
        createGraphBubble(dfname,"continuousVariable1",
        "continuousVariable2", "continuousVariable3", "categoricalVariable1",
        "categoricalVariable2", bubbleSizeScale=1)
        
        """
        
        
        df = self._obj.copy()
        df.dropna(inplace=True)
        data=[]
        title = colX+' - '+colY
        
        if bubbleSize is not None:
            title = title+' - '+bubbleSize
        if bubbleColor is not None:
            title = title+" - "+bubbleColor
            
            
        layout = go.Layout(
            title = title.title(),
            xaxis=dict(
                title=(colX).lower(),
                showline=True,
                zeroline = False
            ),
            yaxis=dict(
                title=(colY).lower(),
                showline=True,
                zeroline = False
            )
        )
        
        # for x or y are categorical columns
        if df[colX].dtype in CON.NUMS and df[colY].dtype in CON.NUMS:
            if bubbleSize is not None and df[bubbleSize].dtype in CON.NUMS:
                if min(df[bubbleSize])<= 0:
                    k=0-min(df[bubbleSize])
                    df[bubbleSize]+=k
            data =[]
    
            if bubbleSize is not None:
                try:
                    if bubbleColor is not None:
                        df=df.groupby([bubbleColor])[colX,colY,bubbleSize].mean().reset_index()
                        for i in df[bubbleColor].unique():
                            if bubbleSize is not None:
                                data.append(go.Scatter(
                                    x=df[colX][df[bubbleColor] == i],
                                    y=df[colY][df[bubbleColor] == i],
                                    mode='markers',
                                    name=str(i),
                                    showlegend = True,
                                    hoverlabel = dict(namelength = len(str(i))),
                                    marker=dict(
                                        symbol='circle',
                                        sizemode='area',
                                        sizeref=bubbleSizeScale,
                                        sizemin=4,
                                        size=df[bubbleSize][df[bubbleColor] == i],
                                        line=dict(
                                            width=0
                                        ),
                                    )
                                ))
                        return go.Figure(data,layout)
                    if bubbleColor is None:
                        try:
                            data = [go.Scatter(
                                x=df[colX],
                                y=df[colY],
                                mode='markers',
                                marker=dict(
                                            symbol='circle',
                                            sizemode='area',
                                            size=(df[bubbleSize]),
                                            sizeref = bubbleSizeScale,
                                            line=dict(
                                            width=0
                                ))
                            )]
                        except Exception as e:
                            print("Could not append bubble plot")
                            print(e)
                except Exception as e:
                    print(" Cannot plot bubble plot as size column contains nan values")
                    print(e)
            else:
                # for Scatter Plot
                try:
                    if bubbleColor is not None:
                        for i in df[bubbleColor].unique():
                            try:
                                data.append(go.Scatter(
                                    x=df[colX][df[bubbleColor] == i],
                                    y=df[colY][df[bubbleColor] == i],
                                    mode='markers',
                                    name=str(i).lower(),
                                    marker=dict(
                                        symbol='circle',
                                    )
                                ))
                            except Exception as e:
                                print("Could not append scatter plot for bubble color != none ")
                                print(e)
                    else:
                        try:
                            data = [go.Scatter(
                                x=df[colX],
                                y=df[colY],
                                mode='markers',
                                marker=dict(color='rgb(0,31,95)')
                            )]
                        except Exception as e:
                            print(e)
                except Exception as e:
                    print("Scatter plot cannot be plotted with selected variables")
                    print(e)
            
        return go.Figure(data = data, layout=layout)
    
    
    def generateBarLinePlot(self, colX, colY, nbins = 10):

        """
        A barline plot is opened up in a new tab of the default web browser.
        
        Parameters
        ----------
        colX : String
            The name of a first column

        colY : String
            The name of the second column

        nbins : Integer {default = 10}
            The number of bins the X-axis needs to be divided into.
        
        Returns
        -------
        Nothing

        The statement 'plot(fig, config = config)' plots the barline plot in a new tab of the default web browser

        """

        try:
            listObjects=[]
            for i in self._obj:
                if (self._obj[i].dtype) in ['int16', 'int32', 'int64', 'float16', 'float32', 'float64','float','int']:
                    listObjects.append(i)
            cont_vars = listObjects
        except Exception as e:
            print("Exception in generating the list of continuous variables.")
            print(e)
        data=[]
        if(colX in cont_vars and colY in cont_vars):
            try:
                # creating deciles from column x and getting count in each decile
                bar = self._obj.groupby(pd.qcut(self._obj[colX],nbins,duplicates='drop',precision=0))[colX].count().reset_index(name='count')
            except Exception as e:
                print("Error occured while grouping colX")
                print(e)
            mxbar = bar[colX].astype(str)
            mybar = bar['count']

            try:
                line = self._obj.groupby(pd.qcut(self._obj[colX],nbins,duplicates='drop',precision=0))[colY].sum().reset_index(name = 'sum')
            except Exception as e:
                print("Error occured while grouping colX with colY")
                print(e)
            try:
                # calculating percentage of column y in decile of column x
                line['percent'] = (line['sum']/line['sum'].sum())*100
            except Exception as e:
                print("Error occured while calculating percentage of colY in colX")
                print(e)

            try:
                # Creating bar and line graphs
                data = [go.Bar(x=mxbar,y=mybar, name=colX, marker=dict(color='rgb(62,64,62)')),go.Scatter(x=mxbar,y =line['percent'], name=("percent of "+colY).lower(), marker=dict(color='rgb(237,112,56)'), text=[round(i,2) for i in line['percent']],     textposition='bottom center',textfont=dict(family='opensans',color='rgb(245,168,65)'),mode='lines+text',yaxis='y2')]
            except Exception as e:
                print("Error occured while plotting bar-line graph if X and Y choosen is continuous")
                print(e)
            try:
                layout = go.Layout(
                    title=("Percentage of "+colY+" in "+colX).title(),
                    xaxis = dict(zeroline=False,
                        title = (str(colX) + " levels").lower(),
                            showline=True
                            ),
                    yaxis=dict(
                        title=("Population in "+colX).lower(),
                        zeroline=False,
                        showline=True
                        ),
                    yaxis2=dict(
                        title= ("% of "+colY).lower(),
                        titlefont=dict(
                            color='rgb(0,0,0)'
                            ),
                        tickfont=dict(
                            color='rgb(0,0,0)'
                            ),
                        overlaying='y',
                        side='right',
                        zeroline=False,
                        showline=True
                    )
                )
            except Exception as e:
                print("Error occured while creating layout if X and Y columns choosen are continuous")
                print(e)
            config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
            try:
                fig=go.Figure(data=data, layout=layout)
            except Exception as e:
                print("Unable to create the Figure object")
                print(e)
            return fig

        elif(colX in cont_vars and colY not in cont_vars):

            temp=colY
            colY=colX
            colX=temp
            
        try:
            bar = self._obj.groupby([colX])[colX].count().reset_index(name='count')
        except Exception as e:
            print("Error occured while grouping colX")
            print(e)
        mxbar=bar[colX]
        mybar=bar['count']
        data=[]
        try:
            line = self._obj.groupby([colX])[colY].sum().reset_index(name='sum')
        except Exception as e:
            print("Error occured while grouping colX with colY")
            print(e)
        try:
            line['percent'] = line['sum']/line['sum'].sum()*100
        except Exception as e:
            print("Error occured while calculating percentage of colY in colX")
            print(e)
        try:
            data = [go.Bar(x=mxbar,y=mybar, name=(colX).lower(), marker=dict(color='rgb(62,64,62)')), go.Scatter(x=mxbar,y =line['percent'],name =("Percent of "+colY).lower(), marker=dict(color='rgb(237,112,56)'), text=[round(i,2) for i in line['percent']], textposition='bottom center',textfont=dict(family='opensans',color='rgb(245,168,65)'),mode='lines+text',yaxis='y2')]
        except Exception as e:
            print("Error occured while plotting bar-line graph if X is continuous")
            print(e)
        try:
            layout = go.Layout(
                title=("Percentage of "+colY+" in "+colX).title(),
                xaxis = dict(zeroline=False,
                    title = (str(colX) + " levels").lower(),
                    showline=True
                    ),
                yaxis=dict(
                    title=("Population in "+colX).lower(),
                    zeroline = False,
                    showline = True
                    ),
                yaxis2=dict(
                    title= ("% of "+colY).lower(),
                    titlefont=dict(
                        color='rgb(0,0,0)'
                        ),
                    tickfont=dict(
                        color='rgb(0,0,0)'
                        ),
                    overlaying='y',
                    side='right',
                    zeroline=False,
                    showline=True
                    )
                )
        except Exception as e:
            print("Error occured while creating layout if X column is continuous")
            print(e)
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        fig=go.Figure(data=data, layout=layout)
        return fig

    def plot(self, figure, filename = False, isNotebook = False):
        """
        This function will plot, plotly figures
        
        Paramenter:
        -----------
        
        self      : A pandas DataFrame
        figure    : Plotly figure object (figure_objs)
        filename  : str object need to be provided to save graphs in current directory
        isNotebook: boolean, optional. plot graph inside notebook if true
        """
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        if isNotebook:
            if filename!=False:
                iplot(figure, filename=filename, config=config)
            else:
                iplot(figure, config=config)
        else:
            if filename!=False:
                plot(figure, filename=filename, config=config)
            else:
                plot(figure, config=config)
    
    def generateBivariateReport(self,dependent,event, columnName=None, categoryLimit=20):
        
        """
        Create a csv file in current directory. The report will contain event rate in each bin
        (for continuous variable) or category (for categorical variable) along with bin range
        and category count respectively.
        
        Parameters:
        -----------
        
        self          : pandas Dataframe
        dependent     : dependent variable column name
        event         : value to be consider as event in dependent
        columnName    : list of column Name for which bivariate report is required, default
                        all columns
        categoryLimit : Maximum number of unique values to consider as categorical if column
                        is continuous
        """
        
        df = self._obj
        colY = dependent
        report = pd.DataFrame(columns = ['Column Name','Deciles/Category','Count','Percent'])
        if columnName is None:
            columnName = list(df.columns)
            columnName = filter(lambda a: a != dependent, columnName)
        if len(df)<categoryLimit:
            categoryLimit=len(df)/2
        for i in columnName:
            if len(df[i].unique())>categoryLimit:

                # for Continuous
                colX=i
                bar = df.groupby(pd.qcut(df[colX],10,duplicates='drop',precision=0))[colX].count().reset_index(name='count')
                line=df.groupby(pd.qcut(df[colX],10,duplicates='drop',precision=0))[colY].value_counts().reset_index(name='count')
                myline = line.groupby([colX])['count'].sum().reset_index()
                line=pd.merge(line,myline,how='left',on=colX)
                line['percent']=line['count_x']/line['count_y']*100
                line['percent'][line[colY]!=event]=0
                myline = line.groupby([colX])['percent'].sum().reset_index()
                bar['percent'] = myline['percent']
                bar['Column Name'] = colX
                cols = bar.columns.tolist()
                cols = cols[-1:] + cols[:-1]
                bar = bar[cols]
                bar.columns = ['Column Name','Deciles/Category','Count','Percent']
                bar['Deciles/Category'] = bar['Deciles/Category'].astype(str)
                report = report.append(bar,sort=True, ignore_index=True)
            else:
                colY=dependent
                colX=i
                bar = pd.crosstab(df[colX],df[colY])
                bar['count'] = bar.sum(axis=1)
                bar['percent']=(bar[event]/bar['count'])*100
                temp = bar[['count','percent']].copy()
                temp[colX] = temp.index
                temp.columns = ['count','percent',colX]
                cols = temp.columns.tolist()
                cols = cols[-1:] + cols[:-1]
                temp = temp[cols]
                temp['Column Name'] = colX
                cols = temp.columns.tolist()
                cols = cols[-1:] + cols[:-1]
                temp = temp[cols]
                temp.columns = ['Column Name','Deciles/Category','Count','Percent']
                temp['Deciles/Category']=temp['Deciles/Category'].astype(str)
                report = report.append(temp, sort=True, ignore_index=True)
        if not os.path.exists('Reports'):
            os.makedirs('Reports')
        return report.to_csv("Reports/bivariateReport.csv", index=False)