import pandas as pd
import numpy as np
from scipy import stats
from sklearn.preprocessing import MinMaxScaler,StandardScaler,RobustScaler,MaxAbsScaler 
from collections import namedtuple

CON={'INTS': ['int16', 'int32', 'int64', 'int'],
    'FLOATS': ['float16', 'float32', 'float64', 'float'],
    'COMPLEX': ['complex64', 'complex128'],
    'NUMS': ['int', 'int16', 'int32', 'int64', 'float16', 'float32', 'float64','float'],
    'STRINGS': [],
    'DATES': [],
    'NAN': ['None', '', ' ', 'nan', 'Nan', 'NaN']}
CON = namedtuple("CONSTANTS", CON.keys())(*CON.values())

@pd.api.extensions.register_dataframe_accessor("dataclean")
class DatacleanAccessor(object):
    """
    Contains functions that help with taking a raw dataset and making an analytical dataset - Ready to feed into a model
    1. findContinuousColumns() - Coerces predominantly numeric columns into numeric. Strings and other formats are coerced into nan
    2. removeDates() - Remove date columns. Input date strings should pe parseable by `pandas.to_datetime(infer_datetime_format=True)`
    Parameters
    ----------
    pandas_obj : A pandas DataFrame
    """


    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def findContinuousColumns(self, nrows=100, threshold=2,printcol=True):
        
        """
        This function identifies numeric continuous columns and numeric columns which might have a few garbage values present in them.
        The function tries to convert the first `nrows`(default = 100) values into float type.  
        If more than 95% (this value cannot be changed) of nrows are convertable, then the column is converted to float and hence will be identified as numeric column
        The threshold `5' represents the number of unique values in the column, this value can be modified, enter integers in threshold.
        The miscreant values are converted into nan

        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        nrows : The number of rows to check if the given column is numeric.Default value is 100.
        threshold : The number of unique values in a particular column
                    
        Usage
        --------
        df.dataclean.findContinuousColumns()

        Returns
        -------
        List of continuous columns
   
        """
        listContinuous=[]
        listGarbage=[]
        listObjects=[]
        listFinal=[]
        for i in self._obj:
            if (len(self._obj[i].unique()) > threshold):
                listContinuous.append(i)
        if len(listContinuous) > 0:
            if printcol:
                print("The continuous columns identified from the dataset are",listContinuous)
        for i in self._obj:
            if ((self._obj[i].dtype) not in CON.NUMS) & (len(self._obj[i].unique()) > threshold):
                listObjects.append(i)
        if len(listObjects) > 0:
            for j in listObjects:
                counter=0
                if len(self._obj) < nrows:
                    nrows = len(self._obj)
                for i in range(nrows):
                    value=self._obj[j][i]
                    try:
                        float(value)
                        counter+=1
                    except:
                        continue
                if counter >= int(0.95*nrows):
                    self._obj[j]=pd.to_numeric(self._obj[j],errors='coerce')
                    listGarbage.append(j)
        if len(listGarbage) > 0:
            if printcol:
                print("Columns which could be continuous are ",listGarbage)
        listFinal=listContinuous + listGarbage
        return listFinal    
        

    def findDuplicateColumns(self):
        """
        This function identifies Duplicate columns 
 
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
       
        Usage
        --------
        df.dataclean.findDuplicateColumns()
 
        Returns
        -------
        A dataframe which contains the column names and boolean value(False:if the column appeared for the first time,True:if the column is duplicated)
   
        """
        s=self._obj.T.duplicated()
        df = s.rename(None).to_frame()
        df = df.reset_index()
        df.columns = [str(i) for i in range(0,len(df.columns))]
        return df


    
    def removeDuplicates(self):
        """ This function removes all the duplicate columns present in the dataset
        
        
        Parameters
        ----------
        self : pandas dataframe
        
        Examples
        --------

        removeDuplicates(df="csv_filename.csv)
        
        
        Returns
        -------
        New dataframe without the duplicates values
        
        
        
        """
        x = self._obj.T.drop_duplicates().T    
        return(x)
                 
    
    def findConstantColumns(self):
        """ This function finds columns with constant values in the dataframe, the threshold can be adjusted to check the percentage
        of constant values in the dataframe
        
        
        Parameters
        ----------
        self : pandas dataframe
        
        Examples
        --------

        findConstants(df="csv_filename.csv)
        
        Returns
        -------
        Returns list with constant columns
        

        """
        listConstant=[]
        for i in self._obj:
            if (self._obj[i].nunique())<2:
                listConstant.append(i)

        return(listConstant)


    def removeConstants(self):
        """ This function removes columns with constant values in the dataframe, the threshold can be adjusted to check the percentage
        of constant values in the dataframe
        Returns a new dataframe without the constant values
        
        Parameters
        ----------
        self : pandas dataframe
        
        Examples
        --------

        removeConstants(df="csv_filename.csv)
        
        
        Returns
        -------
        Returns list with constant columns
        

        """
        listofCON = self.findConstantColumns()
        self._obj.drop(listofCON, axis = 1, inplace = True)
        return(self._obj)
        
        #y= t.dataclean.findContinuousColumns()
        #self._obj.drop(i, axis=1,inplace=True)
                    
        #return self._obj
    
    def findDatetime(self):
            """ The date columns are identified from the dataframe, and parse these 
            columns to pandas date time format
            
            
            Parameters
            ----------
            pandas_obj : A pandas DataFrame
                        
            Usage
            --------
            df.dataclean.findDatetimes()
    
        
            Returns
            -------
            List of names of Date and Time columns
       
        
            """
            self._obj = self._obj.copy()
            listObjects = []
            for i in self._obj:
                try:
                    if self._obj[i].dtype == 'object' or self._obj[i].dtype == 'str':
                        self._obj[i]=pd.to_datetime(self._obj[i],errors='raise',infer_datetime_format=True,exact=False)
                        listObjects.append(i)
                except:
                    continue
                    
            
            return listObjects
    
    def removeDatetime(self,inplace=False, dayfirst=False, yearfirst=False, utc=None, box=True, unit=None, origin='unix', cache=False, infer_datetime_format=False, exact=False):
        """ The date columns are identified from the dataframe, and parse these 
        columns to pandas date time format
        
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
                    
        Usage
        --------
        df.dataclean.removeDatetime()

    
        Returns
        -------
        Modified dataframe removing Date and Time columns
   
    
        """
        if not inplace:
            self._obj = self._obj.copy()
        else:
            self._obj = self._obj
            
        listObjects = []
        for i in self._obj:
            if self._obj[i].dtype == 'object':
                try:
                    self._obj[i]=pd.to_datetime(self._obj[i],errors='raise',dayfirst=dayfirst, yearfirst=dayfirst, utc=utc, box=box, exact=exact, infer_datetime_format=infer_datetime_format, origin=origin, cache=cache)
                    listObjects.append(i)
                except:
                    continue   
        for i in listObjects:
            if "datetime" in str(self._obj[i].dtype):
                self._obj.drop(i,axis=1,inplace=True)
        return self._obj
    
    
    def findPrimaryKey(self,threshold=0.50):
        """ This function removes all ID columns from the dataset, the factor specified for the number of unique values is 300, if 
        the number of unique values exceeds threshold value provided by user, it is identified as an ID column
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        threshold : The percentage of rows checked that have to be less than the unique values of that column for the column to be treated as ID type
        
        Usage
        --------
        df.dataclean.findPrimarykey()
                  
        Returns
        -------
        List of names of ID columns
   
        """
        listObjects = [] 
        for i in self._obj:
            
            if (len(self._obj[i].unique())) > (self._obj.shape[0] * threshold) and (self._obj[i].dtype == 'object' or self._obj[i].dtype == 'str') :
                listObjects.append(i)
                continue
            elif(self._obj[i].dtype in CON.INTS):
                if (len(self._obj[i].unique()) == self._obj.shape[0]) or len(self._obj[i].unique()) > (self._obj.shape[0] * threshold):
                    listObjects.append(i)
        return listObjects
    
    
    
    def findCategoricalColumns(self,threshold=5,printcol=True):
        """
        This function identifies categorical columns 

        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        nrows : The number of rows to check and calculate thresold percentage. Default value is 100 rows
        threshold : The percentage of rows checked that have to be numeric for the column to be coerced to a numeric type
                    
        Usage
        --------
        df.dataclean.findCategoricalColumns()

        Returns
        -------
        List of categorical columns
   
        """
        listCategorical=[]
        listNumericCategorical=[]
        listFinal=[]
        
        
        for j in self._obj:
            if (self._obj[j].dtype == 'str' or self._obj[j].dtype == 'object'):
                listCategorical.append(j)
        if len(listCategorical) > 0:
            if printcol:
                print("The categorical columns identified from the dataset are", listCategorical)

        for j in self._obj:
            if (len(self._obj[j].unique()) < threshold):
               listNumericCategorical.append(j)
        if len(listNumericCategorical) > 0:
            if printcol:
                print("Numeric columns which could be categorical are",listNumericCategorical)       
               
        
        listFinal=listCategorical + listNumericCategorical
              
        return listFinal

                        
    def imputeData(self, colNames='all',imputeContinuous=True, imputeContinuousWith='mean', imputeCategorical=True,replaceCategoricalWith=None, threshold = 5):
        
        '''
        The function handles missing data by imputing appropriate values based on user selection
        
        Parameters
        ----------
        pandas_obj 	  : pandas dataframe
        colNames   	  : Column names
            	           if 'all', imputes data for all continuous columns
            	           if 'all' and imputeCategorical = True, data is imputed for all columns where the categorical columns are imputed with the mode of the column
        imputeWith 	  : Method of imputation for continuous columns (mean, median, mode, zero)
        imputeCategorical : Imputes missing values in categorical data with mode or anything given by user, if True
        threshold 	  : Threshold that decides whether the column is identified as a continuous or a categorical column
        replaceCategoricalWith : it replaces the Nan present in the categorical columns with the word given in the parameter replaceCategoricalWith
        imputeContinuous : Imputes missing values in continous data with mean,median,mode or zero, if True
        Usage
        -----
        df.dataclean.imputeData()
        
        Returns
        -------
        self._obj     : Pandas dataframe
        
        A dataframe where the missing values have been imputed  
        '''

        missingValues = ['Nan', 'na', 'NA', 'none', 'None', '  ', '', '^ ', np.nan]
        
        
        if colNames == 'all':
            colNames = self._obj.columns
        
        if imputeContinuous:
            if imputeContinuousWith == 'mean':
                for i in colNames:
                    if (len(self._obj[i].unique())>threshold):
                        self._obj[i].replace(missingValues, round(np.mean(self._obj[i]), 2), inplace=True)
    
            elif imputeContinuousWith == 'median':
                for i in colNames:
                    if (len(self._obj[i].unique())>threshold):
                        self._obj[i].replace(missingValues, round(np.median(self._obj[i].dropna()), 2), inplace=True)
    
            elif imputeContinuousWith == 'mode':
                for i in colNames:
                    if (len(self._obj[i].unique())>threshold):
                        self._obj[i].replace(missingValues, round(stats.mode(self._obj[i])[0][0], 2), inplace=True)
                    
              
            elif imputeContinuousWith == 'zero':
                for i in colNames:
                    if (len(self._obj[i].unique())>threshold):
                        self._obj[i].replace(missingValues, 0, inplace=True)
        
        if imputeCategorical:
            try:
                for i in colNames:
                    try:
                        if (len(self._obj[i].unique())<threshold):
                            if (replaceCategoricalWith is None):
                                self._obj[i].replace(missingValues, self._obj.groupby(i)[i].count().index[0], inplace=True)
                            else:
                                self._obj[i].replace(missingValues,replaceCategoricalWith , inplace=True)
                    except:
                        continue
            except Exception as e:
                print("Unable to perform imputation for categorical column")
                print(e)
                
        return(self._obj)
   
    def scaleData(self,contcols,scaler='StandardScaler'):
        
        '''

        The function scales the continuous columns in the dataset based on user selection
        
        Parameters
        ----------
        pandas_obj : pandas Dataframe
        contcols   : list of continuous columns 
        scaler     : Method of scaling implemented from sklearn (StandardScaler, MinMaxScaler, RobustScaler, MaxAbsScaler)
        
        Usage
        -----
        df.dataclean.scaleData()
        
        Returns
        -------
        A dataframe where the continuous columns have been brought to the same scale
        
        '''
        
        self._obj = self._obj.copy()
        
        if scaler == 'StandardScaler':
            scaled_data = StandardScaler().fit_transform(self._obj[contcols])
            self._obj[contcols] = pd.DataFrame.from_records(scaled_data, index=self._obj.index, columns=self._obj[contcols].columns)
            return self._obj
        
        if scaler == 'MinMaxScaler':
            scaled_data = MinMaxScaler().fit_transform(self._obj[contcols])
            self._obj[contcols] = pd.DataFrame.from_records(scaled_data, index=self._obj.index, columns=self._obj[contcols].columns)
            return self._obj
        
        if scaler == 'RobustScaler':
            scaled_data = RobustScaler().fit_transform(self._obj[contcols])
            self._obj[contcols] = pd.DataFrame.from_records(scaled_data, index=self._obj.index, columns=self._obj[contcols].columns)
            return self._obj
        
        if scaler == 'MaxAbsScaler':
            scaled_data = MaxAbsScaler().fit_transform(self._obj[contcols])
            self._obj[contcols] = pd.DataFrame.from_records(scaled_data, index=self._obj.index, columns=self._obj[contcols].columns)
            return self._obj
        
    def removeOutliers(self, colNames='all', lowerLimit=None, upperLimit=None, IQRCoeff=1.5):
        
        '''
        The function removes the outliers that are present in the column/dataset based on Interquartile range
        
        Parameters
        ----------
        pandas_obj : pandas dataframe
        colNames   : Column names
            	       if 'all', outliers of all the columns in the dataset are removed based on Interquartile range
        lowerLimit : Minimum threshold of values in the column that has been selected, with the default being None
        upperLimit : Maximum threshold of values in the column that has been selected, with the default being None
        IQRCoeff   : Coefficient of the Interquartile range, with the default value being 1.5 
        
        Usage
        -----
        df.dataclean.removeOutliers()
        
        Returns
        -------
        self._obj  : Pandas dataframe
        
        A dataframe where the outliers have been replaced with NaN's
        '''
        
        numerics = ['int16', 'int32', 'int64', 'float16', 'float32', 'float64','float','int']
        
        if colNames == 'all':
            colNames = self._obj.columns
    
        for i in colNames:
            if self._obj[i].dtype in numerics:
                q1 = self._obj[i].quantile(0.25)
                q3 = self._obj[i].quantile(0.75)
                iqr = q3 - q1
    
                if lowerLimit is None:
                    lowerLimit = q1 - IQRCoeff * iqr
                if upperLimit is None:
                    upperLimit = q3 + IQRCoeff * iqr
    
                self._obj[i] = self._obj[i].where((self._obj[i] > (lowerLimit - 1)) & (self._obj[i] < (upperLimit + 1)))
    
        return(self._obj)
          
    
    
    def removePrimaryKey(self,columnList):
        try:
            self._obj.drop(columnList,axis=1,inplace=True)
        except Exception as e:
            print("Could not drop ID columns")
            print(e)
        pass 
    
    
    def toContinuous(self,colNames='all'):
        '''
        The function converts the columns to numeric datatype based on user selection
        
        Parameters
        ----------
        pandas_obj : pandas dataframe
        colNames   : Column names
            	       if 'all',all the columns in the dataset are converted to numeric datatype
        
        Usage
        -----
        df.dataclean.toContinuous()
        
        Returns
        -------
        self._obj  : Pandas dataframe
        
        A dataframe where the datatype of categorical columns is converted to numeric datatype
        '''
        self._obj=self._obj.copy() 
        if colNames=='all':
             colNames=self._obj.columns
             for i in colNames:
                 self._obj[i]=pd.to_numeric(self._obj[i],errors='ignore')
        else:
            for i in colNames:
                self._obj[i]=pd.to_numeric(self._obj[i],errors='coerce')
        return(self._obj)    
 
    
    
    def toCategorical(self,contCols,catCols,toCatCols):
        '''
        The function returns a tuple with two lists of continuous and categorical columns
        
        Parameters
        ----------
        pandas_obj : pandas dataframe
        contCols   : List of continuous columns given by the user
        catcols    : List of categorical columns given by the user
        toCatCols  : List of continuous columns which should be categorical
        Usage
        -----
        df.dataclean.toCategorical()
        
        Returns
        -------
        tuple : with 2 lists
        
        A tuple with 2 lists where one is continuous list and other is categorical list
        '''
        self._obj=self._obj.copy()
        reqContCols=list(set(contCols)-set(toCatCols))
        catCols=catCols+toCatCols
        return (reqContCols,catCols)
    
    def getMissingPercent(self, printInfo = False):
        """
        The funcion look for missing values(np.nan) in all the columns
        of dataframe and return a dataframe containing mssing value count
        and missing percentage in each column.
    		
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        printInfo  : boolean:True/False
                     If true print the return dataframe in console
    
        Usage
        --------
        >>df.univariate.getMissingPercent()
                    | MissingCount    | MissingPercent
        column1     | 20              | 10
        column2     | 10              | 5
    
        Returns
        -------
        Nothing. Your original DataFrame is modified inplace
       
        """
        missingTable = pd.DataFrame()
        missingTable['MissingCount'] = self._obj.isnull().sum()
        missingTable['MissingPercent'] = 100 * self._obj.isnull().sum()/len(self._obj)
        missingTable.set_index(self._obj.columns, inplace=True)
        missingTable.reset_index(inplace=True)
        if printInfo:
            print(missingTable)
        return missingTable
    
    def removeMissingColumns(self, missingPercentLimit = 20):
        """
        The function will drop all the columns that have
        missing values(np.nan) more than defined missingPercent
        limit. Note that drop is not done inplace, therefore
        only returned dataframe will contain such changes
		
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        missingPercentLimit  : integer
                          Threshold for permissible missing values
                          in dataframe

        Usage
        --------
        >>df.univariate.getMissingPercent()
                    | MissingCount    | MissingPercent
        column1     | 20              | 10
        column2     | 10              | 5

        Returns
        -------
        Nothing. Your original DataFrame is modified inplace
   
        """
        missingTable = self._obj.dataclean.getMissingPercent()
        dropMissingList = list(missingTable[missingTable['MissingPercent']>=missingPercentLimit].index)
        return self._obj.drop(dropMissingList,axis=1)
