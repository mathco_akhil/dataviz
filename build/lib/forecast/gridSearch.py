# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 14:42:53 2019

@author: Akhil Saraswat
"""

import pandas as pd
import numpy as np
import itertools
from statsmodels.tsa.arima_model import ARIMA
from statsmodels.tsa.statespace.sarimax import SARIMAX


class gridSearch:
    
    def _init_(max_p=3,max_d=3,max_q=3,max_m=12):
        self.max_p=max_p,
        self.max_d=max_d
        self.max_q=max_q
        self.max_m=max_m
    
    
    def searchArima(df,depVar,max_p=3,max_d=3,max_q=3,silent=False):
        try:
            p=range(0,max_p)
        except Exception as e:
            print("max_p has to be an integer greater than 0")
            print(e)
        try:
            d=range(0,max_d)
        except Exception as e:
            print("max_d has to be an integer greater than 0")
            print(e)
        try:
            q=range(0,max_q)
        except Exception as e:
            print("max_q has to be an integer greater than 0")
            print(e)
        # Generate all different combinations of p, q and q triplets
        try:
            pdq = list(itertools.product(p, d, q))
        except Exception as e:
            print("Unable to create list of all combination of pdq from the given value")
            print(e)
    
        
        try:
            bestAIC = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        bestParam = None
        
        print('Running GridSearch')
        
        #use gridsearch to look for optimial arima parameters
        try:
            for param in pdq:       
                try:
                    model = ARIMA(df[depVar],order=param).fit(disp=0)
    
    
                    #if current run of AIC is better than the best one so far, overwrite it
                    if model.aic<bestAIC:
                        bestAIC = model.aic
                        bestParam = param
    
                except Exception as e:
                    print(e)
                    continue
        except Exception as e:
            print("unable to iterate through pdq")
            print(e)
                    
        return {'Best AIC':bestAIC,'order':bestParam}
        
    def searchSarima(df,depVar,max_p=3,max_d=3,max_q=3,max_m=12):
        try:
            p=range(0,max_p)
        except Exception as e:
            print("max_p has to be an integer greater than 0")
            print(e)
        try:
            d=range(0,max_d)
        except Exception as e:
            print("max_d has to be an integer greater than 0")
            print(e)
        try:
            q=range(0,max_q)
        except Exception as e:
            print("max_q has to be an integer greater than 0")
            print(e)
        m=range(0,max_m)
        # Generate all different combinations of p, q and q triplets
        try:
            pdq = list(itertools.product(p, d, q))
        except Exception as e:
            print("Unable to create list of all combination of pdq from the given value")
            print(e)
    
        # Generate all different combinations of seasonal p, q and q triplets
        try:
            seasonalPDQ = [(x[0], x[1], x[2], x[3]) for x in list(itertools.product(p, d, q, m))]
        except Exception as e:
            print("Unable to create list of all combination of seasonalPDQ from the given value")
            print(e)
    
        
        try:
            bestAIC = np.inf
        except Exception as e:
            print("Numpy is not installed/imported in time series function module")
            print(e)
        bestParam = None
        bestSParam = None
        
        print('Running GridSearch')
        
        #use gridsearch to look for optimial arima parameters
        try:
            for param in pdq:
                
                try:
                    for paramSeasonal in seasonalPDQ:
                        try:
                            model = SARIMAX(df[depVar],
                                         order=param,
                                         seasonal_order=paramSeasonal,
                                         enforce_stationarity=False,
                                         enforce_invertibility=False).fit()
    
    
                            #if current run of AIC is better than the best one so far, overwrite it
                            if model.aic<bestAIC:
                                bestAIC = model.aic
                                bestParam = param
                                bestSParam = paramSeasonal
    
                        except:
                            continue
                except Exception as e:
                    print("unable to iterate through seasonalPDQ")
                    print(e)
        except Exception as e:
            print("unable to iterate through pdq")
            print(e)
                    
        return {'Best AIC':bestAIC,'order':bestParam,'seasonal_order':bestSParam}