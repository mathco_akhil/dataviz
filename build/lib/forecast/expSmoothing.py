# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 13:44:49 2019

@author: TheMathCompany
"""

import pandas as pd
import numpy as np
from statsmodels.tsa.api import SimpleExpSmoothing

@pd.api.extensions.register_dataframe_accessor("expSmoothing")
class ExpSmoothing(object):

    def __init__(self, pandas_obj):
        self._obj = pandas_obj

    def build(self,depVar=None, smoothing_level=None, optimized=True):
        """
        Parameters
        ----------
        df    : pandas dataframe
        depVar: dependent variable name
            The name of the variable for prediction
        smoothing_level : float, optional
            The smoothing_level value of the simple exponential smoothing, if the value is
            set then this value will be used as the value.
        optimized : bool
            Should the values that have not been set above be optimized automatically?
        """
        df = self._obj
        if depVar is not None:
            try:
                model = SimpleExpSmoothing(df[depVar])
            except Exception as e:
                print("Unable to run exponentialSmoothing with the given data")
                print(e)
            try:
                model_fit = model.fit(smoothing_level=smoothing_level,optimized=optimized)
            except Exception as e:
                print("Unable to fit SimpleExpSmoothing to the dataset")
                print(e)
        else:
            try:
                model = SimpleExpSmoothing(df[0])
            except Exception as e:
                print("Unable to run exponentialSmoothing to the dataset, please provide depVar")
                print(e)
            try:
                model_fit = model.fit(smoothing_level=smoothing_level,optimized=optimized)
            except Exception as e:
                print("Unable to fit SimpleExpSmoothing to the dataset")
                print(e)
        return model_fit

    
    def scoreTrain(self,model_fit,depVar):
        """
        This function return the predicted values for train set and print
        MAPE for the Train set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of exponential smoothing
        
        depVar           : column name of the dependent variable as string
        """
        
        df = self._obj
        try:
            df['predicted']=model_fit.predict(start=0,end=len(df))
        except Exception as e:
            print("unable to run predict function the dataframe")
            print(e)
            
        try:
            mape = np.mean(np.abs((df[depVar]-df['predicted']) / df[depVar])) * 100
        except Exception as e:
            print("Error occured while calculating mape")
            print(e)
        try:
            print("Train MAPE : %f"%mape)
            print("AIC : %f"%model_fit.aic)
        except Exception as e:
            print("Mape value does not exist")
            print(e)
        return df
    
    
    def scoreTest(self,model_fit,depVar):
        """
        This function return the forecasted values for test set and print
        MAPE for the test set.
        
        pandas_obj       : A pandas DataFrame
        
        model_fit        : fitted model of exponential Smoothing
        
        depVar           : column name of the dependent variable as string
        """
        
        df = self._obj
        try:
            df['predicted']=np.array(model_fit.forecast(len(df)))
        except Exception as e:
            print("Unable to forecast for the given length of dataframe")
            print(e)
        try:
            mape = np.mean(np.abs((df[depVar]-df['predicted']) / df[depVar])) * 100
        except Exception as e:
            print("Error occured while calculating mape")
            print(e)
        try:
            print("Test MAPE : %f"%mape)
        except Exception as e:
            print("Mape value does not exist")
            print(e)
        return df