# -*- coding: utf-8 -*-
"""
Created on Fri Feb 22 10:01:34 2019

@author: Lenovo
"""

import pandas as pd
import numpy as np
import pickle
from sklearn import svm
from sklearn.svm import SVC
@pd.api.extensions.register_dataframe_accessor("supportVectorMachine")
class supportVectorMachine(object):
    """
    
    Contains functions for building a SVM model and generating the scores for the train and test dataset.
    
    """


    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def buildModel(
        self,
        depVar,
        C=1.0,
        kernel='rbf',
        degree=3,
        gamma='auto',
        coef0=0.0,
        shrinking=True,
        probability=True,
        tol=0.001,
        cache_size=200, 
        class_weight=None, 
        verbose=False, 
        max_iter=-1,
        decision_function_shape='ovr',
        random_state=100):
        """
        This function builds a Support Vector Machine model.
        It could be used to build an SVM model on the train data and the model could be tuned by altering the hyperparameters for better results.
        
        Parameters
        ----------
        pandas_obj              : A pandas DataFrame
        depVar                  : Dependent variable from the dataset
        C                       : float, optional (default=1.0)
        Penalty parameter C of the error term.
        kernel                  : string, optional (default=’rbf’)
        Specifies the kernel type to be used in the algorithm. It must be one of ‘linear’, ‘poly’, ‘rbf’, ‘sigmoid’, ‘precomputed’ or a callable. If none is given, ‘rbf’ will be used. If a callable is given it is used to pre-compute the kernel matrix from data matrices; that matrix should be an array of shape (n_samples, n_samples).
        degree                  : int, optional (default=3)
        Degree of the polynomial kernel function (‘poly’). Ignored by all other kernels.
        gamma                   : float, optional (default=’auto’)
        Kernel coefficient for ‘rbf’, ‘poly’ and ‘sigmoid’.
        Current default is ‘auto’ which uses 1 / n_features, if gamma='scale' is passed then it uses 1 / (n_features * X.var()) as value of gamma. The current default of gamma, ‘auto’, will change to ‘scale’ in version 0.22. ‘auto_deprecated’, a deprecated version of ‘auto’ is used as a default indicating that no explicit value of gamma was passed.
        coef0                   : float, optional (default=0.0)
        Independent term in kernel function. It is only significant in ‘poly’ and ‘sigmoid’.
        shrinking               : boolean, optional (default=True)
        Whether to use the shrinking heuristic.
        probabilitysvm          : boolean, optional (default=False)
        Whether to enable probability estimates. This must be enabled prior to calling fit, and will slow down that method.
        tol                     : float, optional (default=1e-3)
        Tolerance for stopping criterion.
        cache_size              : float, optional
        Specify the size of the kernel cache (in MB).
        class_weight            : {dict, ‘balanced’}, optional
        Set the parameter C of class i to class_weight[i]*C for SVC. If not given, all classes are supposed to have weight one. The “balanced” mode uses the values of y to automatically adjust weights inversely proportional to class frequencies in the input data as n_samples / (n_classes * np.bincount(y))
        verbose                 : bool, default: False
        Enable verbose output. Note that this setting takes advantage of a per-process runtime setting in libsvm that, if enabled, may not work properly in a multithreaded context.
        max_iter                : int, optional (default=-1)
        Hard limit on iterations within solver, or -1 for no limit.
        decision_function_shape : ‘ovo’, ‘ovr’, default=’ovr’
        Whether to return a one-vs-rest (‘ovr’) decision function of shape (n_samples, n_classes) as all other classifiers, or the original one-vs-one (‘ovo’) decision function of libsvm which has shape (n_samples, n_classes * (n_classes - 1) / 2). However, one-vs-one (‘ovo’) is always used as multi-class strategy.
        random_state            : int, RandomState instance or None, optional (default=None)
        The seed of the pseudo random number generator used when shuffling the data for probability estimates. If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.

        Usage
        --------
        df.supportVectorMachine.buildModel(depVar='p',random_statesvm=101) #Various other parameters can be added.

        Returns
        -------
        A model object.
        
        """ 
        y = self._obj[depVar]
        x = self._obj.drop(depVar, axis=1)
        svm_model = SVC(C=C,
        kernel=kernel,
        degree=degree,
        gamma=gamma,
        coef0=coef0,
        shrinking=shrinking,
        probability=probability,
        tol=tol,
        cache_size=cache_size,
        class_weight=class_weight,
        verbose=verbose,
        max_iter=max_iter,
        decision_function_shape=decision_function_shape, 
        random_state=random_state)
        svm_model_result = svm_model.fit(x, y)
 #       filename = 'buildModelSVM.pkl'
 #       pickle.dump(svm_model_result, open(filename, 'wb'))
        return(svm_model_result)
        
        
        
    def scoreDataset(self,svmModelResult,depVar=None):
        """
        A function to generate scores for both the train and test dataset.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        svmModelResult : SVM model object
        dep_var : Dependant variable from the dataset.
        
        Usage
        --------
        df.supportVectorMachine.scoreDataset(svmModelResult='a',depVar='p')

        Returns
        -------
        A dataframe with the scores appended to each datapoint.
        
        """
        x4 = self._obj.drop(depVar, axis=1)
        scoring_test = svmModelResult.predict_proba(x4)
        
#        np.set_printoptions(threshold=np.nan)
        dataframe_test=pd.DataFrame(scoring_test, columns=['0','1'])
        scoringtest=dataframe_test['1']
        df_d = pd.concat([self._obj, dataframe_test], axis=1)
        
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
 #       df_d.to_csv("dataset_testing.csv",index=False,index_label=False)
        
        return(df_d)
            
        
    