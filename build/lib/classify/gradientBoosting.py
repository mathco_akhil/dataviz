# -*- coding: utf-8 -*-
"""
Created on Mon Feb 25 14:29:19 2019

@author: Lenovo
"""

import pandas as pd
import numpy as np
import pickle
from sklearn.ensemble import GradientBoostingClassifier
@pd.api.extensions.register_dataframe_accessor("gradientBoosting")
class gradientBoosting(object):
    """
    
    Contains functions for building a Gradient Boosting model,generating feature importance based on the model built and also generating the scores for the train and test dataset.
    
    
    """


    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
        
    def buildModel(
        self,
        depVar,
        loss='deviance',
        learning_rate=0.1,
        n_estimators=100,
        subsample=1.0,
        criterion='friedman_mse',
        min_samples_split=2, 
        min_samples_leaf=1,
        min_weight_fraction_leaf=0.0,
        max_depth=3,
        min_impurity_decrease=0.0,
        min_impurity_split=None,
        init=None,
        random_state=None, 
        max_features=None,
        verbose=0,
        max_leaf_nodes=None,
        warm_start=False,
        presort='auto',
        validation_fraction=0.1,
        n_iter_no_change=None,
        tol=0.0001):
        """
        This function builds a XgBoost/Adaboost model.
        It could be used to build a GBM model on the train data and the model could be tuned by altering the hyperparameters for better results.
        
        Parameters
        ----------
        pandas_obj              : A pandas DataFrame
        depVar                  : Dependent variable from the dataset
        loss                    : {‘deviance’, ‘exponential’}, optional (default=’deviance’)
        loss function to be optimized. ‘deviance’ refers to deviance (= logistic regression) for classification with probabilistic outputs. For loss ‘exponential’ gradient boosting recovers the AdaBoost algorithm.
        learning_rate           : float, optional (default=0.1)
        learning rate shrinks the contribution of each tree by learning_rate. There is a trade-off between learning_rate and n_estimators.
        n_estimators            : int (default=100)
        The number of boosting stages to perform. Gradient boosting is fairly robust to over-fitting so a large number usually results in better performance.
        subsample               : float, optional (default=1.0)
        The fraction of samples to be used for fitting the individual base learners. If smaller than 1.0 this results in Stochastic Gradient Boosting. subsample interacts with the parameter n_estimators. Choosing subsample < 1.0 leads to a reduction of variance and an increase in bias.
        criterion               : string, optional (default=”friedman_mse”)
        The function to measure the quality of a split. Supported criteria are “friedman_mse” for the mean squared error with improvement score by Friedman, “mse” for mean squared error, and “mae” for the mean absolute error. The default value of “friedman_mse” is generally the best as it can provide a better approximation in some cases.
        min_samples_split       : int, float, optional (default=2)
        The minimum number of samples required to split an internal node:
        If int, then consider min_samples_split as the minimum number.
        If float, then min_samples_split is a fraction and ceil(min_samples_split * n_samples) are the minimum number of samples for each split.
        min_samples_leaf        : int, float, optional (default=1)
        The minimum number of samples required to be at a leaf node. A split point at any depth will only be considered if it leaves at least min_samples_leaf training samples in each of the left and right branches. This may have the effect of smoothing the model, especially in regression.
        If int, then consider min_samples_leaf as the minimum number.
        If float, then min_samples_leaf is a fraction and ceil(min_samples_leaf * n_samples) are the minimum number of samples for each node.
        min_weight_fraction_leaf: float, optional (default=0.)
        The minimum weighted fraction of the sum total of weights (of all the input samples) required to be at a leaf node. Samples have equal weight when sample_weight is not provided.
        max_depth : integer, optional (default=3)
        maximum depth of the individual regression estimators. The maximum depth limits the number of nodes in the tree. Tune this parameter for best performance; the best value depends on the interaction of the input variables.
        min_impurity_decrease   : float, optional (default=0.)
        A node will be split if this split induces a decrease of the impurity greater than or equal to this value.
        The weighted impurity decrease equation is the following:
        N_t / N * (impurity - N_t_R / N_t * right_impurity
                            - N_t_L / N_t * left_impurity)
        where N is the total number of samples, N_t is the number of samples at the current node, N_t_L is the number of samples in the left child, and N_t_R is the number of samples in the right child.
        N, N_t, N_t_R and N_t_L all refer to the weighted sum, if sample_weight is passed.
        min_impurity_split      : float, (default=1e-7)
        Threshold for early stopping in tree growth. A node will split if its impurity is above the threshold, otherwise it is a leaf.
        init                    : estimator or ‘zero’, optional (default=None)
        An estimator object that is used to compute the initial predictions. init has to provide fit and predict_proba. If ‘zero’, the initial raw predictions are set to zero. By default, a DummyEstimator predicting the classes priors is used.
        random_state            : int, RandomState instance or None, optional (default=None)
        If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random.
        max_features            : int, float, string or None, optional (default=None)
        The number of features to consider when looking for the best split:
        If int, then consider max_features features at each split.
        If float, then max_features is a fraction and int(max_features * n_features) features are considered at each split.
        If “auto”, then max_features=sqrt(n_features).
        If “sqrt”, then max_features=sqrt(n_features).
        If “log2”, then max_features=log2(n_features).
        If None, then max_features=n_features.
        Choosing max_features < n_features leads to a reduction of variance and an increase in bias.
        Note: the search for a split does not stop until at least one valid partition of the node samples is found, even if it requires to effectively inspect more than max_features features.
        verbose : int, default: 0
        Enable verbose output. If 1 then it prints progress and performance once in a while (the more trees the lower the frequency). If greater than 1 then it prints progress and performance for every tree.
        max_leaf_nodes          : int or None, optional (default=None)
        Grow trees with max_leaf_nodes in best-first fashion. Best nodes are defined as relative reduction in impurity. If None then unlimited number of leaf nodes.
        warm_start              : bool, default: False
        When set to True, reuse the solution of the previous call to fit and add more estimators to the ensemble, otherwise, just erase the previous solution. See the Glossary.
        presort                 : bool or ‘auto’, optional (default=’auto’)
        Whether to presort the data to speed up the finding of best splits in fitting. Auto mode by default will use presorting on dense data and default to normal sorting on sparse data. Setting presort to true on sparse data will raise an error.
        validation_fraction     : float, optional, default 0.1
        The proportion of training data to set aside as validation set for early stopping. Must be between 0 and 1. Only used if n_iter_no_change is set to an integer.
        n_iter_no_change        : int, default None
        n_iter_no_change is used to decide if early stopping will be used to terminate training when validation score is not improving. By default it is set to None to disable early stopping. If set to a number, it will set aside validation_fraction size of the training data as validation and terminate training when validation score is not improving in all of the previous n_iter_no_change numbers of iterations. The split is stratified.
        tol                     : float, optional, default 1e-4
        Tolerance for the early stopping. When the loss is not improving by at least tol for n_iter_no_change iterations (if set to a number), the training stops.
       
        Usage
        --------
        df.gradientBoosting.buildModel(depVar='p',random_state=101) #Various other parameters can be added.

        Returns
        -------
        A model object.
        
        """ 
        y = self._obj[depVar]
        x = self._obj.drop(depVar, axis=1)
        gbm_model = GradientBoostingClassifier(
        loss=loss,
        learning_rate=learning_rate,
        n_estimators=n_estimators,
        subsample=subsample,
        criterion=criterion,
        min_samples_split=min_samples_split,
        min_samples_leaf=min_samples_leaf,
        min_weight_fraction_leaf=min_weight_fraction_leaf,
        max_depth=max_depth,
        min_impurity_decrease=min_impurity_decrease,
        min_impurity_split=min_impurity_split,
        init=init,
        random_state=random_state,
        max_features=max_features,
        verbose=verbose,max_leaf_nodes=max_leaf_nodes,
        warm_start=warm_start,
        presort=presort,
        validation_fraction=validation_fraction,
        n_iter_no_change=n_iter_no_change,
        tol=tol)

        gbm_model_result = gbm_model.fit(x, y)
        filename = 'buildModelGradientBoosting.pkl'
        pickle.dump(gbm_model_result, open(filename, 'wb'))
        return(gbm_model_result)
        
        
    def getSummary(self,gbmModelResult):
        """
        This function gives feature importance of the Gradient Boosting model built.
        The module takes the model object from the model built as an input.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        gbmModelResult : Gradient Boosting model object
        
        Usage
        --------
        df.gradientBoosting.getSummary(gbmModelResult=a)

        Returns
        -------
        A dataframe with feature and the feature importance for each feature.
                
        """
 #       a= logModel.summary()
 #       np.savetxt(r'c:\data\np.txt', df.values, fmt='%d')
        feat_imp=gbmModelResult.feature_importances_
        feature=self._obj.columns
        feat=pd.DataFrame(feature,columns=['Feature'])
 #       feat["                      "] = self._obj.apply(lambda _: '------------------------->',axis=1)
        feat_importance=pd.DataFrame(feat_imp,columns=['Feature Importance'])
        df_feat_imp1 = pd.concat([feat,feat_importance], axis=1)
        df_feat_imp1.to_csv("dataset_featureimportance.csv",index=False,index_label=False)
        return(df_feat_imp1)
        
        
    def scoreDataset(self,gbmModelResult,depVar=None):
        """
        A function to generate scores for both the train and test dataset.
        
        Parameters
        ----------
        pandas_obj : A pandas DataFrame
        gbmModelResult : Gradient Boosting model object.
        depVar : Dependant variable from the dataset.
        
        Usage
        --------
        df.gradientBoosting.scoreDataset(gbmModelResult='a',depVar='p')

        Returns
        -------
        A dataframe with the scores appended to each datapoint.
        
        """
        x4 = self._obj.drop(depVar, axis=1)
        scoring_test = gbmModelResult.predict_proba(x4)
        
 #       np.set_printoptions(threshold=np.nan)
        dataframe_test=pd.DataFrame(scoring_test, columns=['0','1'])
        scoringtest=dataframe_test['1']
        df_d = pd.concat([self._obj, dataframe_test], axis=1)
        
        df_d = df_d.rename(columns={"1":"Pred_Prob"})
#        df_d.to_csv("dataset_testing_GBM.csv",index=False,index_label=False)
        
        return(df_d)
            
        
    