import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import matplotlib.patheffects as PathEffects
import seaborn as sns
from sklearn.manifold import TSNE


@pd.api.extensions.register_dataframe_accessor("tsne")
class Tsne(object):
    """
    This class helps in performing dimensionality reudction using T-Sne
    1. buildModel() - Builds PCA model with given hyperparameters and fits data
    2. visualizeTsne() - This function Creates a two dimensonal visualization of the T-Sne to to assess the data structure and detect clusters
    """

    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
    def buildModel(self,n_components=2, perplexity=30.0, early_exaggeration=12.0, learning_rate=200.0, n_iter=1000, n_iter_without_progress=300, min_grad_norm=1e-07, metric='euclidean', init='random', verbose=0, 
              random_state=None, method='barnes_hut', angle=0.5):
        '''
        Apply T-sne by fitting data and reduce the dimensions with desired number of components
        
        Parameters
        ----------
        n_components : int, optional (default: 2)
        Dimension of the embedded space.
    
        perplexity : float, optional (default: 30)
        The perplexity is related to the number of nearest neighbors that is used in other manifold learning algorithms. Larger datasets usually require a larger perplexity. Consider selecting a value between 5 and 50. The choice is not extremely critical since t-SNE is quite insensitive to this parameter.
    
        early_exaggeration : float, optional (default: 12.0)
        Controls how tight natural clusters in the original space are in the embedded space and how much space will be between them. For larger values, the space between natural clusters will be larger in the embedded space. Again, the choice of this parameter is not very critical. If the cost function increases during initial optimization, the early exaggeration factor or the learning rate might be too high.
    
        learning_rate : float, optional (default: 200.0)
        The learning rate for t-SNE is usually in the range [10.0, 1000.0]. If the learning rate is too high, the data may look like a ‘ball’ with any point approximately equidistant from its nearest neighbours. If the learning rate is too low, most points may look compressed in a dense cloud with few outliers. If the cost function gets stuck in a bad local minimum increasing the learning rate may help.
    
        n_iter : int, optional (default: 1000)
        Maximum number of iterations for the optimization. Should be at least 250.
    
        n_iter_without_progress : int, optional (default: 300)
        Maximum number of iterations without progress before we abort the optimization, used after 250 initial iterations with early exaggeration. Note that progress is only checked every 50 iterations so this value is rounded to the next multiple of 50.
    
    
        min_grad_norm : float, optional (default: 1e-7)
        If the gradient norm is below this threshold, the optimization will be stopped.
    
        metric : string or callable, optional
        The metric to use when calculating distance between instances in a feature array. If metric is a string, it must be one of the options allowed by scipy.spatial.distance.pdist for its metric parameter, or a metric listed in pairwise.PAIRWISE_DISTANCE_FUNCTIONS. If metric is “precomputed”, X is assumed to be a distance matrix. Alternatively, if metric is a callable function, it is called on each pair of instances (rows) and the resulting value recorded. The callable should take two arrays from X as input and return a value indicating the distance between them. The default is “euclidean” which is interpreted as squared euclidean distance.
    
        init : string or numpy array, optional (default: “random”)
        Initialization of embedding. Possible options are ‘random’, ‘pca’, and a numpy array of shape (n_samples, n_components). PCA initialization cannot be used with precomputed distances and is usually more globally stable than random initialization.
    
        verbose : int, optional (default: 0)
        Verbosity level.
    
        random_state : int, RandomState instance or None, optional (default: None)
        If int, random_state is the seed used by the random number generator; If RandomState instance, random_state is the random number generator; If None, the random number generator is the RandomState instance used by np.random. Note that different initializations might result in different local minima of the cost function.
    
        method : string (default: ‘barnes_hut’)
        By default the gradient calculation algorithm uses Barnes-Hut approximation running in O(NlogN) time. method=’exact’ will run on the slower, but exact, algorithm in O(N^2) time. The exact algorithm should be used when nearest-neighbor errors need to be better than 3%. However, the exact method cannot scale to millions of examples.
    
    
        angle : float (default: 0.5)
        Only used if method=’barnes_hut’ This is the trade-off between speed and accuracy for Barnes-Hut T-SNE. ‘angle’ is the angular size (referred to as theta in [3]) of a distant node as measured from a point. If this size is below ‘angle’ then it is used as a summary node of all points contained within it. This method is not very sensitive to changes in this parameter in the range of 0.2 - 0.8. Angle less than 0.2 has quickly increasing computation time and angle greater 0.8 has quickly increasing error.
        
        Usage
        --------
        df.tsne.buildModel()
        
        Returns
        -------
        model object
        '''
        tsne = TSNE(n_components=2, perplexity=30.0, early_exaggeration=12.0, learning_rate=200.0, n_iter=1000, n_iter_without_progress=300, min_grad_norm=1e-07, metric='euclidean', init='random', verbose=0, 
                    random_state=None, method='barnes_hut', angle=0.5).fit_transform(self._obj)
        
        return tsne
    
    def visualizeTsne(self,reduced_data, target):
        '''
        This function Creates a two dimensonal visualization of the T-Sne to to assess the data structure and detect clusters
        
        Parameters
        ----------
        reduced_data: Array of reduced components from T-sne
        
        target: target variable from original dataframe or cluster lables
        
        Usage
        --------
        df.tsne.visualizeTsne()
        
        Returns
        -------
        Matplotlib object
        '''
        # choose a color palette with seaborn.
        num_classes = len(np.unique(target))
        palette = np.array(sns.color_palette("hls", num_classes))
    
        # create a scatter plot.
        plt.figure(figsize=(15, 12))
        ax = plt.subplot(aspect='equal')
        
        ax.scatter(reduced_data[:,0], reduced_data[:,1], lw=0, s=40, c=palette[target.astype(np.int)])
        
        plt.xlim(reduced_data[:,0].min()*1.2,reduced_data[:,0].max()*1.2)
        plt.ylim(reduced_data[:,1].min()*1.2,reduced_data[:,1].max()*1.2)
        plt.xlabel("Dimension 1")
        plt.ylabel("Dimension 2")
        #ax.axis('off')
        #ax.axis('tight')
        # add the labels for each digit corresponding to the label
        txts = []
    
        for i in range(num_classes):
    
            # Position of each label at median of data points.
    
            xtext, ytext = np.median(reduced_data[target == i, :], axis=0)
            txt = ax.text(xtext, ytext, str(i), fontsize=24)
            try:
                txt.set_path_effects([
                PathEffects.Stroke(linewidth=5, foreground="w"),
                PathEffects.Normal()])
            except Exception as python_error_message:
                print("Error occured while creating path effects")
                print(python_error_message)
            
            txts.append(txt)
    
        return plt

