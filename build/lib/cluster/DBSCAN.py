import pandas as pd
from sklearn.cluster import DBSCAN
import matplotlib.pyplot as plt
from sklearn.neighbors import NearestNeighbors
import plotly.graph_objs as go
from plotly.offline import plot


@pd.api.extensions.register_dataframe_accessor("dbscan")
class DBSCANCluster(object):
    """
    This class builds predict and visualize DBSCAN clustering
    1. buildModel() - Builds DBSCAN model with given hyperparameters and fits data
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns visualised plot with cluster labels
    4. buildEpsdist() - This function plots a K Plot to find optimize epochs distance for DBSCAN provied minimum samples required
    """

    def __init__(self, pandas_obj):
        self._obj = pandas_obj
        
    def buildModel(self,eps,min_samples,metric='euclidean',metric_params=None, algorithm='auto', leaf_size=30, p=None, n_jobs=None):
        '''
        Apply DBSCAN by fitting data with eps and min_samples
        Generate clusters for each data point
        
        Parameters
        ----------
        df: dataframe to be used for clustering
    
        eps: The maximum distance between two samples for them to be considered as in the same neighborhood.
    
        min_samples: The number of samples (or total weight) in a neighborhood for a point to be considered as a core point. This includes the point itself.
    
        metric: The metric to use when calculating distance between instances in a feature array. If metric is a string or callable, 
        it must be one of the options allowed by sklearn.metrics.pairwise_distances for its metric parameter. 
        If metric is “precomputed”, X is assumed to be a distance matrix and must be square. 
        X may be a sparse matrix, in which case only “nonzero” elements may be considered neighbors for DBSCAN.
    
        metric_params : dict, optional
        Additional keyword arguments for the metric function.

        algorithm : {‘auto’, ‘ball_tree’, ‘kd_tree’, ‘brute’}, optional
        The algorithm to be used by the NearestNeighbors module to compute pointwise distances and find nearest neighbors. See NearestNeighbors module documentation for details.

        leaf_size : int, optional (default = 30)
        Leaf size passed to BallTree or cKDTree. This can affect the speed of the construction and query, as well as the memory required to store the tree. The optimal value depends on the nature of the problem.

        p : float, optional
        The power of the Minkowski metric to be used to calculate distance between points.

        n_jobs : int or None, optional (default=None)
        The number of parallel jobs to run. None means 1 unless in a joblib.parallel_backend context. -1 means using all processors.
        
        Usage
        --------
        df.dbscan.buildModel()

        Returns
        -------
        fitted model object
        '''
        
        df = self._obj.copy()
        dbsc = DBSCAN(eps=eps, min_samples=min_samples,metric=metric,metric_params=metric_params, algorithm=algorithm, leaf_size=leaf_size, p=p, n_jobs=n_jobs).fit(df)
        return dbsc
    
    def predictCluster(self,model):
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        model : model object
        
        Usage
        --------
        df.dbscan.predictCluster()

        Returns
        -------
        pandas dataframe
        
        """

        labels = model.fit_predict(self._obj)
        df = self._obj.copy()
        df['clusters'] = labels
        return df
        
    def visualizeCluster(self,var1,var2,model):
        '''
        This function used to plot DBScan cluster visualization
        
        Parameters
        ----------
        var1: X axis variable
        
        var2: Y axis variable
        
        model : model object
        
        Usage
        --------
        df.dbscan.visualizeCluster()

        Returns
        -------
        Matplotlib object
        
        '''
        df =  self.predictCluster(model)
        fig, ax2 = plt.subplots(1, 1)
            
        fig.set_size_inches(18, 7)
        
        cmap = plt.cm.get_cmap('jet')
    # assigning label names to be shown in a plot
        for i, clusters in df.groupby('clusters'):
            if(i == -1):
                k = 'noise'
                i = i + 1
            else:
                k = 'cluster%d'%i
                i = i + 1
    # Plotting the scatter plot for the DBScan clustering 
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=k)
        
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        ax2.legend()
        plt.suptitle(("DBSCAN Clustering Visualization"),
                         fontsize=14, fontweight='bold')    
        return plt

    def buildEpsdist(self,min_samples):
        '''
        This function plots a K Plot to find optimize epochs distance for DBSCAN provied minimum samples required
        
        Parameters
        ----------
        min_samples: The number of samples (or total weight) in a neighborhood for a point to be considered as a core point. This includes the point itself.
        
        Usage
        --------
        df.dbscan.buildEpsdist()

        Returns
        -------
        Plotly object
        '''
    # Nearest neighbours for KPlot is calculated
        nbrs = NearestNeighbors(n_neighbors=min_samples).fit(self._obj)
    #K Nearest neighbours are calculated and are fit to ensure that the optimum epoch is obtained
        distances, indices = nbrs.kneighbors(self._obj)
        
        distanceDec = sorted(distances[:,min_samples-1])
    # Layout for the elbow plot is defined
        layout = go.Layout(width = 1000,height = 500,title = 'K-Distance plot',yaxis=dict(title='k-distance'),xaxis=dict(title='Points(sample) sorted by distance'))
        trace = go.Scatter(x=list(range(1,self._obj.shape[0]+1)),y=list(distanceDec))
        data = [trace]
        fig = go.Figure(data=data,layout=layout)
            
        config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']}
        return plot(fig,config=config)