import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as cm
import plotly.graph_objs as go
from plotly.offline import plot
import collections as cc
from sklearn.cluster import KMeans
from sklearn.metrics import silhouette_samples, silhouette_score

@pd.api.extensions.register_dataframe_accessor("kmeans")
class KMeansCluster(object):
    """
    Contains functions to build, predict and visualise KMeans
    1. buildModel() - Builds KMeans model with given hyperparameters and fits data
    2. predictModel() - Returns dataframe with appended cluster labels
    3. visualiseCluster() - Returns visualised plot with cluster labels
    4. visualizeSilhouette() - This function is used to display silhouette plot
    5. visualizeElbow() - This function is used to display elbow plot
    6. buildClusterInfo() - This user defined function prints the summary of the cluster such as population and centers of the cluster
    
    """   
    def __init__(self, pandas_obj):
        self._obj = pandas_obj


    def buildModel(self, n_clusters=8,init='k-means++', n_init=10, max_iter=300, tol=0.0001, precompute_distances='auto', verbose=0,random_state=None,copy_x=True, n_jobs=None, algorithm='auto'):
        """
        Apply KMeans by fitting data with number of clusters
        Generate clusters for each data point
        
        Parameters
        ----------
        n_clusters : int, optional, default: 8
        The number of clusters to form as well as the number of centroids to generate.
    
        init : {‘k-means++’, ‘random’ or an ndarray}
        Method for initialization, defaults to ‘k-means++’:
    
        ‘k-means++’ : selects initial cluster centers for k-mean clustering in a smart way to speed up convergence. See section Notes in k_init for more details.
    
        ‘random’: choose k observations (rows) at random from data for the initial centroids.
    
        If an ndarray is passed, it should be of shape (n_clusters, n_features) and gives the initial centers.
    
        n_init : int, default: 10
        Number of time the k-means algorithm will be run with different centroid seeds. The final results will be the best output of n_init consecutive runs in terms of inertia.
    
        max_iter : int, default: 300
        Maximum number of iterations of the k-means algorithm for a single run.
    
        tol : float, default: 1e-4
        Relative tolerance with regards to inertia to declare convergence
    
        precompute_distances : {‘auto’, True, False}
        Precompute distances (faster but takes more memory).
    
        ‘auto’ : do not precompute distances if n_samples * n_clusters > 12 million. This corresponds to about 100MB overhead per job using double precision.
    
        True : always precompute distances
    
        False : never precompute distances
    
        verbose : int, default 0
        Verbosity mode.
    
        random_state : int, RandomState instance or None (default)
        Determines random number generation for centroid initialization. Use an int to make the randomness deterministic. See Glossary.
    
        copy_x : boolean, optional
        When pre-computing distances it is more numerically accurate to center the data first. If copy_x is True (default), then the original data is not modified, ensuring X is C-contiguous. If False, the original data is modified, and put back before the function returns, but small numerical differences may be introduced by subtracting and then adding the data mean, in this case it will also not ensure that data is C-contiguous which may cause a significant slowdown.
    
        n_jobs : int or None, optional (default=None)
        The number of jobs to use for the computation. This works by computing each of the n_init runs in parallel.
        
        algorithm : “auto”, “full” or “elkan”, default=”auto”
        K-means algorithm to use. The classical EM-style algorithm is “full”. The “elkan” variation is more efficient by using the triangle inequality, but currently doesn’t support sparse data. “auto” chooses “elkan” for dense data and “full” for sparse data.
        
        
        Usage
        --------
        df.kmeans.buildModel(n_clusters = 4)

        
        Returns
        -------
        fitted model object
        
        """
        df = self._obj.copy()
        kmeans = KMeans(n_clusters=n_clusters,init=init, n_init=n_init, max_iter=max_iter, tol=tol, precompute_distances=precompute_distances, verbose=verbose,random_state=random_state,copy_x=copy_x, n_jobs=n_jobs, algorithm=algorithm)
        kmeans.fit(df)
        return kmeans
    
    def predictCluster(self,model):
        """
        Assigns cluster labels for the given dataframe
        
        Parameters
        ----------
        model : fitted model object
        
        Usage
        --------
        df.kmeans.predictCluster()

        Returns
        -------
        Dataframe with merged cluster 
        
        """

        labels = model.predict(self._obj)
        df = self._obj.copy()
        df['clusters'] = labels
        return df
    
    def visualizeCluster(self,var1,var2,model):
        """
        This function used to plot KMeans cluster visualization
        
        Parameters
        ----------
        var1: X axis variable
        
        var2: Y axis variable
        
        model: fitted model object
        
        Usage
        --------
        df.kmeans.visualizeCluster()

        Returns
        -------
        Matplotlib Object
        """
        centers = pd.DataFrame(model.cluster_centers_,columns=self._obj.columns)
        df = self.predictCluster(model)
    # Add a subplot to the current figure
        fig, ax2 = plt.subplots(1, 1)
        fig.set_size_inches(18, 7)
    # returns a jet colormap
        cmap = plt.cm.get_cmap('jet')
        for i, clusters in df.groupby('clusters'):
            ax2.scatter(clusters[var1], clusters[var2], marker='o', s=50, lw=0, alpha=0.7,
                        c=cmap(i/df.clusters.nunique()), edgecolor='k',label=i)
    # Labeling the clusters
    # Draw white stars at cluster centers
        ax2.legend()
        ax2.scatter(centers.loc[:, var1], centers.loc[:, var2], marker='*',
                        c="black", alpha=1, s=200, edgecolor='k',cmap='viridis')
            
        ax2.set_xlabel("Feature space for the %s"% var1)
        ax2.set_ylabel("Feature space for the %s"% var2)
        plt.suptitle(("K-Means Clustering Visualization"),
                        fontsize=14, fontweight='bold')
        return plt

    def buildClusterInfo(self,model):
        """
        This user defined function prints the summary of the cluster such as population and centers of the cluster
        
        Parameters
        ----------
        model: fitted model object
        
        Usage
        --------
        df.kmeans.buildClusterInfo()

        Returns
        -------
        pandas dataframe
        
        """
        
        df = self.predictCluster(model)
        centers = model.cluster_centers_
    # counts population for each cluster
        population = dict(cc.Counter(df.clusters))
        cluster_info = pd.DataFrame()
        cluster_info['Cluster'] = list(population.keys())
    # storing centers of clusters in a dataframe
        try:
            cluster_info['Cluster Center'] = list(centers.round(4))
        except Exception as python_error_message:
            print("Error occured while gennerating Cluster Center")
            print(python_error_message)
    # storing population of clusters in a dataframe
        try:
            cluster_info["Population"] = list(population.values())
        except Exception as python_error_message:
            print("Error occured while gennerating Population")
            print(python_error_message)
        return cluster_info
    
    def visualizeElbow(self, min_clust=2, max_clust=11):
        """
        This user  defined function displays elbow plot.
        
        Parameters
        ----------
        min_clust: minimum number of clusters. Default value is 2
        
        max_clust: maximum number of clusters. Default value is 11
        
        Usage
        --------
        df.kmeans.visualizeElbow()

        Returns
        -------
        Plotly Object
        """
        df = self._obj.copy()
    # creating empty dictionary to store within sum of square error for each cluster
        sse = {}
        for k in range(min_clust, max_clust):
            try:
    # Creates k clusters for the given data frame 
                kmeans = KMeans(n_clusters=k, max_iter=1000).fit(df)
            except Exception as python_error_message:
                print("Error occured while building KMeans clustering")
                print(python_error_message)
            try:
                df["clusters"] = kmeans.labels_
            except Exception as python_error_message:
                print("Error occured while generating cluster labels")
                print(python_error_message)
            try:
                sse[k] = kmeans.inertia_ 
            except Exception as python_error_message:
                print("Error occured while creating dictionary to store sum of square for respective clusters")
                print(python_error_message)
    # Plotting elbow plot
        layout = go.Layout(width = 1100,height = 500,title = 'Elbow Plot for Clustering',yaxis=dict(title='Sum of Square Error'),xaxis=dict(title='Cluster Count'))
        trace = go.Scatter(x=list(sse.keys()),y=list(sse.values()))
        data = [trace]
        fig = go.Figure(data=data,layout=layout)
        return plot(fig, config = {'showLink': False, 'displaylogo':False, 'modeBarButtonsToRemove':['sendDataToCloud']})
    
    def visualizeSilhouette(self,n_clusters,var1,var2,random_state=None):
        """
        This function is used to display silhouette plot a measure of how close each point in one cluster is to points in the neighboring clusters and thus provides a way to assess parameters like number of clusters visually. 
        
        Parameters
        ----------
        n_clusters: max number of number of clusters
        
        var1: X axis variable
        
        var2: Y axis variable
        
        random_state = seed value
        
        Usage
        --------
        df.kmeans.visualizeSilhouette()

        Returns
        -------
        Matplotlib Object
        """
        df = self._obj.copy()
        for n_clusters in range(2,n_clusters+1):
    # Create a subplot with 1 row and 2 columns
            fig, (ax1, ax2) = plt.subplots(1, 2)
            fig.set_size_inches(18, 7)
    
    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
            ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
            ax1.set_ylim([0, len(df) + (n_clusters + 1) * 10])
    
    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
            clusterer = KMeans(n_clusters=n_clusters, random_state=random_state)
            cluster_labels = clusterer.fit_predict(df)
    
    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
            silhouette_avg = silhouette_score(df, cluster_labels)
            print("For n_clusters =", n_clusters,
                   "The average silhouette_score is :", silhouette_avg)
    
    # Compute the silhouette scores for each sample
            sample_silhouette_values = silhouette_samples(df, cluster_labels)
            y_lower = 10
            for i in range(n_clusters):
    # Aggregate the silhouette scores for samples belonging to cluster i, and sort them
                ith_cluster_silhouette_values = \
                    sample_silhouette_values[cluster_labels == i]
    
                ith_cluster_silhouette_values.sort()
    
                size_cluster_i = ith_cluster_silhouette_values.shape[0]
                y_upper = y_lower + size_cluster_i
                
                try:
                    color = cm.nipy_spectral(float(i) / n_clusters)
                except Exception as python_error_message:
                    print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                    print(python_error_message) 
                    
                ax1.fill_betweenx(np.arange(y_lower, y_upper),
                                      0, ith_cluster_silhouette_values,
                                      facecolor=color, edgecolor=color, alpha=0.7)
    
    # Label the silhouette plots with their cluster numbers at the middle
                ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))
    
    # Compute the new y_lower for next plot
                y_lower = y_upper + 10  # 10 for the 0 samples
    
            ax1.set_title("The silhouette plot for the various clusters.")
            ax1.set_xlabel("The silhouette coefficient values")
            ax1.set_ylabel("Cluster label")
    
    # The vertical line for average silhouette score of all the values
            ax1.axvline(x=silhouette_avg, color="red", linestyle="--")
                    
            ax1.set_yticks([])  # Clear the yaxis labels / ticks
            ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])
    
    # 2nd Plot showing the actual clusters formed
            try:
                colors = cm.jet(cluster_labels.astype(float) / n_clusters)
            except Exception as python_error_message:
                print("Error occured while calculating color. Check if n_cluster value is negative or zero")
                print(python_error_message)
            
            ax2.scatter(df[var1], df[var2], marker='o', s=30, lw=0, alpha=0.7,
                            c=colors, edgecolor='k')
    # Labeling the clusters
            centers = clusterer.cluster_centers_
    # Draw black stars at cluster centers
            centers1 = pd.DataFrame(centers,columns=df.columns)
            
            ax2.scatter(centers1.loc[:, var1], centers1.loc[:, var2], marker='*',
                            c="black", alpha=1, s=200, edgecolor='k')
    
            ax2.set_title("The visualization of the clustered data.")
            ax2.set_xlabel("Feature space for the %s"% var1)
            ax2.set_ylabel("Feature space for the %s"% var2)
    
            plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                              "with n_clusters = %d" % n_clusters),
                             fontsize=14, fontweight='bold')
        return plt